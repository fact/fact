function h=map(x,col1,col2,varargin)
    
  //function [] = map(x,col1,col2,(addtolabel),(charsize)
  //map - graph of map of data using identifiers as names
  //function map(X,col1,col2,(charsize))

  global newfig;
  
  s=div(x);

  defaultsize = 1;     // default size for axis
  csize = 1;           // default size for plotted data (possibly changed)
  labelsize = 1;       // default size for X and Y labels

  if newfig=='T' then
    figure();
  end

  if argn(2)>=5 then csize = varargin(2);     end

  plot(s.d(:,col1),s.d(:,col2),"k.");

  xset("font size",csize)
  xstring(s.d(:,col1),s.d(:,col2),s.i);
  xset("font size",defaultsize)
  
 
  if argn(2)>3 then 
    addtolabel=varargin(1);
  else 
    addtolabel='';
  end
  
//  if argn(2)>=4 & typeof(addtolabel)=='div' then     // structure 
//    x_lab=addtolabel.infos(1); //+'  '+part(string(addtolabel.d(col1)),1:4)+'%';
//    y_lab=addtolabel.infos(2); //+'  '+part(string(addtolabel.d(col2)),1:4)+'%';
//  else
  if argn(2)>3 & (type(addtolabel)==1 | type(addtolabel)==10) & max(size(addtolabel))==2 then                                                                    // chaine de 2 caractères o 
    x_lab=addtolabel(1);
    y_lab=addtolabel(2);
  else 
    x_lab=s.v(col1);
    y_lab=s.v(col2);
  end
  
  a = get("current_axes");

  a.x_label.text=x_lab;
  a.y_label.text=y_lab;
  

  h=gcf()

endfunction

