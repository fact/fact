// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gateway()

    sci_gateway_dir=get_absolute_file_path('builder_gateway.sce');
    
    // pour C et C++:
    os=getos();
    
    if os=="Darwin" then
        languages       = ["c";"cpp"];
    elseif os=="Linux" then
        languages       = ["c";"cpp"];          // 18mai20 le cpp compile mais ne se charge pas => enleve
						// 19aout21 remis pour scilab-6.1.1 
    else
        languages       = ["cpp"];              // 23aout21 le c ne compile pas bien 
    end;

    tbx_builder_gateway_lang(languages,sci_gateway_dir);
    tbx_build_gateway_loader(languages,sci_gateway_dir);
    tbx_build_gateway_clean(languages,sci_gateway_dir);


endfunction


builder_gateway()
clear builder_gateway;       // remove builder_gateway on stack
