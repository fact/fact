#include "context.hxx"
#include "cpp_gateway_prototype.hxx"
#include "libfact_obiwarp_gw.hxx"
extern "C"
{
#include "libfact_obiwarp_gw.h"
}

#define MODULE_NAME L"libfact_obiwarp_gw"

int libfact_obiwarp_gw(wchar_t* _pwstFuncName)
{
    if(wcscmp(_pwstFuncName, L"obiwarp_lmata") == 0){ symbol::Context::getInstance()->addFunction(types::Function::createFunction(L"obiwarp_lmata", &sci_obiwarp, MODULE_NAME)); }

    return 1;
}
