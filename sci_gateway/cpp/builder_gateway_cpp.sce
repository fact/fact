// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_cpp()

    gw_c_path = get_absolute_file_path("builder_gateway_cpp.sce");
    LDFLAGS = "";
    
    CFLAGS = ilib_include_flag(gw_c_path);
    CFLAGS = CFLAGS + ilib_include_flag(gw_c_path + "../../src/cpp");

    // PutLhsVar managed in gateways sci_sum    
    WITHOUT_AUTO_PUTLHSVAR = %t;

    tbx_build_gateway("fact_cpp", ..
    [ "obiwarp_lmata" "sci_obiwarp" "cppsci"], ..
    ["sci_obiwarp.cpp"], ..
    gw_c_path, ..
    ["../../src/cpp/libfact_obiwarp"], ..
    LDFLAGS, ..
    CFLAGS);

endfunction

builder_gw_cpp();
clear builder_gw_cpp; // remove builder_gw_cpp on stack
