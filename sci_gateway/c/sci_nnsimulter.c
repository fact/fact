/* ==================================================================== */
/* Template toolbox_skeleton */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ==================================================================== */
#include "nnsimul_gw.h"

/* ==================================================================== */
/* x: (nd x ni)      */
/* y: (nd x no)      */
/* wh: ((ni+1)x nh)  */
/* wo: ((nh+1)x no)  */
/* stdres: (1 x no)  */
/* covw: (nw * nw)   */
/* stdout: (nd * no) */



int sci_nnsimulter(GATEWAY_PARAMETERS)
{ 
    SciErr sciErr;
 
    int ni=0, nd=0, nh=0, no=0 , nw=0; 

    int *piAddressVarOne = NULL;
    double *wh = NULL;
    
    int *piAddressVarTwo = NULL;
    double *wo = NULL;
    
    int *piAddressVarThree = NULL;
    double *x = NULL;
 
    int *piAddressVarFour = NULL;
    double *y = NULL;    

    double *sse = NULL;    
    double *gra = NULL;
    double *hes = NULL; 

    int iType1 = 0;
    int iType2 = 0;
    int iType3 = 0;
    int iType4 = 0;
    
    int i,j;


    /* check that we have only 4 input arguments */
    /* check that we have only 3 output arguments */
    CheckInputArgument(pvApiCtx, 4, 4) ;
    CheckOutputArgument(pvApiCtx, 3, 3) ;



    /* get Address of input one */
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType1 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 1, matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarOne, &i, &nh, &wh);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

    ni=i-1; 
 
 
     /* get Address of input two */
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType2 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 2, matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarTwo, &i, &no, &wo);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

    
    if (i != nh+1) 
    {  Scierror(999, "%s: Wrong number of hidden neurons.\n", fname, 1);  return 0;  }

      
 
     /* get Address of input three */
    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarThree, &iType3);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType3 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 3, a matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarThree, &nd, &i, &x);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

 
    if (i != ni) 
    {  Scierror(999, "%s: Wrong number of input variables.\n", fname, 1);  return 0;  }

 
    

    /* get Address of input four */
    sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddressVarFour);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }
    
    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarFour, &iType4);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }
    
    if ( iType4 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 4, a matrix expected.\n", fname, 1);  return 0;  }
    
    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarFour, &i, &j , &y);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }
    

    if (i != nd) 
    {  Scierror(999, "%s: Wrong number of input variables.\n", fname, 1);  return 0;  }     
    
    if (j != no) 
    {  Scierror(999, "%s: Wrong number of input variables.\n", fname, 1);  return 0;  }    
    
    nw=(ni+1)*(nh) + (nh +1)*(no);
   
    
    
    /* memory allocation: */
    sse = malloc((no) *sizeof(double));
    gra = malloc((nw) * (no) * sizeof(double));
    hes = malloc((nw) * (nw) * (no) * sizeof(double));


    
    /* call c function nnsimul */
    nnsimulter(wh, wo, x, y, sse, gra, hes, ni, nd, nh, no, nw); 

    

    
    /* Create the matrix as return of the function */
    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, 1 ,no, sse);
    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx)+ 1;

    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, nw , no, gra);
    AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx)+ 2;    
  
    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 3, nw ,nw*no, hes);
    AssignOutputVariable(pvApiCtx, 3) = nbInputArgument(pvApiCtx)+ 3; 
    
    ReturnArguments(pvApiCtx);

    return 0;

}
/* ==================================================================== */
