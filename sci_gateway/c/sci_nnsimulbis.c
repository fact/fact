/* ==================================================================== */
/* Template toolbox_skeleton */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ==================================================================== */
#include "nnsimul_gw.h"

/* ==================================================================== */
/* x: (nd x ni)      */
/* y: (nd x no)      */
/* wh: ((ni+1)x nh)  */
/* wo: ((nh+1)x no)  */
/* stdres: (1 x no)  */
/* covw: (nw * nw)   */
/* stdout: (nd * no) */

int sci_nnsimulbis(GATEWAY_PARAMETERS)

{          
    SciErr sciErr;
 
    int ni=0, nd=0, nh=0, no=0 , nw=0; 

    int *piAddressVarOne = NULL;
    double *wh = NULL;
    
    int *piAddressVarTwo = NULL;
    double *wo = NULL;
    
    int *piAddressVarThree = NULL;
    double *x = NULL;
 
    int *piAddressVarFour = NULL;
    double *stdres = NULL;    
    
    int *piAddressVarFive = NULL;
    double *covw = NULL;    
    
    double *   y = NULL;

    double * Stdout = NULL;

    int iType1 = 0;
    int iType2 = 0;
    int iType3 = 0;
    int iType4 = 0;
    int iType5 = 0;
    
    int i,j;
    /* check that we have only 5 input arguments */
    /* check that we have only 2 output arguments */
    CheckInputArgument(pvApiCtx, 5, 5) ;
    CheckOutputArgument(pvApiCtx, 2, 2) ;



    /* get Address of input one */
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType1 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 1, matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarOne, &i, &nh, &wh);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

    ni=i-1; 
 
 
     /* get Address of input two */
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType2 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 2, matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarTwo, &i, &no, &wo);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

    
    if (i != nh+1) 
    {  Scierror(999, "%s: Wrong number of hidden neurons.\n", fname, 1);  return 0;  }

      
 
     /* get Address of input three */
    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarThree, &iType3);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType3 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 3, a matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarThree, &nd, &i, &x);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

 
    if (i != ni) 
    {  Scierror(999, "%s: Wrong number of input variables.\n", fname, 1);  return 0;  }

 
    

    /* get Address of input four */
    sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddressVarFour);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }
    
    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarFour, &iType4);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }
    
    if ( iType4 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 4, a matrix expected.\n", fname, 1);  return 0;  }
    
    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarFour, &i, &j , &stdres);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }
    

    if (i != 1) 
    {  Scierror(999, "%s: Wrong number of input variables.\n", fname, 1);  return 0;  }     
    
    if (j != no) 
    {  Scierror(999, "%s: Wrong number of input variables.\n", fname, 1);  return 0;  }    
    
    
    
    /* get Address of input five */
    sciErr = getVarAddressFromPosition(pvApiCtx, 5, &piAddressVarFive);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }
    
    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarFive, &iType5);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }
    
    if ( iType5 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 5, a matrix expected.\n", fname, 1);  return 0;  }
    
    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarFive, &nw , &j , &covw);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

 
    if (j != nw ) 
    {  Scierror(999, "%s: Input 5 has wrong dimensions.\n", fname, 1);  return 0;  }    
    
    if (j != ((ni+1)*nh + (nh+1)*no)) 
    {  Scierror(999, "%s: input 5 has wrong dimensions.\n", fname, 1);  return 0;  }     


   
    y = malloc(sizeof(double) * (nd) * (no));
    Stdout = malloc(sizeof(double) * (nd) * (no));
   

    for (j=0;j<nd*no;j++) 
       y[j]=0;
    for (j=0;j<nd*no;j++)
       Stdout[j]=0;    
    
    
    /* call c function nnsimulbis */
    nnsimulbis(wh, wo, x, stdres, covw, y, Stdout, ni, nd, nh, no, nw); 

    
   
    
    /* Create the matrix as return of the function */
    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, nd ,no, y);
    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx)+ 1;

    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, nd ,no, Stdout);
    AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx)+ 2;    
    
    
    ReturnArguments(pvApiCtx);

    return 0;

}
/* ==================================================================== */
