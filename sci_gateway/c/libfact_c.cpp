#include <wchar.h>
#include "libfact_c.hxx"
extern "C"
{
#include "libfact_c.h"
#include "addfunction.h"
}

#define MODULE_NAME L"libfact_c"

int libfact_c(wchar_t* _pwstFuncName)
{
    if(wcscmp(_pwstFuncName, L"c_nnsimul") == 0){ addCStackFunction(L"c_nnsimul", &sci_nnsimul, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"c_nnsimulbis") == 0){ addCStackFunction(L"c_nnsimulbis", &sci_nnsimulbis, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"c_nnsimulter") == 0){ addCStackFunction(L"c_nnsimulter", &sci_nnsimulter, MODULE_NAME); }

    return 1;
}
