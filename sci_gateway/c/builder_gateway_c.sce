// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_c()

    gw_c_path = get_absolute_file_path("builder_gateway_c.sce");
    LDFLAGS = "";
    
    CFLAGS = ilib_include_flag(gw_c_path);
    CFLAGS = CFLAGS + ilib_include_flag(gw_c_path + "../../src/c");

    // PutLhsVar managed in gateways sci_sum    
    WITHOUT_AUTO_PUTLHSVAR = %t;

    tbx_build_gateway("fact_c", ..
    [ "c_nnsimul","sci_nnsimul";"c_nnsimulbis","sci_nnsimulbis";"c_nnsimulter","sci_nnsimulter"], ..
    ["sci_nnsimul.c","sci_nnsimulbis.c","sci_nnsimulter.c"], ..
    gw_c_path, ..
    ["../../src/c/libnnsimul"], ..
    LDFLAGS, ..
    CFLAGS);

endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
