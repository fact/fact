/* ==================================================================== */
/* Template toolbox_skeleton */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ==================================================================== */
#include "nnsimul_gw.h"

/* ==================================================================== */
/* x: (nd x ni)      */
/* y: (nd x no)      */
/* wh: ((ni+1)x nh)  */
/* wo: ((nh+1)x no)  */



int sci_nnsimul(GATEWAY_PARAMETERS)

{   SciErr sciErr;
 
    int ni=0, nd=0, nh=0, no=0 ; 

    int *piAddressVarOne = NULL;
    double *wh = NULL;
    
    int *piAddressVarTwo = NULL;
    double *wo = NULL;
    
    int *piAddressVarThree = NULL;
    double *x = NULL;
  
    double *y = NULL;     
    
    int iType1 = 0;
    int iType2 = 0;
    int iType3 = 0;
    
    int i,j; 

    
    /* check that we have only 3 input argument */
    /* check that we have only 1 output argument */
    CheckInputArgument(pvApiCtx, 3, 3) ;
    CheckOutputArgument(pvApiCtx, 1, 1) ;

    /* get Address of input one */
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType1 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 1, matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarOne, &i, &nh, &wh);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

    ni=i-1; 
 
 
     /* get Address of input two */
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType2 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 2, matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarTwo, &i, &no, &wo);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

   

    /* JCB: VERIFIER QUE i=nh+1 */ 
    if (i != nh+1) 
    {  Scierror(999, "%s: Wrong number of hidden neurons.\n", fname, 1);  return 0;  }

      
 
     /* get Address of input three */
    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
    if (sciErr.iErr)
    { printError(&sciErr, 0); return 0; }

    /* check input type */
    sciErr = getVarType(pvApiCtx, piAddressVarThree, &iType3);
    if (sciErr.iErr)
    {printError(&sciErr, 0); return 0; }

    if ( iType3 != sci_matrix )
    {  Scierror(999, "%s: Wrong type for input argument #%d: var 3, a matrix expected.\n", fname, 1);  return 0;  }

    /* get matrix */
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarThree, &nd, &i, &x);
    if (sciErr.iErr) 
    { printError(&sciErr, 0); return 0; }

 
    if (i != ni) 
    {  Scierror(999, "%s: Wrong number of input variables.\n", fname, 1);  return 0;  }


    
    y = malloc(sizeof(double) * nd * no);
 
 
    for (j=0;j<nd*no;j++) 
       y[j]=0; 
    
    
    /* call c function nnsimul */
    nnsimul(wh, wo, x, y, ni, nd, nh, no); 

    

    
    /* Create the matrix as return of the function */
    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, nd ,no, y);

    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx)+ 1;

    ReturnArguments(pvApiCtx);

    return 0;
  }

