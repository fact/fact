#ifdef __cplusplus
extern "C" {
#endif
#include <mex.h> 
#include <sci_gateway.h>
#include <api_scilab.h>
#include <sci_malloc.h>
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc sci_nnsimul;
extern Gatefunc sci_nnsimulbis;
extern Gatefunc sci_nnsimulter;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway_without_putlhsvar,sci_nnsimul,"c_nnsimul"},
  {(Myinterfun)sci_gateway_without_putlhsvar,sci_nnsimulbis,"c_nnsimulbis"},
  {(Myinterfun)sci_gateway_without_putlhsvar,sci_nnsimulter,"c_nnsimulter"},
};
 
int C2F(libfact_c)()
{
  Rhs = Max(0, Rhs);
  if (*(Tab[Fin-1].f) != NULL) 
  {
     if(pvApiCtx == NULL)
     {
       pvApiCtx = (StrCtx*)MALLOC(sizeof(StrCtx));
     }
     pvApiCtx->pstName = (char*)Tab[Fin-1].name;
    (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  }
  return 0;
}
#ifdef __cplusplus
}
#endif
