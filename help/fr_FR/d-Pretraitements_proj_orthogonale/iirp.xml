<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="iirp" 
          xml:lang="fr"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>iirp</refname>

    <refpurpose>calcule les vecteurs-propres et les paramètres de réglage de Independent Interference Reduction (IIR)  </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Séquence d'appel</title>

    <synopsis>[res] = iirp(x_ed,xcal,ycal,split,lv,(centering)) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Paramètres</title>
    <variablelist>
    
      <varlistentry>
         <term> x_ed: </term>
          <listitem> <para> une matrice (n1 x q) ou une structure Div issue d'un plan d'expérience pour lequel y=0 pour tout individu
          </para> </listitem> </varlistentry>       
          
       <varlistentry>
         <term> xcal,ycal: </term>   
          <listitem> <para> jeu d'étalonnage; une matrice de spectres (n x q) et un vecteur de valeurs de référence (n x 1) ou des structures Div 
          </para> </listitem> </varlistentry>           
          
        <varlistentry>
         <term> split: </term>   
          <listitem> <para> paramètre de sélection des blocs pour la validation croisée; un scalaire représentant un nombre de blocs contigus ou un vecteur identifiant chaque échantillon à un bloc  </para> 
          </listitem> </varlistentry>         
          
        <varlistentry>
         <term> lv: </term>   
          <listitem> <para> nombre de variables latentes pour la régression PLS; un scalaire  </para> 
          </listitem> </varlistentry>          
          
         <varlistentry>
         <term> (centering): </term>   
          <listitem> <para> modèle centré = 1 (par défaut) / modèle non centré =0   </para> 
          </listitem> </varlistentry>        

      <varlistentry>
        <term> res.d_matrix: </term>
          <listitem> <para> la matrice d'information nuisible, soit x_iirp pour le prétraitement IIR</para> 
          <para> res.d_matrix.d est une matrice de dimensions (n1 x q)</para> 
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.eigenvec: </term>
          <listitem> <para> vecteurs-propres de d_matrix  </para> 
          <para> res.eigenvec.d est une matrice de dimensions (q x n_eigenvect)</para> 
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.ev_pcent: </term>
          <listitem> <para> valeurs-propres de d_matrix exprimées en %  </para> 
          <para> res.ev_pcent.d est un vecteur de dimensions (n_eigenvect x 1) </para> 
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.wilks: </term>
          <listitem> <para> lambda de Wilks  </para>  
          <para> res.wilks.d est un vecteur de dimensions ((n_eigenvect+1) x 1)  </para>
          <para> attention, paramètre non pertinant, il n'y a qu'un seul groupe avec IIR  </para>
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.rmsecv: </term>
          <listitem> <para> rmsecv pour plusieurs dimensions d'EROS et plusieurs dimensions de PLSR  </para>  
          <para> res.rmsecv.d est une matrice de dimensions (lv x (n_eigenvect+1))  </para>
          </listitem> </varlistentry> 

        <varlistentry>
         <term> res.pls_models: </term>   
          <listitem> <para> modèles PLS obtenus après une correction EROS utilisant 0/1/2/...n_eigenvect vecteurs-propres de res.eigenvect.d  </para>  
          <para> res.pls_models est une liste de dimensions (n_eigenvect+1) </para>
          <para> voir l'aide de pls pour l'explication des éléments de res.pls_models </para>
          </listitem> </varlistentry>

     </variablelist>                 
  </refsection>

  <refsection>
    <title>Exemples</title>    
   <programlisting role="example">[res_iir]=iirp(xg,xcal,ycal,10,5)               </programlisting>
   <programlisting role="example">[res_iir]=iirp(xg,xcal,ycal,10,5,0)             </programlisting>
  </refsection>

 <refsection>
    <title>Bibliographie </title>
     <simplelist type="vert">
  <member>P.W.Hansen, Pre-processing method minimizing the need for reference analyses, J. of Chemom.,2001  </member>
     </simplelist>
  </refsection>


  <refsection>
    <title>Auteurs</title>
    <simplelist type="vert">
      <member>JM Roger, IRSTEA  </member>
      <member>JC Boulet, INRA  </member>
    </simplelist>
  </refsection>
  
</refentry>
