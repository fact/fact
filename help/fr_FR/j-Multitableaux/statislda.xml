<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab"
          xml:id="statislda"
          xml:lang="fr"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)$ </pubdate>
  </info>

  <refnamediv>
    <refname>statislda</refname>
    <refpurpose>méthode Statis-analyse factorielle discriminante  </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Séquence d'appel</title>
    <synopsis>result = statislda(Xtab,Y,(RV)); </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Paramètres</title>
    <variablelist>
    
    
      <varlistentry>
         <term> Xtab: </term>   
         <listitem> 
            <para> les différents tableaux de données, sous forme d'une liste                                    </para>
            <para> Xtab(i) est une matrice de dimensions (n x pi) ou une structure div      </para> 
            <para> la somme des pi donne p                                                          </para>                           
         </listitem> 
      </varlistentry>   
          
          
     <varlistentry>
        <term> Y: </term>
          <listitem> 
             <para>la classe de chaque observation                                                                                                </para>                       
             <para>Y est un vecteur conjonctif, ou une matrice disjonctive, ou une structure Div de données conjonctives ou disjonctives </para> 
          </listitem> 
     </varlistentry>
          
     <varlistentry>
        <term> (RV): </term>
          <listitem> 
             <para> t: normalisation de la matrice C                  </para>                       
             <para> f: pas de normalisation (par défaut)              </para>    
          </listitem>
     </varlistentry>
          
     <varlistentry>
        <term> result.p: </term>
          <listitem> 
             <para> axes de Statis-LDA                                                </para>
             <para> result.p.d est une matrice de dimensions (p x n_axes)             </para>                    
          </listitem> 
     </varlistentry>          
          
     <varlistentry>
        <term> result.t: </term>
          <listitem> 
             <para> les coordonnées des observations selon les axes de Statis-LDA     </para>  
             <para> result.t.d est une matrice de dimensions (n x n_axes)             </para>                            
          </listitem> 
     </varlistentry>         
 
     <varlistentry>
        <term> result.tg: </term>
          <listitem> 
             <para> les coordonnées des centres de gravité des q tables               </para>  
             <para> result.tg.d est une matrice de dimensions (q x n_axes)            </para>                            
          </listitem> 
     </varlistentry>                   
                                     
                                                                                  
      <varlistentry>
        <term> result.normw: </term>
          <listitem> 
             <para> les normes des q opérateurs associés aux LDA de (Xi, Y)            </para>    
             <para> result.normw.i est un vecteur de dimensions (q x 1)               </para>                   
          </listitem> 
      </varlistentry>
  
                  
      <varlistentry>
        <term> result.cmatrix: </term>
          <listitem> 
             <para> matrice des produits scalaires entre les q opérateurs             </para>  
             <para> result.cmatrix.d est une matrice de dimensions (q x q)            </para>                    
          </listitem> 
      </varlistentry>         


      <varlistentry>
        <term> result.cmatrix_eigenvec: </term>
          <listitem> 
             <para> vecteurs-propres de cmatrix                                       </para>  
             <para> result.cmatrix_eigenvec.d est une matrice de dimensions (q x q)   </para>                    
          </listitem> 
      </varlistentry> 

          
     <varlistentry>
        <term> result.cc: </term>
          <listitem> 
             <para> coefficients du compromis après diagonalisation de cmatrix        </para> 
             <para>  result.cc.d est un vecteur de dimensions (q x 1)                 </para>        
          </listitem> 
     </varlistentry>          
          
     <varlistentry>
        <term> result.vtot: </term>
          <listitem> 
             <para> la matrice de variance-covariance du compromis                    </para>  
             <para> result.vtot.d est une matrice de dimensions (p x p)               </para>                       
          </listitem> 
     </varlistentry>
          
     <varlistentry>
        <term> result.vtot_eigenvec: </term>
          <listitem> 
             <para>   vecteurs propres de vtot                                        </para>
             <para> result.vtot_eigenvec.d est une matrice de dimensions (p x n_axes) </para>             
          </listitem> 
     </varlistentry>          
          
     <varlistentry>
        <term> result.vtot_eigenval: </term>
          <listitem> 
             <para> valeurs-propres de vtot                                           </para> 
             <para> result.vtot_eigenval.d est un vecteur de dimensions (n_axes x 1)  </para>                 
          </listitem> 
     </varlistentry>          
          
     <varlistentry>
        <term> result.xconc_mean: </term>
          <listitem> 
             <para> moyenne de la matrice des données concaténées selon les colonnes  </para>
             <para> result.xconc_mean.d est un vecteur de dimensions (p x 1)          </para>                       
          </listitem> 
     </varlistentry>          
                    
                              
     </variablelist>                 
  </refsection>


  <refsection>
    <title>Exemples</title>    
    <programlisting role="example">[res]=statislda(x,y);      </programlisting>
  </refsection>


  <refsection>
    <title>Bibliographie</title>
     <simplelist type="vert">
      <member>R.Sabatier et al. Une nouvelle proposition: l'analyse discriminante multitableaux: STATIS-LDA, J. de la Société Française de Statistiques, 154 (3), 2013 </member>
     </simplelist>
  </refsection>


  <refsection>
    <title>Auteurs</title>
    <simplelist type="vert">
      <member>R.Sabatier, UM1, FR </member>
    </simplelist>
  </refsection>
  
</refentry>
