<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="outlier_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>outlier</refname>

      <refpurpose> calculation of 3 parameters: T2-Hotelling, Q or residual variances, and leverage, useful to identify outliers  </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>[t2,q,leverage] = outlier(x,x_scores,x_loadings,lv) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
         <term> x: </term>   
             <listitem> <para> x is a Div structure or a matrix of dimensions (n x q) </para>
             </listitem> </varlistentry>

      <varlistentry>
        <term> x_scores: </term>
          <listitem> <para> the scores obtained after a PCA onto x </para>
              <para> x_scores is a Div structure or a matrix of dimensions (n x naxes) 
              </para>
          </listitem> </varlistentry>
          
      <varlistentry> 
        <term> x_loadings: </term>
          <listitem> <para> the loadings obtained after a PCA onto x  </para>
                <para> x_loadings is a Div structure or a matrix of dimensions (n x naxes)  </para>
          </listitem> </varlistentry>       
 
       <varlistentry>
         <term> lv: </term>   
             <listitem> <para> the maximum number of eigenvectors or latent variables to be  computed   </para>
                  <para> lv must be lower or equal to naxes  </para>
             </listitem> </varlistentry>
   
       <varlistentry>
         <term> t2: </term>
           <listitem> <para> Hotelling's T2; a Div structure  </para>
             <para> T2.d is of dimensions (n x lv)  </para>
             </listitem> </varlistentry> 
   
       <varlistentry>
         <term> q: </term>
           <listitem> <para> the residual variances; a Div structure  </para>
             <para> q.d is of dimensions (n x lv)  </para>
             </listitem> </varlistentry>
   
       <varlistentry>
         <term> leverage: </term>
           <listitem> <para> the leverage effect; a Div structure  </para>
                 <para> leverage.d is of dimensions (n x lv) </para>
            </listitem> </varlistentry>

           
     </variablelist>                 
  </refsection>

  <refsection>
    <title>Examples</title>    
    <programlisting role="example">[t2hot,q_var,lever]=outlier(x,x_scores,x_eigenvec,20)                    </programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>F Gogé, IRSTEA  </member>
    </simplelist>
  </refsection>
  
</refentry>
