// Copyright (C) 2008 - INRIA
// Copyright (C) 2009-2011 - DIGITEO

// This file is released under the 3-clause BSD license. See COPYING-BSD.
// 

mode(-1);
lines(0);

clear main_builder
clear factlib
clear libfact_c

function main_builder()

    TOOLBOX_NAME  = "fact";
    TOOLBOX_TITLE = "Fact";
    toolbox_dir   = get_absolute_file_path("builder.sce");

    // Check Scilab's version
    // =============================================================================

    try
        v = getversion("scilab");
    catch
        error(gettext("Scilab 2023.0.0 or more is required."));
    end



    // Check modules_manager module availability
    // =============================================================================

    if ~isdef("tbx_build_loader") then
        error(msprintf(gettext("%s module not installed."), "modules_manager"));
    end

    // Action
    // =============================================================================

    tbx_builder_macros(toolbox_dir);
    tbx_builder_src(toolbox_dir);     
    clear factlib
    clear libfact_c
    tbx_builder_gateway(toolbox_dir); 
    tbx_build_localization(toolbox_dir);
    if or(getscilabmode() == ["NW";"STD"]) then
        tbx_builder_help(toolbox_dir);
    end
    tbx_build_loader(toolbox_dir);
    tbx_build_cleaner(toolbox_dir);

endfunction
// =============================================================================
main_builder();
clear main_builder; // remove main_builder on stack
// =============================================================================


