FACT, "Free-Access Chemometric Toolbox".

This chemometric toolbox is dedicated to multivariate analysis. It contains famous applications e.g. partial least squares regression (PLSR), but also less common applications e.g. pretraitments by orthogonal projection, discriminant methods or multiway analysis.  

To get started, a tutorial FR/EN and training datasets can be downloaded at the link below. 

Contributors:  Jean-Claude Boulet, INRA, France
	       Dominique Bertrand, INRA, France
               Jean-Michel Roger, IRSTEA, France 

Released under the CeCILL-C license
------------------------------------------------







