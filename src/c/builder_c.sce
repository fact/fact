// This file is released under the 3-clause BSD license. See COPYING-BSD.

// This macro compiles the files

function builder_c()

    src_c_path = get_absolute_file_path("builder_c.sce");

    CFLAGS = ilib_include_flag(src_c_path);
    //CFLAGS = CFLAGS + " -DFACT_EXPORTS";
    
    tbx_build_src(["nnsimul","nnsimulbis","nnsimulter"], ..
    ["nnsimul.c","nnsimulbis.c","nnsimulter.c"], ..
    "c",                ..
    src_c_path,         ..
    "",                 ..
    "",                 ..
    CFLAGS);

endfunction

builder_c();
clear builder_c; // remove builder_c on stack
