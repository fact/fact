/*
% Neural Network for Static models -- Simulation
%
% Usage 1: Output prediction                    - NOT COMPUTED HERE - 
%     Out=nnsimul(Wh,Wo,Inp)
%
% Usage 2: Output & standard deviation          - NOT COMPUTED HERE -
%     [Out,StdOutter]=nnsimulbis(Wh,Wo,Inp,StdRester,CovWter)
%
% Usage 3: Residuals, gradient & hessian        - THE ONLY ONE COMPUTED HERE -
%     [sse,gra,hes]=nnsimulter(Wh,Wo,Inp,Out)
%
% Var     Size       Significance
% ----    ---------- -----------------------------------------
% Ni     (1 x 1)     Number of network inputs (without bias)
% Nh     (1 x 1)     Number of neurons in the hidden layer (without bias)
% No     (1 x 1)     Number of network outputs
% Nd     (1 x 1)     Number of experimental data points (observations)
% Nw     (1 x 1)     Total number of weights = (Ni+1)*Nh + (Nh+1)*No
%
% Wh     (Ni+1 x Nh) Weight matrix for the hidden layer (tanh transfer function)
% Wo     (Nh+1 x No) Weight matrix for the output layer (identity transfer function)
% Inp    (Nd x Ni)   Input data matrix. One row per observation.
% Out    (Nd x No)   Output data matrix. One row per observation.
% StdOutter (Nd x No)   Estimated standard deviation of predicted output.
% StdRester (1 x No)    Standard deviation of the residual (measurement) noise.
% CovWter   (Nw x Nw)   Covariance matrix of network weights.
% Sse    (1 x No)    Sums of squares of the errors for each output.
% Gra    (Nw x No)   Gradients for each output: partial derivatives of the
%                    sums of squares of the errors with respect to each weight.
% Hes (Nw x Nw x No) Hessians for each output: approximate second order
%                    derivatives of the sums of squares of the errors
%                    with respect to the weights.

% (c) 1999 Ioan Cristian TRELEA
*/


#include "nnsimul.h"
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>


#define wh(a,b)         (wh[(a)+(ni+1)*(b)])
#define wo(a,b)         (wo[(a)+(nh+1)*(b)])
#define x(a,b)          (x[(a)+(nd)*(b)])
#define y(a,b)          (y[(a)+(nd)*(b)])
#define gra(a,b)        (gra[(a)+(nw)*(b)])
#define hes(a,b,c)      (hes[(a)+(nw)*(b)+(nw)*(nw)*(c)])
#define dOutdWh(a,b,c)  (dOutdW[(a)+(no)*(b)+(no)*(ni+1)*(c)])
#define dOutdWo(a,b,c)  (dOutdW[(no)*(ni+1)*(nh) + (a)+(no)*(b)+(no)*(nh+1)*(c)])
#define dOutdW(a,b)     (dOutdW[(a)+(no)*(b)])

/*
void ErrMsg(int ErrNmb, ...)
 { char ErrBuf[256];
   va_list arglist;
   va_start(arglist,ErrNmb);
   vsprintf(ErrBuf,ErrTbl[ErrNmb],arglist);
   va_end(arglist);
   mexErrMsgTxt(ErrBuf);
 }
*/

double nnsimulterfh(double a)           // Transfer function hidden layer
{ return 2.0/(1.0+exp(-2*a))-1;
}

double nnsimulterdfh(double a)          // Derivative transfer function hidden layer
{ return 1-a*a;
}

double nnsimulterfo(double a)              // Transfer function output layer
{ return a;
}

double nnsimulterdfo(double a)             // Derivative transfer function output layer
{ return 1; 
}




   // Computation Usage 3:  [sse,gra,hes]=nnsimulter(wh,wo,x,y)

   int nnsimulter(double* wh, double* wo, double* x, double* y, double* sse, double* gra, double* hes, int ni, int nd, int nh, int no, int nw )

  {   

    int i,j,k;
    
    int ii,ih,io,id,iw,jw;

    double  *StdOut,*StdRes,*CovW;
    double  *hid,*SimOut,*dOutdW,*dOutdWh,*dOutdWo;

    // Computation Usage 3:    [Sse,Gra,Hes]=nnsimul(Wh,Wo,Inp,Out)
        hid=malloc((nh+1)*sizeof(double));      // Place for hidden layer
        SimOut=malloc(no*sizeof(double));     // For 1 data point
        dOutdW=malloc(no*nw*sizeof(double));  // For 1 data point
        for (io=0; io<no; io++)       
            SimOut[io]=0.0; // Prepare sum over id
        for (iw=0; iw<no*nw; iw++)    
            dOutdW[iw]=0.0;  // Ensure dOutdWo(io,ih,jo)=0, for io!=jo
        for (iw=0; iw<no; iw++)    
            sse[iw]=0.0;     // Prepare sum over id      
        for (iw=0; iw<nw*no; iw++)    
            gra[iw]=0.0;     // Prepare sum over id
        for (iw=0; iw<nw*nw*no; iw++) 
            hes[iw]=0.0;     // Prepare sum over id
        for (iw=0; iw<nh; iw++)       
            hid[iw]=0.0;         
        hid[nh]=1.0;                   // Bias
        
        for (id=0; id<nd; id++)        // For all data points
          {
            // Compute Hid
            for (ih=0; ih<nh; ih++)    // For all hidden neurons
              { hid[ih]=wh(ni,ih);     // Bias
                for (ii=0; ii<ni; ii++) 
                    hid[ih]+=x(id,ii)*wh(ii,ih);
                hid[ih]=nnsimulterfh(hid[ih]);   // Transfer function
              }
            for (io=0; io<no; io++)    // For all outputs
              {
              
                // Compute SimOut & Sse
                SimOut[io]=wo(nh,io);     // Bias
                for (ih=0; ih<nh; ih++) SimOut[io]+=hid[ih]*wo(ih,io);
                SimOut[io]=nnsimulterfo(SimOut[io]);   // Transfer function
                sse[io]+=(SimOut[io]-y(id,io))*(SimOut[io]-y(id,io));
                  
                  
                // Compute dOutdW
                for (ih=0; ih<nh; ih++)// For all hidden neurons, except bias
                  { for (ii=0; ii<ni; ii++) // For all inputs, except bias
                        dOutdWh(io,ii,ih)=x(id,ii)*nnsimulterdfh(hid[ih])*wo(ih ,io)*nnsimulterdfo(SimOut[io]);
                        dOutdWh(io,ni,ih)=nnsimulterdfh(hid[ih])*wo(ih,io)*nnsimulterdfo(SimOut[io]); // For the bias
                  }
                //
                for (ih=0; ih<=nh; ih++)// For all hidden neurons, including bias
                    dOutdWo(io,ih,io)=hid[ih]*nnsimulterdfo(SimOut[io]);
                     
                // Compute Gra
                for (iw=0; iw<nw; iw++)// For all weights
                    gra(iw,io)+=2*(SimOut[io]-y(id,io))*dOutdW(io,iw);
                    
                // Compute Hes
                for (iw=0; iw<nw; iw++)// For all weights
                    for (jw=0; jw<nw; jw++)// For all weights
                        hes(iw,jw,io)+=2*dOutdW(io,iw)*dOutdW(io,jw);
               //
              }
          }
          
        free(dOutdW);
        free(SimOut);
        free(hid);
        
        return 0;
    }


