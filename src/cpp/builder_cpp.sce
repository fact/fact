// This file is released under the 3-clause BSD license. See COPYING-BSD.

// This macro compiles the files

function builder_cpp()

    src_cpp_path = get_absolute_file_path("builder_cpp.sce");

    names = ["obiwarp"];
    files = ["vec.cpp" "mat.cpp" "lmat.cpp" "dynprog.cpp" "pngio.cpp" "obiwarp.cpp"];
    LDFLAGS = "";
    CFLAGS = ilib_include_flag(src_cpp_path);
    FFLAGS = "";
    CC = "";
    libname = "fact_obiwarp";

    tbx_build_src(names, ..
    files, ..
    "c", ..
    src_cpp_path, ..
    "", ..
    LDFLAGS, ..
    CFLAGS, ..
    FFLAGS, ..
    CC, ..
    libname);

endfunction

builder_cpp();
clear builder_cpp; // remove builder_cpp on stack

