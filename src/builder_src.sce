// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_src()
    os=getos();
    if os=="Darwin" then
        langage_src       = ["c";"cpp"];
    elseif os=="Linux" then
        langage_src       = ["c";"cpp"];        // 18mai20 le cpp compile mais ne se charge pas => enleve
						// 19aout21 remis pour scilab-6.1.1
    else
        langage_src       = ["cpp"];            //23aout21 problemes de compilation du c 
    end;

    path_src = get_absolute_file_path("builder_src.sce");
    tbx_builder_src_lang(langage_src, path_src);
endfunction

builder_src();
clear builder_src; // remove builder_src on stack
