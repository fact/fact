function h=curves(xs,varargin)                              

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013            

  global newfig
  
  s=div(xs);
  [n,q]=size(s.d);
  
  if  argn(2)>=2 then couleur=varargin(1);   end
  if  argn(2)>=3 then xlab=varargin(2);      end
  if  argn(2)>=4 then ylab=varargin(3);      end
  if  argn(2)>=5 then titre=varargin(4);     end
   
  // nouvelle figure
  if  newfig=='T' then
     figure();
  end

  h=gcf(); 
  h.background=8;      
                                   
  x=s.d;
  
  
  // gestion de l'échelle des abscisses:  on enlève tous les préfixes
  
  labels_abs=strsubst(s.i,'POP','');
  labels_abs=strsubst(labels_abs,'PC',''); 
  labels_abs=strsubst(labels_abs,'LV',''); 
  labels_abs=strsubst(labels_abs,'DV','');    // discriminant variables 
  labels_abs=strtod(labels_abs);

  lab_abs_isnan=isnan(labels_abs);  // si il ne reste plus de valeurs numériques après strtod
  test_nan=find(lab_abs_isnan==%T);
    
  if test_nan~=[] then   
    labels_abs=[1:1:n]';           // conversion string to double pas possible
    warning('default values (indexes) were used for the abscissa scale' )
  end

  // impression de la figure
  if argn(2)>=2  then
    plot(labels_abs,x,couleur); 
  elseif argn(2)<2  then 
    plot(labels_abs,x); 
  end


  // gestion des labels de la figure
  if size(s.infos,1)==2 & argn(2)<=2 then    
      xylabels=s.infos;
      xlabel(xylabels(1));
      ylabel(xylabels(2));
  end
 
  if(argn(2)>2) then xlabel(xlab); end;                                         
  if(argn(2)>3) then ylabel(ylab); end;                                           
  if(argn(2)>4) then title(titre); end;               
  
              
    
endfunction               
