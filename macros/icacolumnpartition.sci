function W = icacolumnpartition(X, partitions)
    
    // decoupe x selon les lignes en un nombre de blocs contigus = partitions
    // equivalent de Jack knife
    q = size(X,2);
    stride = floor( q/partitions);
    remaining = modulo( q , partitions);
    
    count = 1;
    W = list();
    
    for i = 1:partitions;

        step = count + stride -1;
        if  i == partitions  then
            step = step + remaining;  
        end
              
        W(i) = X(:,count:step);
        count = count + stride;
    end

endfunction
