function [model] = cvclass(xi,yi,split0,lv,func,varargin)

  // maj : 28aout24
  
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
  
  // Arguments d'entrée: ====================================================================
  // split0: une valeur (nbre de groupes de cv) ou 2 valeurs (nbre de groupes de cv x nbre de repetitions)
  // lv: une valeur (nbre de dv=variables discriminantes pour fda / nbre de lv =variables latentes pour plsda)
  //     ou deux valeurs (nbre de lv + nbre de dv pour plsfda)
  // arguments de varargin: 
  // metric:     defaut=0
  // scale:      pour X; defaut='cs'
  // class:      défaut=0
  // seuil:      défaut=1/nbre de classes
  
  // Arguments de sortie: ===================================================================
  // model.conf_cal_nobs:  une liste de longueur dv, valeur max=nbre classes -1
  //                       chaque élément = un div de dimensions nb classes x nb classes x lv
  //      .conf_cal:       une liste de longueur dv, valeur max=nbre classes -1
  //               
  

  //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary) 
  //-----------------------------------------------------------------

 // gestion des variables en entrée:
  x2=div(xi);
  if typeof(yi)=='div' then
    [test1]=isdisj(yi);
    [test2]=isconj(yi);     
    if test1=='T' then 
        y2=yi;
    elseif test2=='T' then   
        y2.d=conj2dis(yi.d);
        y2=div(y2.d);
    else 
        error ('cvclass: wrong value of y in cvclass/cvclass2' )
    end     
  elseif typeof(yi)=='string' then
    [y2.d,y2.v]=str2conj(yi);
    y2.d=conj2dis(y2.d);
    y2=div(y2);
  elseif typeof(yi)=='constant' & min(size(yi))==1 then
    yi=conj2dis(yi);
    y2=div(yi);
  elseif typeof(yi)=='constant' & min(size(yi))>1 then
    y2=div(yi);
  end
  x=x2.d;

  
  if  size(x,1)<2 then 
      error ('cvlclass: input data must contain at least 2 observations');  
  end
  
  y=y2.d;
  
  // gestion de y2.v:
  if size(y2.v)~=size(y2.d,2) then 
    y2.v='cl_'+string([1:1:size(y2.d,2)]');
  end
  
  if size(y2.v)==1 then
      error('y contains only one class')
  end



  // gestion des arguments d'entrée par défaut (copie dans classtestxx)
  nbcl=size(y2.d,2);
  if argn(2)==5 then
     metric=0;
     scale = 'cs';
     class = 0;
     seuil = 1/nbcl;
  elseif argn(2)==6 then
     metric=varargin(1);
     scale='cs';
     class = 0;
     seuil = 1/nbcl;
  elseif argn(2)==7 then
     metric=varargin(1);
     scale=varargin(2);
     class=0;
     seuil=1/nbcl;
  elseif argn(2)==8 then
     metric=varargin(1);
     scale=varargin(2);
     class=varargin(3);
     seuil=1/nbcl;
  elseif argn(2)==9 then
     metric=varargin(1);
     scale=varargin(2);
     class=varargin(3);
     seuil=varargin(4);    
  else error('cvclass: wrong number of input arguments in classtestxx') 
  end;

 
  // quelques vérifications ....
  [mx,nx] = size(x);
  [my,nbcl] = size(y);
  if mx~=my then
     error('cvclass: the number of samples in x and y do not match')
  end;
  
  
//  // précision des dimensions 
//  if func=='plsfda' then            // 10jan19
//      lv(2)=min(lv(2),nbcl-1);
//  elseif func=='fda' then           //19nov.20 enelve plsda car pas justifié de brider la dimension 
//      lv=min(lv,nbcl-1);
//  end
  
  // ajustement du nombre de dimensions pour la FDA, la PLSDA et la PLSFDA   // 21aout
  if func=='fda' | func=='plsfda' then   
        lv2=min(lv($),nbcl-1);
        lv($)=lv2;
  elseif func=='plsda' | func=='forwda' | func=='copda' then 
        lv2=lv;          
  end
  
  
  // ajustement du nombre de LVs pour la PLSFDA => supérieur au nombre de DV 
  if func=='plsfda'
  	lv(1)=max(lv(1),lv2+1);
  end	


  // ETALONNAGE =====================================================================================
 
  // matrices de confusion 
  
  [confus_m,lv_max,metric,scale,class,seuil]=classtestxx(x,y,x,y,lv,func,metric,scale,class,seuil);
  
  //confus_m0 est une liste de dimensions lv2 
  // elle contient des matrices carrées de dimensions (nlcl x nlcl) (x nlv pour plsfda)
  
  

  // non classés                                        //21aout23
  if func=='plsfda' then                                // plsfda (3d)
      nonclasse=zeros(lv2,lv(1));
      nonclasse_bycl=zeros(lv2,nbcl,lv(1));
 
      
      for i=1:lv2;                                      //21aout23
          for m=1:lv(1);
              conf_i=confus_m(i).d;
              [nul1,nul2,nul3,nci,nci_bycl]=conf_obs2pcent(conf_i(:,:,m),sum(y,'r'));
              nonclasse(i,m)=nci;
              nonclasse_bycl(i,:,m)=nci_bycl';
          end 
      end

  
  else                                                  // fda, plsda (2d)
      nonclasse=zeros(lv2,1);
      nonclasse_bycl=zeros(lv2,nbcl);

      for i=1:lv2;
          conf_i=confus_m(i).d;
          [c_i,nul2,nul3,nci,nci_bycl]=conf_obs2pcent(conf_i,sum(y,'r'));
          nonclasse(i)=nci;
          nonclasse_bycl(i,:)=nci_bycl';
      end
  end
  
  
  // matrice de confusion d'étalonnage mise en pourcentage
  if func=='plsfda' then                                // plsfda (3d)
      ec=zeros(lv2,lv(1));
      eclasses=zeros(nbcl,lv2,lv(1));
      confus_pcent=confus_m;              // liste de matrices (nbcl x nblc x lv(1))
      for i=1:lv2;
          conf_pcent=confus_m(i);    // un div (nbcl x nbcl x lv(1))
          ec_temp=zeros(1,lv(1));
          eclasses_temp=zeros(nbcl,lv(1));
          for m=1:lv(1);
              [c_i,ec_i,eclasses_i]=conf_obs2pcent(conf_pcent.d(:,:,m));  
              conf_pcent.d(:,:,m)=c_i;
              ec_temp(m)=ec_i;
              eclasses_temp(:,m)=eclasses_i;
              //ec(i,m)=ec_i;                 // ec = error of classification
              //eclasses(:,i,m)=eclasses_i    // eclasses = error by classes 
          end
          confus_pcent(i)=conf_pcent;  
          ec(i,:)=ec_temp;
          eclasses(:,i,:)=eclasses_temp;        
      end
      // confus_pcent: OK 
 //  pause
  else                                                  // fda, plsda (2d)
      ec=zeros(lv2,1);
      eclasses=zeros(nbcl,lv2);
      confus_pcent=confus_m;
      for i=1:lv2;
          [c_i,ec_i,eclasses_i]=conf_obs2pcent(confus_m(i).d);
          confus_pcent(i).d=c_i;                        //22aout23
          ec(i)=ec_i;
          eclasses(:,i)=eclasses_i
      end

  end


  // -------------------------------------
  // CALCUL DU MODELE (fda, plsda, forwda)
  // -------------------------------------
  
  // gestion de l'option 'c'/'cs':    				//28aout24
  if scale=='c' then 
      x_model=centering(x2);
  elseif scale=='cs' then
      x_model=standardize(centering(x2));
  elseif scale=='cp' then
      x_model=pareto(centering(x2));    
  else 
      error('cvclass: scale options are c, cs or cp only')
  end

  
  func2='class'+func;
  execstr(msprintf('[resclass]= %s (x_model.d,y,lv);',func2));

  // resclass contient deux champs: .scores et .rloadings



  // ------------------
  // VALIDATION CROISEE 
  // ------------------
  
  
  // 2 options: avec ou sans repetition
  
  if max(size(split0))==2 then 
      nb_repet=split0(2);
  else
      nb_repet=1; 
  end;

  // but= recuperer conf_cv_all
  if func=='plsfda' then                                // plsfda (3d)
      conf_cv_all=list();
      for i=1:lv2;
         conf_cv_all(i)=zeros(nbcl,nbcl,lv(1),nb_repet); 
      end
  else                                                      // fda, plsda 
      conf_cv_all=list();
      for i=1:lv2;
         conf_cv_all(i)=zeros(nbcl,nbcl,nb_repet); 
      end
  end

  // ====== boucle sur les repetitions ====================================
  for m=1:nb_repet;    
      
      // reinitialisation de conf_cv a chaque tour de la boucle
      if func=='plsfda' then                                // plsfda (3d)
          conf_cv=list();
          for i=1:lv2;
              conf_cv(i)=zeros(nbcl,nbcl,lv(1)); 
          end
      else                                                      // fda, plsda 
          conf_cv=list();
      
          for i=1:lv2;
              conf_cv(i)=zeros(nbcl,nbcl); 
          end
      end 
      
     // boucle 10x ---------------------------------------------------------------
     for k=1:10;    // pour faire tourner 10x la validation croisée avec les blocs
                    // indispensable car si tous les ech. d'une classe sont dans un seul jeu (test ou cal)
                    // ils ne peuvent pas être prédits
                    // avec 10 répèts: on a + de chances d'une dispersion de tous les échantillons d'une même classe
                    // pour partie dans calx et pour partie dans testx
                
        // détermination des blocs
        if prod(size(split0))<=2 then
            flag = 0;                  // split est une valeur ou un vecteur de 2 éléments
            split = min([split0(1),mx]);
            nbpred = floor(mx /split);
            nbpred=max(nbpred,1);                   // pour ne pas avoir 0...
            [k,mel] = gsort(rand(mx,1),'g','i');    //ordre croissant
            x = x(mel,:);
            y = y(mel,:);
        else 
            flag = 1;                 // split est un vecteur 
            lot = split0;
            clear("split");
            split = max(lot);
            mel = 1:mx;
        end;

        // test de chaque paquet
        for i = 0:split-1;
            if flag==0 then
                deb = i*nbpred+1;
                fin = deb+nbpred-1;
                calx  =  [x(1:deb-1,:);x(fin+1:mx,:)];
                testx =  x(deb:fin,:);
                caly  =  [y(1:deb-1,:);y(fin+1:mx,:)];
                testy =  y(deb:fin,:);
            elseif flag==1 then 
                testx = x(find(bool2s(lot==i+1)),:);  
                calx  = x(find(bool2s(lot~=i+1)),:);
                testy = y(find(bool2s(lot==i+1)),:);
                caly  = y(find(bool2s(lot~=i+1)),:);
            end;
        
            // 12juil18 modil pour correspondre à plsfda (lv + dv), plsda (lv) et fda (dv)
            lv3=lv;
            lv3($)=lv_max;
                                                            //21aout23 remplace varargin(:) par metric, sclae, class,seuil
            st = 'cf=classtestxx(calx,caly,testx,testy,lv3,func,metric, scale, class, seuil);';   // 12jui lv_max remplace par lv3
            execstr(st);     

            // mis dans conf_cv  
            for j=1:lv2;
                conf_cv(j)=conf_cv(j)+cf(j).d;  
            end;
        end;
     end;       
     // fin de boucle 10x  ----------------------------------------------------------------
  
     for i=1:lv2;
         if func=='plsfda' then 
             conf_cv_all(i)(:,:,:,m)=conf_cv(i);
         else
             conf_cv_all(i)(:,:,m)=conf_cv(i);
         end
     end;

     // conf_cv:               liste lv2,éléments, chacun = matrice (nbcl x nbcl x lv(1))
     // conf_cv_all:           liste lv2 éléments, chacun = matrice (nbcl x nbcl x lv(1) x nb_repet)
     
     
  end;     
  
  // fin de la boucle sur les repetitions =================================================
  
  // mise de conf_cv_all sous forme de div 
  conf_cv_all2=list();
  
  for i=1:lv2;
      conf_temp.d=conf_cv_all(i);
      conf_temp.i='pred_'+ string([1:nbcl]');     
      if func=='plsfda' then                     // plsfda
          conf_temp.v.v1='ref_'+ string([1:nbcl]');
          conf_temp.v.v2='LV_'+ string([1:lv(1)]');
          if nb_repet>1 then
              conf_temp.v.v3='noCVrepet_'+ string([1:nb_repet]');
          end
      else                                      //fda, plsda 
          if nb_repet>1 then
              conf_temp.v.v1='ref_'+ string([1:nbcl]');
              conf_temp.v.v2='noCVrepet_'+ string([1:nb_repet]');
          else
              conf_temp.v='ref_'+ string([1:nbcl]');
          end
      end
      conf_temp=div(conf_temp);
      conf_cv_all2(i)=conf_temp;
  end
  


  // reconstruction de conf_cv à partir de conf_cv_all
  
  // recopie des lignes avant la boucle "repetitions"
    if func=='plsfda' then                                // plsfda (3d)
        conf_cv=list();
            for i=1:lv2;
                conf_cv(i)=zeros(nbcl,nbcl,lv(1)); 
            end
    else                                                      // fda, plsda 
        conf_cv=list();
      
        for i=1:lv2;
            conf_cv(i)=zeros(nbcl,nbcl); 
        end
    end 
 
    // remplissage de conf_cv
    for i=1:lv2;
        if func=='plsfda' then 
            conf_cv_temp=zeros(nbcl,nbcl,lv(1));
        else
            conf_cv_temp=zeros(nbcl,nbcl);
        end
        
        for m=1:nb_repet; 
            if func=='plsfda' then 
                conf_cv_temp=conf_cv_temp+conf_cv_all(i)(:,:,:,m);
            else
                conf_cv_temp=conf_cv_temp+conf_cv_all(i)(:,:,m);
            end
        end 
        conf_cv(i)=conf_cv_temp;
    end
    
  
  
  // conf_cv mise en pourcentage; erreurs de CV globales et par classe
  if func=='plsfda' then
      ep=zeros(lv2,lv(1));
      ep_classes=zeros(nbcl,lv2,lv(1));
  
      for i=1:lv2; 
          for j=1:lv(1);
              [confcv_i,ep_i,ep_classes_i]=conf_obs2pcent(conf_cv(i)(:,:,j));
              ep(i,j)=ep_i;
              ep_classes(:,i,j)=ep_classes_i;
          end;
      end;
   else                      // plsda 
      ep=zeros(lv2);
      ep_classes=zeros(nbcl,lv2);
  
      for i=1:lv2; 
          [confcv_i,ep_i,ep_classes_i]=conf_obs2pcent(conf_cv(i)(:,:));
          ep(i)=ep_i;
          ep_classes(:,i)=ep_classes_i;
      end;   
   end

  
  // calcul des erreurs de classification détaillés en CV    2mars21
  if func=='plsfda' then
      err_cv_detail=zeros(lv2,nb_repet,lv(1));
      for i=1:lv2;
          for j=1:lv(1);
              conf_cv_temp=conf_cv_all(i);    //conf_cv_temp: (nbcl x nbcl x lv(1) x nb_repet)
              for m=1:nb_repet;
                  error_temp=(sum(conf_cv_temp(:,:,j,m))-sum(diag(conf_cv_temp(:,:,j,m))))/sum(conf_cv_temp(:,:,j,m));
                  err_cv_detail(i,m,j)=0.1*round(1000*error_temp);    // pour avoir 1 décimale après la virgule 
              end
          end    
      end
  else
      err_cv_detail=zeros(lv2,nb_repet);
      for i=1:lv2;
          conf_cv_temp=conf_cv_all(i);    //conf_cv_temp: (nbcl x nbcl x nb_repet)
          for m=1:nb_repet;
              error_temp=(sum(conf_cv_temp(:,:,m))-sum(diag(conf_cv_temp(:,:,m))))/sum(conf_cv_temp(:,:,m));
              err_cv_detail(i,m)=0.1*round(1000*error_temp);    // pour avoir 1 décimale après la virgule 
          end
      end    
  end
  
  // calcul de la moyenne + variabilité des erreurs de classification  // 2mars 21
  if func=='plsfda' then
      err_cv_mean=zeros(lv2,lv(1));
      err_cv_std=zeros(lv2,lv(1));
      err_cv_mean_plus2stdev=zeros(lv2,lv(1));
      err_cv_mean_minus2stdev=zeros(lv2,lv(1));
      for i=1:lv2;
          for j=1:lv(1);
              err_cv_mean(i,j)=mean(err_cv_detail(i,:,j),'c')
              err_cv_std(i,j)=stdev(err_cv_detail(i,:,j),'c');
              err_cv_mean_plus2stdev(i,j)=err_cv_mean(i,j) + 1.96*err_cv_std(i,j);
              err_cv_mean_minus2stdev(i,j)=err_cv_mean(i,j) - 1.96*err_cv_std(i,j);
          end
      end
  else 
      err_cv_mean=zeros(lv2);
      err_cv_std=zeros(lv2);
      err_cv_mean_plus2stdev=zeros(lv2);
      err_cv_mean_minus2stdev=zeros(lv2);
      
      err_cv_mean=mean(err_cv_detail,'c')
      err_cv_std=stdev(err_cv_detail,'c');
      err_cv_mean_plus2stdev=err_cv_mean + 1.96*err_cv_std;
      err_cv_mean_minus2stdev=err_cv_mean - 1.96*err_cv_std;
     
  end
  
 
  // calcul de l'erreur si tirage au hasard   //2 mars 21
  if func=='plsfda' then
      err_random=ones(lv2,lv(1))*0.1*round(1000*(nbcl-1)/nbcl);
  else
      err_random=ones(lv2,1)*0.1*round(1000*(nbcl-1)/nbcl);
  end




  // SORTIES ====================================================
  // mise des hypermatrices de confusion sous forme de listes Div
 
  model.conf_cal_nobs=[];
  model.conf_cal_nobs=confus_m;  //21  aout23
  
  model.conf_cal=[];
  model.conf_cal=confus_pcent;    
  
  model.conf_cv=[];                
  model.conf_cv=conf_cv_all2

  // erreurs:   ec = (lv2,lv(1)) erreur de classification 
  //            ep=(lv2,lv(1))   erreur de prédiction
  if func=='plsfda' then
      model.err.d=zeros(lv2,5,lv(1));
      model.err.d(:,1,:)=ec; 
      model.err.d(:,2,:)=err_cv_mean;
      model.err.d(:,3,:)=err_cv_mean_minus2stdev;
      model.err.d(:,4,:)=err_cv_mean_plus2stdev;
      model.err.d(:,5,:)=err_random;
  else
      model.err.d=zeros(lv2,5);
      model.err.d(:,1)=ec; 
      model.err.d(:,2)=err_cv_mean;
      model.err.d(:,3)=err_cv_mean_minus2stdev;
      model.err.d(:,4)=err_cv_mean_plus2stdev;
      model.err.d(:,5)=err_random;
  end
  
  model.err.i='DV'+string([1:1:lv2]');
  if func=='plsfda' then
      model.err.v.v1=['error of classification, p.cent'; 'error of cross-validation, p.cent'; 'error of CV - 2stdev'; 'error of CV + 2stdev';'error if random attribution'];
      model.err.v.v2='LV_'+string([1:lv(1)]');
  else
      model.err.v=['error of classification, p.cent'; 'error of cross-validation, p.cent'; 'error of CV - 2stdev'; 'error of CV + 2stdev';'error if random attribution'];
  end
  model.err=div(model.err);
  model.err.infos=['number of dimensions';'p.cent errors of classification and/or CV'];

  model.err_glx=[];  // mis là pour être juste après model.err

  model.errbycl_cal.d=eclasses;     // (nbcl,lv2,lv(1))
  model.errbycl_cal.i='ref_'+ string([1:nbcl]');
  if func=='plsfda' then
      model.errbycl_cal.v.v1='DV'+string([1:1:lv2]');
      model.errbycl_cal.v.v2='LV'+string([1:1:lv(1)]');
  else
      model.errbycl_cal.v='LV'+string([1:1:lv2]'); 
  end
  model.errbycl_cal=div(model.errbycl_cal);
  model.errbycl_cal.infos=['number of discriminant variables';'classification error for each class'];
  
  model.errbycl_cv.d=ep_classes;      // (nbcl,lv2,lv(1))
  model.errbycl_cv.i='ref_'+ string([1:nbcl]');
  if func=='plsfda' then
      model.errbycl_cv.v.v1='DV'+string([1:1:lv2]');
      model.errbycl_cv.v.v2='LV'+string([1:1:lv(1)]');
  else
      model.errbycl_cv.v='LV'+string([1:1:lv2]'); 
  end 

  model.errbycl_cv=div(model.errbycl_cv);
  model.errbycl_cv.infos=['number of discriminant variables';'CV error for each class'];
  
  model.notclassed.d=nonclasse;           // (lv2,lv(1))
  model.notclassed.i='LV'+string([1:1:lv2]');
  if func=='plsfda' then
      model.notclassed.v='LV'+string([1:1:lv(1)]');
  else
      model.notclassed.v=['not classed, p.cent of all the observations'];
  end
  model.notclassed=div(model.notclassed);
  model.notclassed(5)=['number of discriminant variables';'p.cent of not classified observations'];
  
  model.notclassed_bycl.d=nonclasse_bycl;  // (lv2,nbcl,lv(1));
  model.notclassed_bycl.i='LV'+string([1:1:lv2]');
  if func=='plsfda' then
      model.notclassed_bycl.v.v1='ref_'+ string([1:nbcl]');
      model.notclassed_bycl.v.v2='LV'+string([1:1:lv(1)]');
  else
      model.notclassed_bycl.v='ref_'+ string([1:nbcl]');
  end
  model.notclassed_bycl=div(model.notclassed_bycl);
  model.notclassed_bycl(5)=['number of discriminant variables';'p.cent of not classified observations, for each class'];

  model.method=func;
  
  model.xcal=[];
  model.xcal=x2;
  
  model.ycal=[];
  model.ycal=y2; 

  model.scores.d=resclass.scores;          
  model.scores.i=x2.i;
  if func=='plsfda' then                                     //21aout23
      model.scores.v.v1= 'DV' + string([1:1:lv2]'); 
      model.scores.v.v2= 'LV' + string([1:1:lv(1)]'); 
  elseif model=='plsda' then 
      model.scores.v= 'LV' + string([1:1:lv2]');  
  else
      model.scores.v= 'DV' + string([1:1:lv2]');     
  end
 
  model.scores=div(model.scores);
  
  model.rloadings.d=resclass.rloadings;      
  model.rloadings.i=x2.v;
  model.rloadings.v=model.scores.v;          // note: rloadings vérifie: T=X*rloadings 
  model.rloadings=div(model.rloadings);
 

  model.classif_metric=metric;
  
  model.scale=scale;
   
  model.classif_opt=class;
 
  model.threshold=seuil;
 

  // gestion des arrondis des pourcentages: à 0,1 près 
  for i=1:lv2;
      model.conf_cal(i).d=0.1*round(10*model.conf_cal(i).d);
      // model.conf_cv(i).d=0.1*round(10*model.conf_cv(i).d);   // mis en commentaire car difficile à gérer avec les répétitions de CV
  end
  model.err.d=0.1*round(10*model.err.d);
  model.errbycl_cal.d=0.1*round(10*model.errbycl_cal.d);
  model.errbycl_cv.d=0.1*round(10*model.errbycl_cv.d);
  model.notclassed.d=0.1*round(10*model.notclassed.d);
  model.notclassed_bycl.d=0.1*round(10*model.notclassed_bycl.d);

  // modification de la sortie 'err' pour plsfda qui a 3 dimensions 
  if func=='plsfda' then
      xtemp=model.err.d;
      [a,b,c]=size(xtemp);
      if b~=5 then 
          error('cvclass: model.err.d has not a regular dimension');
      end
      xout.d=zeros(a,c,2);   // on ne garde que erreur de classification + erreur de CV
      xout.d(:,:,1)=matrix(xtemp(:,1,:),[a,c]);
      xout.d(:,:,2)=matrix(xtemp(:,2,:),[a,c]);
      xout.i=model.err.i;
      xout.v.v1=model.err.v.v2;
      xout.v.v2=["error of classification";"error of cross-validation"];
      xout=div(xout);
      model.err_glx=xout; 
  end



endfunction




