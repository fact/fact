function [res_all,vif_out,dw_out,var_x,gini_x,x_deflat_ref]=dimension_tune(model)
    
        // indicateurs d'aide au réglage de la dimension du modèle 
        // 1- sur les b-coefficients
        // 2- sur les matrices X déflatées 
        
        
        x=model.x_ref;
        y=model.y_ref;
        x2=x.d;
        y2=y.d;
        [n,q]=size(x2);
        
        model.b=div(model.b);
        model.err=div(model.err);
        
        ctr=model.center;
        b=model.b.d;
        p=model.loadings.d;
        lv=size(b,2);
        
        // 17jan21 pour correspondre à LV=0
        b=[ones(q,1)/q b];
        //b=[zeros(q,1) b];
        
        // ----------------------------------
        // application sur les b-coefficients 
        // ----------------------------------
    
        //Durbin-Watson
        dw_d=ica_durbwatmatrix_op(b,'col');
        dw.d=dw_d;
        dw.i='LV'+string([0:lv]');
        dw.v='dw-b';
        dw=div(dw);
    
        //facteur morphologique 
        mf=dimtune_morph_factor(b);        //17aout22 rajoute dimtune_
        mf.v='mf-b';
        
        //gini 
        resgini=dimtune_gini(b);           //17aout22 rajoute dimtune_
        resgini.v='gini-b';

        //norme de B
        sce=diag(b'*b);
        bnorm.d=sqrt(sce);
        bnorm.i=dw.i;
        bnorm.v='norm-b';
        bnorm=div(bnorm);
    
        // Added by DNR  9jan22
        //Variance de B
        var_B=variance(b,'r');
        var_B_res.d=var_B';
        var_B_res.i=dw.i;
        var_B_res.v='variance-b';
        var_B_res=div(var_B_res);
        // Added by DNR  9jan22
        
        // -------------------------------
        // application sur les X deflatées 
        // -------------------------------        
       
        // centrage éventuel de X
        x3=x2-ctr*ones(n,1)*model.x_mean.d';
        y3=y2-ctr*ones(n,1)*model.y_mean.d';
        
        //calcul d'une matrice déflatée pour chaque VL - méthode wold-pls  
        x_deflat_ref=zeros(n,q,lv+1);
        
        [b_ref,t_ref,p_ref]=calikpls(x3,y3,lv);
        
        xi=x3;
        x_deflat_ref(:,:,1)=x3;
        for i=1:lv;
            ti=t_ref(:,i);
            pi=p_ref(:,i);
            xi=xi-ti*pi';
            x_deflat_ref(:,:,i+1)=xi;    
        end
        
        // kmo-ref
        kmo_ref_d=zeros(lv+1,1);
        
        for i=1:lv+1;
            x_deflat_ref_i=x_deflat_ref(:,:,i);
            x_deflat_ref_i=matrix(x_deflat_ref_i,[n,q]);
            kmo_ref_i=dimtune_kaiser_meyer_olkin(x_deflat_ref_i);  //17aout22 rajoute dimtune_
            kmo_ref_d(i)=kmo_ref_i;
        end
        
        kmo_ref_res.d=kmo_ref_d;
        kmo_ref_res.i='LV'+string([0:lv]');
        kmo_ref_res.v='kmo-xdefl';
        kmo_ref_res=div(kmo_ref_res);
 
        // Added by DNR
        // Variance
        var_x=zeros(n,lv+1);
;       for i=1:lv+1;
            x_deflat_ref_i=x_deflat_ref(:,:,i);
            x_deflat_ref_i=matrix(x_deflat_ref_i,[n,q]);
            var_ref_d(:,i)=variance(x_deflat_ref_i,'c');
        end
        var_x.d=var_ref_d;
        var_x.i=x.i;
        var_x.v=kmo_ref_res.i;
        var_x=div(var_x);
        // Added by DNR  9jan22
        
        // Added by DNR  10jan22
        //gini 
        gini_x.d=zeros(n,lv+1);
        for i=1:lv+1;
            x_deflat_ref_i=x_deflat_ref(:,:,i);
            x_deflat_ref_i=matrix(x_deflat_ref_i,[n,q]);
            gini_tmp=dimtune_gini(x_deflat_ref_i');        //17aout22 rajoute dimtune_
            gini_x.d(:,i)=gini_tmp.d;
        end
        gini_x.i=x.i;
        gini_x.v='LV'+string([0:lv]');
        gini_x=div(gini_x);
        // Changed by DNR 10jan22
        
        // VIF 
        // choix de la dimension de la pls 
        rmsecv=model.err.d(:,2);
        index=find(rmsecv==min(rmsecv));
        lv_vif=max(index,3);        // au minimum 3 VL 
    
        vif_res=zeros(lv+1,q);
        
        // pour chaque niveau de déflation
        for i=1:lv+1;
            xi_vif=x_deflat_ref(:,:,i);
            vif_i=dimtune_var_inflat_factor(xi_vif,1);   //9mars21 mis 1 sur instruction de Douglas 
                                                         // mais pas clair du tout
                                                         //17aout22 rajoute dimtune_
            vif_res(i,:)=vif_i;
        end
    
        vif_out.d=vif_res;
        vif_out.i='LV'+string([0:lv]');
        vif_out.v=model.x_ref.v;
        vif_out=div(vif_out);
        vif_out=vif_out'; 
    
        // calcul de vif moyen
        vifmatrix_moy.d=mean(vif_res,'c');
        vifmatrix_moy.i='LV'+string([0:lv]');
        vifmatrix_moy.v='vif-xdefl';
        vifmatrix_moy=div(vifmatrix_moy);
    
        // Durbin-Watson sur matrice X 
        dw_res=zeros(n,lv+1);
        
        for i=1:lv+1;
            xi_dw=x_deflat_ref(:,:,i);
            dw_i=ica_durbwatmatrix_op(xi_dw,'r');  
            dw_res(:,i)=dw_i;
        end

        dw_out.d=dw_res;
        dw_out.i=x.i;
        dw_out.v='LV'+string([0:lv]');
        dw_out=div(dw_out);

        // calcul de DW moyen
        dwmatrix_moy.d=mean(dw_res,'r');     
        dwmatrix_moy.d=dwmatrix_moy.d';
        dwmatrix_moy.i='LV'+string([0:lv]');
        dwmatrix_moy.v='dw-xdefl';
        dwmatrix_moy=div(dwmatrix_moy); 

    
        // ------------
        // regroupement
        // ------------
        
       
        err.d=[stdev(y2);model.err.d(:,2)];
        err.i='LV'+string([0:lv]');
        err.v='rmsecv';
        err=div(err);
      
        if size(model.err.d,2)>2 then 					// 19oct22 compatibilite fact 1.3.1 = 1.2.5 suite a probleme de compilation des librairies C dans ubuntu18 / ubuntu20 
            err_minus2std.d=model.err.d(:,2)-1.96*model.err.d(:,3);
            err_minus2std.d=[stdev(y2);err_minus2std.d];
            err_minus2std.i='LV'+string([0:lv]');
            err_minus2std.v='rmsecv-2std';
            err_minus2std=div(err_minus2std); 
         
            err_plus2std.d=model.err.d(:,2)+1.96*model.err.d(:,3);
            err_plus2std.d=[stdev(y2); err_plus2std.d];
            err_plus2std.i='LV'+string([0:lv]');
            err_plus2std.v='rmsecv+2std';
            err_plus2std=div(err_plus2std);
    
            err_out=[err err_minus2std err_plus2std];
   
        elseif size(model.err.d,2)<=2 then
            err_out=err; 
            
        end
        
        // Changed by DNR 9jan22      
        res_all=[err_out bnorm var_B_res dw mf resgini kmo_ref_res vifmatrix_moy dwmatrix_moy];
        res_all=res_all(2:$,:);
        res_all.d=real(res_all.d);  
        
        //transposition pour faciliter le scatter plot 
        vif_out=vif_out';
        dw_out=dw_out';
        var_x=var_x';
        gini_x=gini_x';
        
        
      
endfunction
