function y = dendro_pdist (x,dtype,varargin)
    // calcule la distance 2 à 2 entre vecteurs ligne - computes pairwise distance between features
    // x : une matrice de vecteurs lignes
    //          si x est structure div (toolbox FACT), remplacer x par x.d
    //          le cas d'une hypermatrice 3D n'est pas testé
    // Optionnellemet on peut choisir un type de norme
    //      'euc' : par defaut  - norme euclidennnte
    //      'cit' : norme euclidennnte
    //      'cor' : norme euclidennnte
    //      'ham' : norme euclidennnte
    //      'jac' : norme euclidennnte
    //      'mah' : norme euclidennnte
    //      'min' : norme euclidennnte
    //      'seu' : norme euclidennnte
    //
    // Created by Yumnam Kirani - CDAC Kolkata - 5 November 2013 - IVPT3 Toolbox
    // Adaptation pour Scialb 6.0 et FACT 1.0 - Pierre Kiener - Novembre 2019
    //
    // distributed under the BSD licence
    //
    // Modification pour Scialb 6.0 - P.Kiener - Novembre 2019
    //
    // Autres sources possibles 
    // https://forge.scilab.org/index.php/p/nan-toolbox/source/tree/master/macros/nan_pdist.sci (toutes normes - en nov19 uniquement pour Scilab 5.x)
    // http://lists.scilab.org/pipermail/users/2009-November/001424.html    (seulement distance euclidienne)

    rhs=argn(2);       // Modification 11/2019
    if  rhs < 1
        error('No input is given!');
    elseif rhs>1 & ~(type(dtype)==10)
        error ('Second argument must be a string!');
    elseif (rhs>1 & type(dtype)==10) & (length(dtype)<3)
        error ('Second argument must have more than 2 characters!');
    end

    if (isempty (x))
        error ('First argument must be a non-empty matrix');
    elseif (length (size (x)) > 2)
        error ('First argument must be 1 or 2 dimensional');
    end

    if (rhs < 2)
        dtype = 'euc';
    end

    p = 2;          // default for Minkowski
    if (rhs > 2)
        p = varargin(1); 
    end

    lz=size(x,1);
    xc=[];
    yc=[];
    for i=1:lz
        xc=[xc;i*ones(lz-i,1)];
        yc=[yc;(i+1:lz)'];
    end

    x = x';
    dfx = x(:,xc) - x(:,yc);
    dtype = ascii(convstr (dtype));
    dtype=ascii(dtype(1:3));
    select (dtype)
    case 'che' then
        dfx = x(:,xc) - x(:,yc);
        y = max (abs (dfx), 'r');

    case 'cit' then
        y = sum (abs (dfx), 1);

    case 'cor' then
        P = nansum(x,1);
        Q=nansum(x.^2,1);
        NM = bool2s(~isnan(x)')*bool2s(~isnan(x));
        x(x~=x) = 0; 
        N=sum(x>0,1);
        CM = x'*x;
        M  = P./N;
        r  = (Q./N- M.*M); 
        xcor = CM./NM - M'*M;
        xcor = xcor./sqrt(r'*r);
        y = 1 - xcor (sub2ind (size (xcor), xc, yc))';   

    case 'cos' then
        pdx = x(:,xc) .* x(:,yc);
        wts = nansum (x(:,xc).^2, 1) .* nansum (x(:,yc).^2, 1);
        y = 1 - sum (pdx,'r') ./ sqrt (wts);

    case 'euc' then
        y = sqrt (nansum (dfx.^2, 1));

    case 'ham' then
        dfx =  (x(:,xc) - x(:,yc))~=0;
        y = sum (dfx, 1) / size (x,1);

    case 'jac' then
        dfx =  (dfx)~=0;
        wts = x(:,xc) | x(:,yc);
        y =1- sum (dfx & wts, 1) ./ sum (dfx | wts, 1);

    case 'mah' then

        x=x';
        mu=repmat(mean(x,'r'),lz,1); //ori x
        vx=(x-mu)'*(x-mu)/(lz-1);
        wts=inv(vx);
        //     disp(wts-wta);
        y = sqrt (sum ((wts * dfx) .* dfx, 1));

    case 'min' then

        y = (sum ((abs (dfx)).^p, 1)).^(1/p);

    case 'seu' then
        x=x';
//        wts = inv (diag (nanvar(x,1))); //ori x
        wts = inv (diag (nanstdev(x,1).^2)); //ori x        // Modification 11/2019 pour Scilab 6.0 tant que nanvar n'existe pas

    else
        error('Unknown Distance type!');
    end

endfunction

