function [res] = epo(xg,classes_echant,classes_perturb,xtest,ytest,split,lv,varargin)
    
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  if argn(2)==7 then
   centrage=1;
  elseif argn(2)==8 then
   centrage=varargin(1);
  end
   
  [dmatrix] = pop_dextract('epo',xg,classes_echant,classes_perturb);
  res=pop_dtune(dmatrix,20,xg,classes_echant,xtest,ytest,split,'pls', centrage,lv)   
       //maxdim=20 et method='pls'(arbitraire)

   
endfunction
    
    
