function [T,perm] = dendro_dendrogram(Z,varargin)
    // [T,perm] = dendrogram(Z,p,'orientation',[..],'colorthreshold',[..],'obslabels',[..],'reorder',[..])
    //
    // Crée le dendrogramme d'une structure obtenue par la fonction linkage
    //      Z : structure des données obtenue par la fonction linkage
    // Options possibles de paramétrage , le symbole[...] désignant le paramètre de contrôle :
    //      p : nombre de cluster / branches visibles à partir du somment
    //              par défaut 30
    //              au plus le nombre de données / feuilles initiales 
    //      orientation du sommet du dendrogramme
    //              't' : top - haut
    //              'b' : bottom - bas
    //              'r' : right - droite
    //              'l' : left - gauche
    //      colorisation de l'arbre :
    //              colorthreshold : la valeur seuil en dessous de laquelle l'arbre change de couleur
    //      obslabels : code des labels 
    //              fournir un vecteur de string avec autant de codes de labels que de feuilles, par exemple "x.i" pour un structure "div"
    //              ces labels seront affichés en fonction du dendrogramme
    //      reorder : ordre des feuilles
    //              fournir un vecteur donnant l'ordre des feuilles
    //      
    // Created by Yumnam Kirani - CDAC Kolkata - 5 November 2013 - IVPT3 Scialb Toolbox - BSD licence
    // Adaptation pour Scialb 6.0 et FACT 1.0 - Pierre Kiener - Novembre 2019
    // Distributed under the CeCILL-C license

    rhs=argn(2);  // Modification Scilab 6.0
    m = size(Z,1)+1;

    // par défaut : seule la structure de données est fournie et 30 branches
    if rhs < 2
        p = 30;
    end

    // nombre de branches du dendrogramme fixé
    if rhs == 2
        p = varargin(1);
        p=min(p,size(Z,"r"));
    end

    // Paramètres par défaut
    orientation = 't';
    horz = %f;
    colr = %f;
    obslabels = [];
    threshold = 0.7 * max(Z(:,3));
    leaforder = [];

    // Plus de paramètres que la hauteur du dendrogramme
    if rhs > 2
        if or(type(varargin(1))==[1,5,8]) //isnumeric
            p = varargin(1);
            offset = 1;
        else
            p = 30;
            offset = 0;
        end
        p=min(p,size(Z,"r"));
        
        // Validation du nombre d'arguments optionnels
        if modulo(rhs - offset,2)== 0
            error('Incorrect number of arguments to DENDROGRAM.');
        end

        // Traitement de chaque argument optionnel
        okargs = ['orientation' 'colorthreshold' 'obslabels','reorder'];    // Correction syntax pour Scilab 6.0 { => [
        for j=(1 + offset):2:rhs-2
            pname = varargin(j);
            pval = varargin(j+1)
            k = find(okargs==convstr(pname,'l')); 
            if isempty(k)
                error(msprintf('Unknown parameter name:  %s.',pname));   // Correction syntax pour Scilab 6.0
            elseif length(k)>1
                error(msprintf('Ambiguous parameter name:  %s.',pname));    // Correction syntax pour Scialb 6.0
            else
                select(k)

                case 1 then // orientation
                    if ~isempty(pval)
                        if type(pval)==10 // ischar
                            orientation = convstr(pval(1),'l');
                        else
                            orientation = 0;    // bad value
                        end
                    end
                    if ~or(orientation==['t','b','r','l'])    // Correction syntax pour Scilab 6.0 { => [
                        orientation = 't';
                        warning('Unknown orientation specified, ''top'' is used by default.');
                    end
                    if or(orientation==['r','l'])    // Correction syntax pour Scilab 6.0 { => [
                        horz = %t;
                    else
                        horz = %f;
                    end

                case 2  then // colorthreshold
                    colr = %t;
                    if or(type(pval)==10)
                        if strcmp(convstr(pval,'l'),'default')      // Correction syntax en  Scilab 6.1
                            warning('Unknown threshold specified, using default');
                        end
                    end
                    if or(type(pval)==1)
                        threshold = pval;
                    end

                case 3  then// labels
                    if or(type(pval)==10)
                        pval = cellstr(pval);
                    end
                    if ~iscellstr(pval)
                        error('Value of ''labels'' parameter is invalid');
                    end
                    if ~isvector(pval) | size(pval,'*')~=m
                        error('Must supply a label for each observation.');
                    end
                    obslabels = pval(:);

                case 4 then // leaf order
                    if ~isvector(pval) | size(pval,'*') ~=m
                        error('leaforder is not a valid permutation.');
                    end
                    leaforder = pval;

                end
            end
        end
    end

    Z = transz(Z);

    T = (1:m)';

    if (m > p) & (p ~= 0)

        Y = Z((m-p+1):$,:);         // get the last nodes

        R = unique(Y(:,1:2)); 
        Rlp = R(R<=p);
        Rgp = R(R>p);

        W(Rlp) = Rlp;                 // use current node number if <=p
        W(Rgp) = setdiff(1:p, Rlp)';   // otherwise get unused numbers <=p
        W = W(:);
        T(R) = W(R);

        // Assign each leaf in the original tree to one of the new node numbers
        for i = 1:p
            c = R(i);
            T = clusternum(Z,T,W(c),c,m-p+1,0); // assign to its leaves.
        end

        // Create new, smaller tree Z with new node numbering
        Y(:,1) = W(Y(:,1));
        Y(:,2) = W(Y(:,2));
        Z = Y;

        m = p; 
    end

    A = zeros(4,m-1);
    B = A;
    n = m;
    X = 1:n;
    Y = zeros(n,1);
    r = Y;

    W = zeros(size(Z,1), size(Z,2));
    W(1,:) = Z(1,:);

    nsw = zeros(n,1); rsw = nsw;
    nsw(Z(1,1:2)) = 1;
    rsw(1) = 1;
    k = 2; s = 2;

    while (k < n)
        i = s;
        nz=nsw(Z(i,1:2));
        anz=mtlb_any(nz);
        while rsw(i) | ~anz
            if rsw(i) & i == s 
                s = s+1; 
            end
            i = i+1;
            nz=nsw(Z(i,1:2));
            anz=mtlb_any(nz);
        end

        W(k,:) = Z(i,:);
        nsw(Z(i,1:2)) = 1;
        rsw(i) = 1;
        if s == i
            s = s+1; 
        end
        k = k+1;
    end

    g = 1;

    for k = 1:m-1 // initialize X
        i = W(k,1);
        if ~r(i),
            X(i) = g;
            g = g+1;
            r(i) = 1;
        end
        i = W(k,2);
        if ~r(i),
            X(i) = g;
            g = g+1;
            r(i) = 1;
        end
    end

    // if a leaf order is specified use it else unchanged
    if ~isempty(leaforder)
        [dummy, X] = mtlb_sort(leaforder); 
    end
    [u,perm]=mtlb_sort(X); 

    // Labellisation des branches feuilles alias "singletons" - Modification importante du code original
    label = string(perm');              // par défaut label en code numérique
    if ~isempty(obslabels)              // si liste de label fourni
        singletons = find(histc(1:m,T,%F)==1);   // respect syntaxe de histc dans Scilab 6.x
        for j=1:size(singletons,"*")            // pour feuilles visible, numérotation avec leur code
            sj = singletons(j);
            obslabels{sj};
            label(find(perm==sj)) = obslabels{sj}
        end
    end


    // set up the color
    theGroups = 1;
    groups = 0;
    cmap = [0 0 1];

    if colr
        groups = sum(Z(:,3)< threshold);
        if groups > 1 & groups < (m-1)
            theGroups = zeros(m-1,1);
            numColors = 0;
            for count = groups:-1:1
                if (theGroups(count) == 0)
                    P = zeros(m-1,1);
                    P(count) = 1;
                    P = colorcluster(Z,P,Z(count,1),count);
                    P = colorcluster(Z,P,Z(count,2),count);
                    numColors = numColors + 1;
                    theGroups(mtlb_logical(P)) = numColors;
                end
            end
            cmap = jetcolormap(numColors);
            cmap($+1,:) = [0 0 0]; 
        else
            groups = 1;
        end
    end  

    col = zeros(m-1,3);

    for n = 1:(m-1)
        i = Z(n,1); j = Z(n,2); w = Z(n,3);
        A(:,n) = [X(i) X(i) X(j) X(j)]';
        B(:,n) = [Y(i) w w Y(j)]';
        X(i) = (X(i)+X(j))/2; Y(i)  = w;
        if n <= groups
            col(n,:) = cmap(theGroups(n),:);
        else
            col(n,:) = cmap($,:);
        end
    end

    // Visualisation graphique
    ymin = min(Z(:,3));
    ymax = max(Z(:,3));
    margin = (ymax - ymin) * 0.05;
    n = size(label,1);

    clf;
    drawlater()
    col=floor(col*255)
    
    if(~horz)   // Dendrogramme vertical
        for count = 1:(m-1)
            plot2d(A(:,count)',B(:,count)',color(col(count,1), col(count,2),col(count,3)));
        end
        lims = [0 m+1 max(0,ymin-margin) (ymax+margin)];
        a=gca();
        a.data_bounds=[0.5 max(0,ymin-margin); n+0.5 ymax+margin];
        ntks=a.x_ticks;
        ntks(2)=[1:size(label,1)]';
        ntks(3)=label;
        a.x_ticks=ntks;
        a.auto_scale="off";
        a.box="off";
        mask = mtlb_logical([0 0 1 1]); 
        if orientation=='b'
            a=gca();
            a.x_location = "top";
            a.axes_reverse = ["off","on","off"]
        end 

    else   // Dendrogramme horizontal
        for count = 1:(m-1)
            plot(B(:,count),A(:,count), color(col(count,1), col(count,2),col(count,3)));
        end

        lims = [max(0,ymin-margin) (ymax+margin) 0 m+1 ];
        a=gca();
        a.data_bounds=[0.5 max(0,ymin-margin); n+0.5 ymax+margin];
        ntks=a.y_ticks;
        ntks(2)=[1:size(label,1)]';
        ntks(3)=label;
        a.y_ticks=ntks;
        a.auto_scale="off";
        a.box="off";
        mask = mtlb_logical([1 1 0 0]);
        if orientation=='l'
            a=gca();
            a.x_location = "top";
            a.axes_reverse = ["on","on","off"];
            a.y_location="right"
        end

    end
    if margin==0
        if ymax~=0
            lims(mask) = ymax * [0 1.25];
        else
            lims(mask) = [0 1];
        end
    end
    a.tight_limits = "on";
    a.data_bounds = [lims(1) lims(3);lims(2),lims(4)];
    drawnow()
endfunction
// ---------------------------------------
function T = clusternum(X, T, c, k, m, d)
    // assign leaves under cluster c to c.

    d = d+1;
    n = m; flag = 0;
    while n > 1
        n = n-1;
        if X(n,1) == k // node k is not a leave, it has subtrees
            T = clusternum(X, T, c, k, n,d); // trace back left subtree
            T = clusternum(X, T, c, X(n,2), n,d);
            flag = 1; break;
        end
    end

    if flag == 0 & d ~= 1 // row m is leaf node.
        T(X(m,1)) = c;
        T(X(m,2)) = c;
    end
endfunction 
// ---------------------------------------
function T = colorcluster(X, T, k, m)
    // find local clustering

    n = m; 
    while n > 1
        n = n-1;
        if X(n,1) == k // node k is not a leave, it has subtrees
            T = colorcluster(X, T, k, n); // trace back left subtree
            T = colorcluster(X, T, X(n,2), n);
            break;
        end
    end
    T(m) = 1;
endfunction
// ---------------------------------------
function Z = transz(Z)

    m = size(Z,1)+1;

    for i = 1:(m-1)
        if Z(i,1) > m
            Z(i,1) = traceback(Z,Z(i,1));
        end
        if Z(i,2) > m
            Z(i,2) = traceback(Z,Z(i,2));
        end
        if Z(i,1) > Z(i,2)
            Z(i,1:2) = Z(i,[2 1]);
        end
    end
endfunction 

function a = traceback(Z,b)

    m = size(Z,1)+1;

    if Z(b-m,1) > m
        a = traceback(Z,Z(b-m,1));
    else
        a = Z(b-m,1);
    end
    if Z(b-m,2) > m
        c = traceback(Z,Z(b-m,2));
    else
        c = Z(b-m,2);
    end

    a = min(a,c);
endfunction 
