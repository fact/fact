function[bcoeffs,tscores,ploads,dof,rloadings]=calridge(x,y,kvalues)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  sci_version=strtod(part(getversion(),[8 ;9 ;10]));
  if sci_version < 6 then
     stacksize('max');
  end
 
  [n,p]=size(x);

  [aux1,aux2]=size(kvalues);
  if aux1==1 then 
     kvalues=kvalues';
     aux1=aux2;
     aux2=1;
  elseif aux1~=1 & aux2~=1 then error('the k values do not form a vector')
  end
  
  bcoeffs=zeros(p,aux1);  
  tmp=x'*y;
  xpx=x'*x;  

  for i=1:aux1;
  bcoeffs(:,i)=pinv((xpx+ kvalues(i)*eye(p,p)))*tmp;
  end


  tscores=[];
  ploads=[];
  rloadings=[];
  
  
  if kvalues==0 then
    dof=0                             // cas de la MLR
  else 
    dof=ones(max(size(kvalues))); 
  end

endfunction

