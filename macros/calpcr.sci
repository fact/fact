function[b,t,ploads,dof,R]=calpcr(x,y,nf)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  sci_version=strtod(part(getversion(),[8 ;9 ;10]));
  if sci_version < 6 then
     stacksize('max');
  end

  [n,p]=size(x);
  res=pcana(x,0,0);

  T_coord=res.scores.d(:,1:nf)
  P_eigenvec2=res.eigenvec.d

  b=zeros(p,nf);
  for i=1:nf;
     b(:,i)= P_eigenvec2(:,1:i)*inv(T_coord(:,1:i)'*T_coord(:,1:i))*T_coord(:,1:i)'*y;
  end;

  t=T_coord;
  ploads=P_eigenvec2(:,1:nf);

  dof=[1:1:nf]';
  
  R=ploads;      // 25fev22
 
endfunction
