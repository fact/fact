function [model] = covsel_mlr(xin,yin,split,lv,centr);
//  [sec,secv,pred,num,blocks,crbvar,crbcov,xdef,ydef] = covselmlrcv(x,y,split,lv,scale)
//
// selection + MLR sur lv variables, suivi d'une validation croisee 

//   x     : data en entree (nxp)
//   y     : variable a predire (nxq)
//   split : 
//      if a scalar, ns blocks are randomly generated
//      if a row vector (n rows), it must contain the number of the block for each
//      individual
//      if line vector (2 columns), repeated random CV. split(1) =
//      repetitions, spli(2) = number of blocks
//      if a matrix, contains the disjonctive encoding of the blocks to be
//      used (0 : cal, 1 : test). In particular, the output argument
//      'blocks' of a first run can be inputed on a second run to force the
//      use of the same splitting structure
//   lv    : nombre de variables a garder
//  scale : integer indiquant le scaling des donnees (1 par defaut) :
//               0 : aucun, 1 : centrage, 2 : autoscale
//   pred : predictions (une colonne par nombre de variables gardees)
//   secv : erreur de cv en fonction du nbre de variables
//   sec  : erreur de calib en fonction du nbre de variables
//   num  : numeros de variables gardees : lignes 1 à q, dans l'ordre ideal
//             pour chaque reponse ; dernière ligne, dans l'ordre
//             multi-reponses
//   blocks : the blocks used by the cross validation
//   crbvar : evolution des variances cumulees de X (1ere colonne) et de Y
//             (2eme) 
//   crbcov : courbes de covariance (au carré)
//   xdef, ydef : matrices de données après projections successives (matrices 3D)

    // Initialisation
    xin2=div(xin);
    yin2=div(yin);
    
    lv = min(lv,rank(xin2.d)); 
    x_mean=mean(xin2.d,'r')';
    y_mean=mean(yin2.d,'r')'; 
    
    // gestion du centrage 9jan19
    if argn(2)<5 then
        centr=1;                // par défaut: on centre 
    end
    
    if centr==1 then
        xin2=centering(xin2);
        yin2=centering(yin2);
    end
    
    x=xin2.d;
    y=yin2.d;
    [nx,px] = size(x);
    [ny,py] = size(y);
    if nx ~= ny
        error('Le nombre d''individus ne correspond pas')
    end


    // centrage des données x pour covsel uniquement -> z 
    ztemp=centering(x); 
    z=ztemp.d;

    // centrage-réduction de y pour covsel uniquement -> t
    t_cent=centering(y);
    t_std=standardize(t_cent);
    t=t_std.d;

    // variance (inertie) totale
    vartot=[trace(z'*z) trace(t'*t)];
  
    z0=z; // 8jan19; pour garder la valeur de z 
    t0=t; // 8jan19


    // initialisation
    num=zeros(lv,1);                      
    crbvar=zeros(lv,2);

    for i=1:lv

        //crbcov(i,:)=diag( z'*t*t'*z )';
    
        // recherche max de covariance
        [cm,j] = max( sum( (z'*t).^2 ,2) );  // cm=valeur, j=indice
    
        v = z(:,j);

        z = z - (v*v')./(v'*v)*z;
        crbvar(i,1) = (vartot(1)-trace(z'*z))/vartot(1);
    
        t = t - (v*v')./(v'*v)*t;
        k=v'*t; 
        crbvar(i,2) = (vartot(2)-trace(t'*t))/vartot(2);
        num(i)=j;
    end;

    nm=[zeros(py,lv);num'];

    for k=1:py;
        //ind = covsel(z(:,num),y(:,k),length(num)); //8jan19 
        ind = covsel(z0(:,num),t0(:,k),length(num));
        nm(k,:)=num(ind);
    end;
    
    // preparation des sorties
    vars_sel=['y_col' + string([1:lv]')];
    
    if py>1 then
        err.d=zeros(2,lv,py);
        err.i=['rmsec';'rmsecv'];
        err.v.v1=vars_sel;
        err.v.v2=yin2.v;                // noms des variables de y
        err=div(err);
        model.err=err;

        ypredcv.d=zeros(ny,lv,py);
        ypredcv.i=xin2.i;
        ypredcv.v.v1=vars_sel;
        ypredcv.v.v2=yin2.v;
        ypredcv=div(ypredcv);
        model.ypredcv=ypredcv;
    
        b.d=zeros(px,lv,py);
        b.i=xin2.v;
        b.v.v1=vars_sel;
        b.v.v2=yin2.v;
        b=div(b);
        model.b=b;
    
    elseif py==1 then
        err.d=zeros(2,lv);
        err.i=['rmsec';'rmsecv'];
        err.v=vars_sel;
        err=div(err);
        model.err=err;
        
        ypredcv.d=zeros(ny,lv);
        ypredcv.i=xin2.i;
        ypredcv.v=vars_sel;
        ypredcv=div(ypredcv);
        model.ypredcv=ypredcv;
    
        b.d=zeros(px,lv);
        b.i=xin2.v;
        b.v=vars_sel;
        b=div(b);
        model.b=b;
        
    end

    model.y_ref=yin2;
  
    model.x_mean.d=x_mean;
    model.x_mean.i=xin2.v;
    model.x_mean.v='mean vector';
    model.x_mean=div(model.x_mean);
    
    model.y_mean.d=y_mean;
    model.y_mean.i=yin2.v;
    model.y_mean.v='y_mean';
    model.y_mean=div(model.y_mean);

    model.center=centr;    // 0 =rien 1 = centre 

    model.method='covsel_mlr'; 
    
    // n° de variables
    var_selected.d=nm;
    var_selected.i=[yin2.v;'y_allvars'];
    var_selected.v=vars_sel;
    var_selected=div(var_selected);
    model.var_selected=var_selected;   
 
    // calcul de la MLR
    for k=1:py                  // py = nbre de colonnes de y
        for i=1:length(num);    // num=indices des variables selectionnees  1->lv
            model_i=mlr(x(:,nm(k,1:i)),y(:,k),split, centr);
            model.err.d(:,i,k)=model_i.err.d(:,1:2)';        			 	//18aout21
            model.ypredcv.d(:,i,k)=model_i.ypredcv.d + model.center*model.y_mean.d; 	//10juil24
            disp('i');
            model.b.d(nm(k,1:i),i,k)=model_i.b.d;
        end;
    end;


endfunction
