function [b,t,ploadings,dof,rloadings] = calmlr(x,y)

  if  argn(2)~=2 then
    error('wrong number of input arguments in calmlr')
  end

  [n,q]=size(x);
  b=pinv(x'*x)*x'*y;

  t=[];
  ploadings=[];

  dof=0;
  
  rloadings=[];

endfunction
