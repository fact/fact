function [res_classpls2da] = classplsfda(x,y,lv)
    
    // distributed under the CeCILL-C license
    // copyright INRA-IRSTEA 2013    
    
    // Entrées =============================================
    // x : une matrice (n x q)  
    // y : une matrice disjonctive (n x k)   (k classes)  
    // lv: un vecteur de longueur 2  lv=[lv1;lv2]
           // lv1 = nbre de dimensions de la PLS2
           // lv2 = nbre de dimensions de la FDA
           
    // Sorties: ============================================
    // res_classplsda2.scores: une matrice (n x lv2 x lv1)
    // res_classplsda2.loadings: une matrice (q x lv2 x lv1)
    
    // maj 21 aout 23 (rajout de da 3° dimension)
    
    
    lv1=lv(1);
    lv2=lv(2);
    
    // Initialisation:
    [n,q]=size(x);   

    y=conj2dis(y);                 
    ngroups=size(y,2);
    
    // vérification du nbre de groupes et de la dimension de la FDA
    if ngroups <= lv2 then
        lv2=ngroups;
    end
    
    [b1,t1,p1,dof1,r1]=calsimpls(x,y,lv1);   // vu sur un exemple: p1=r1
                                            //  t1= x*r1
                                            // les t1 sont orthogonaux , pas les p1 ni les r1 (mais les r1 sont normés)
    res_fda=classfda(t1,y,lv2);

    t2=res_fda.scores;                      // les t2 sont orthogonaux 
    r2=res_fda.rloadings;                   // t2=t1*r2 
                                            // donc: t2= x*r1*r2 
   
    // calcul de tous les loadings 
    [n,n1]=size(t2);
    q=size(b1,1);
    scores_all=zeros(n,n1,lv1)
    rloadings_all=zeros(q,n1,lv1);
    for i=1:lv1;
        rloading_temp=r1(:,1:i)*r2(1:i,:);
        rloadings_all(:,:,i)=rloading_temp;
        scores_all(:,:,i)=x*rloading_temp;
    end
   
   
                                        // A CONTINUER DANS LES AUTRES FONCTIONS 
    // Sorties:                         // modifie le 18aout23 
                                        // avant on n'avait que scores + loadings pour lv1 
                                        // maintenant: pour 1 à lv1
    // res_classpls2da.scores=t2;
    // res_classpls2da.rloadings=r1*r2
    res_classpls2da.scores=scores_all;
    res_classpls2da.rloadings=rloadings_all;
    
endfunction
