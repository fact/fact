function [model,rmsec,rmsecv,b,ypredcv] = nnpls(xi,yi,split,lv,cent)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary) 
  //-----------------------------------------------------------------
  x=div(xi);
  y=div(yi);

  if x.i~= y.i then warning('labels in x and y do not match')
  end
  
  if argn(2)==4 then  // par défaut: centrage
      cent=1;
  end;
  
  [model] = cvreg(x,y,split,'nnpls',cent,lv);
    
  rmsec=model.err.d(:,1);
  rmsecv=model.err.d(:,2);
  b=model.b.d;
  ypredcv=model.ypredcv.d;
 


endfunction


