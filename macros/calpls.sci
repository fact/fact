function [b,t,p,dof,w] = calpls(x,y,nf0)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
  
  nf = min(rank(x),nf0);

  t=zeros(size(x,1),nf0);
  p=zeros(size(x,2),nf0);
  w=zeros(size(x,2),nf0);
  c=zeros(nf0,1);
  b=zeros(size(x,2),nf0);
  dof=zeros(nf0,1);

  // standard (Wold's) algorithm 
  
  w(:,1)=x'*y;
  w(:,1)=w(:,1)/sqrt(w(:,1)'*w(:,1));
  t(:,1)=x*w(:,1);
  c(1)=y'*t(:,1)/(t(:,1)'*t(:,1));
  p(:,1)=x'*t(:,1)/(t(:,1)'*t(:,1));
  x=x-t(:,1)*p(:,1)';
  b(:,1)=w(:,1)*inv(p(:,1)'*w(:,1))*c(1);
  
  for ind=2:nf;
  w(:,ind)=x'*y;
  w(:,ind)=w(:,ind)/sqrt(w(:,ind)'*w(:,ind));
  t(:,ind)=x*w(:,ind); 
  c(ind)=y'*t(:,ind)/(t(:,ind)'*t(:,ind));
  p(:,ind)=x'*t(:,ind)/(t(:,ind)'*t(:,ind));
  x=x-t(:,ind)*p(:,ind)';
  b(:,ind)=w(:,1:ind)*inv(p(:,1:ind)'*w(:,1:ind))*c(1:ind);
  end

  dof(1:nf)=[1:1:nf]';
 
 
  ndiff=nf0-nf;  
  if ndiff>1 then
      b(:,$-ndiff+1:$)=b(:,nf)*ones(1,ndiff);
      t(:,$-ndiff+1:$)=t(:,nf)*ones(1,ndiff);
      p(:,$-ndiff+1:$)=p(:,nf)*ones(1,ndiff);
      w(:,$-ndiff+1:$)=w(:,nf)*ones(1,ndiff);
      dof($-ndiff+1:$,1)=dof(nf,1)*ones(1,ndiff);
  end
  
  // Rappel: 
  // le r de De Jong vérifie: t=x*r
  // pour le recalculer:
  // t=x*w*inv(p'*w) donc r=w*inv(p'*w)
  

endfunction
