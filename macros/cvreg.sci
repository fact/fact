function [model] = cvreg(xi,yi,split,func,cent,varargin)


  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013


  //------------------------------
  // linear vs. non-linear models
  //------------------------------
  if part(func,[1 2])=='nn' then
      flag='NL';
  else
      flag='L';
  end


  //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary) 
  //-----------------------------------------------------------------
  [x]=div(xi);
  [y]=div(yi);
  if x.i~= y.i then warning('labels in x and y do not match')
  end
  
  
  // --------------
  // Initialization
  // --------------      
  [n,p]=size(x.d);   
  x_mean=mean(x.d,1);
  y_mean=mean(y.d,1);    
  
  

  if flag=='L' then
        [rmsecv,ypredcv,dof_nul,lot,rmsecv_stats] = cvxx( x.d, y.d,split,func,cent,varargin(:));
        [rmsec,predU,bsU,last_argout] = caltestxx(x.d,y.d,x.d,y.d,func,cent,varargin(:));
        dof=last_argout.dof;
  elseif flag=='NL' then
        [rmsecv,ypredcv] = cvxx(x.d, y.d,split,func,cent,varargin(:));
        [rmsec,predU,bsU,last_argout] = caltestxx(x.d,y.d,x.d,y.d,func,cent,varargin(:));
        rloads=last_argout.rloads;
        wh=last_argout.wh;
        wo=last_argout.wo;
        dof=[];
  end
  
  // -------------------
  // degrees of freedom
  // -------------------
  lv=size(rmsec,1);
  if flag=='L' then                                                             // 24fev22
    rmsec=sqrt(rmsec.^2 .* (n*ones(lv,1)) ./  ((n-cent)*ones(lv,1)-dof)); 
  elseif flag=='NL' then 
    rmsec=sqrt(rmsec.^2 .* (n*ones(lv,1)) ./  ((n-cent)*ones(lv,1)));
    // si dof pour ANN est connu il faudra le rajouter 
  end 
   


  // ------
  // Output
  // ------

/////// à faire par la fonction calpls, calpcr, calmlr, etc.

  if flag=='L' then
     namelv=string([1:size(rmsec,1)]');     
     namelv='LV'+namelv;
  elseif flag=='NL' then
     namelv='LV'+string(5);
     for i=1:size(rmsec,1)-1;    // 
         namelv=[namelv;'LV'+string(5+5*i)];
     end
  end
  
  if isdef('rmsecv_stats') then
     model.err.d=[rmsec rmsecv rmsecv_stats.stdev];      //1mars21
     model.err.i=namelv;
     model.err.v=['rmsec';'rmsecv';'rmsecv_stdev'];
  else
     model.err.d=[rmsec rmsecv];      
     model.err.i=namelv;
     model.err.v=['rmsec';'rmsecv'];
  end 
   
  model.err=div(model.err);
  model.err(5)=['number of latent variables';'RMSE']; 
   
  model.ypredcv.d=ypredcv;
  model.ypredcv.i=x.i;
  model.ypredcv.v=namelv;
  model.ypredcv=div(model.ypredcv);

  if flag=='L' & isfield(last_argout, 'bcoeffs') & last_argout.bcoeffs~=[] then
      if last_argout.bcoeffs~=[] then    // cas de calnnpls
          model.b.d=last_argout.bcoeffs;
          model.b.i=x.v;
          model.b.v=namelv; 
          model.b=div(model.b);
          model.b(5)=['initial variables';'b-regression coefficients'];
      end
  end
  
  if flag=='L' & isfield(last_argout,'tscores') & last_argout.tscores~=[] then  // modèle linéaire avec scores
    model.scores.d=last_argout.tscores;
    model.scores.i=x.i;                                 
    model.scores.v=namelv;
    model.scores=div(model.scores);
    model.scores(5)=['observations';'scores of the observations onto the loadings '];
  end
  
  if isfield(last_argout,'ploads') & last_argout.ploads ~=[] then
    model.loadings.d=last_argout.ploads;
    model.loadings.i=x.v;
    model.loadings.v=namelv;
    model.loadings=div(model.loadings);
    model.loadings(5)=['initial variables';'scores of the loadings '];
  end
  
  if isfield(last_argout,'rloads') & last_argout.rloads ~=[] then
    model.rloadings.d=last_argout.rloads;
    model.rloadings.i=x.v;
    model.rloadings.v='LV'+string([1:size(last_argout.rloads,2)]');
    model.rloadings=div(model.rloadings);
  end 
 
  if flag=='NL' then
    model.wh=wh;           
    model.wo=wo;
  end
 
  model.x_ref=[];
  model.x_ref=x;
 
  model.y_ref=[];
  model.y_ref=y;
 
  model.x_mean.d=x_mean;
  model.x_mean.i='x_mean';
  model.x_mean.v=x.v;
  model.x_mean=div(model.x_mean);
  model.x_mean(5)=['mean vector';'initial variables']
  model.x_mean=model.x_mean';
  
  model.y_mean.d=y_mean;
  model.y_mean.i='y_mean';
  model.y_mean.v='y_mean';
  model.y_mean=div(model.y_mean);
  
  model.center=cent;
  
  model.method=func;

//  model.err=div(model.err);  
//  model.err(5)=['number of latent variables';'RMSE'];
//  
//  model.ypredcv=div(model.ypredcv);
//  model.ypredcv(5)=['observations';'y predicted'];
//
//  if flag=='L' & last_argout.bcoeffs ~=[] then
//     model.b=div(model.b);
//     model.b(5)=['initial variables';'b-regression coefficients'];
//  end 
//   
//  model.x_mean=div(model.x_mean);
//  model.x_mean(5)=['mean vector';'initial variables']
//  model.x_mean=model.x_mean';
//    
//  if flag=='L' & last_argout.tscores~=[] then
//    model.scores=div(model.scores);
//    model.scores(5)=['observations';'scores of the observations onto the loadings ']
//  end
//  
//  if flag=='L' & last_argout.ploads~=[] then  
//    model.loadings=div(model.loadings);
//    model.loadings(5)=['initial variables';'scores of the loadings '];
//  end
//
  if  isdef('lot') then     //7nov22
    model.cv_structure=lot;
  end;


endfunction
