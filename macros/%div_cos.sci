function b=%div_cos(a)
    
    // sinus 
    b=a;
    b.d=cos(a.d);
    
endfunction
