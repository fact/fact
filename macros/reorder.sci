function [saisir1,saisir2]=reorder(x1,x2)
//reorder 			- reorders the data of files A1 and A2 according to their identifiers
//function [B1 B2]=reorder(A1,A2)
//
//Input arguments:
//===============
//A1, A2: SAISIR matrices in which the rows have at least some identifiers in
//common
//
//Output arguments
//================
//B1, B2: reordered matrices.
//
//This function makes it possible to realign the rows of A1 and A2, in order
//to have the identifiers corresponding.
//This is necessary for any predictive method (particularly regressions).
//The function discards the observations which are not present in A1 and A2.
//The matrix B1 corresponds to A1 and matrix B2 to A2
//Fails if A1 or A2 contains duplicate identifiers of rows.
//A2 is leader (B1 is as close as possible from the order of A2)
//
////Typical example:
////===============
////Let X and y matrices to be reordered
//[X1, y1]=reorder(X,y);
//In X1 and y1 the rows have now the same identifiers (with possibly some
//lost of observations).
////
//If the function fails because some identifiers are in duplicates, use the
//function "check_names" to identifies these duplicated identifiers, and remove some of them
//

s1=div(x1);
s2=div(x2);

if typeof(s1)~='div' | typeof(s2)~='div' then 
    error('the input data should be DIV structures')
end

s1.i=stripblanks(s1.i);
s2.i=stripblanks(s2.i);
aux1=unique(s1.i);
aux2=unique(s2.i);
saisir1=[];
saisir2=[];
  
if(size(s1.i,1)~=size(aux1,1));
  disp('Some names are identical in the SAISIR structure, first argument, in function reorder');
  disp('Reordering impossible. Please remove duplicate names before continuing');  
  return;
end

if(size(s2.i,1)~=size(aux2,1));
  disp('Some names are identical in the SAISIR structure, second argument, in function reorder');
  disp('Reordering impossible. Please remove duplicate names before continuing');  
  return;
end

[n1,p1]=size(s1.d);
[n2,p2]=size(s2.d);

aux=zeros((n1+n2),2);
aux(1:n1,1)=(1:n1)';// to trace the modification
aux((n1+1):(n1+n2),2)=(1:n2)';
total=[s1.i; s2.i];
[total,index]=gsort(total,'lr','i');
xrank=aux(index,:);// the successive values in xrank are candidate to be reordered
nfound=0;
for i=1:((n1+n2)-1)
  aux1=xrank(i,1);
  aux2=xrank(i+1,2);  
  aux3=xrank(i,2);
  aux4=xrank(i+1,1);  
  if((aux1>0)&(aux2>0)|(aux3>0)&(aux4>0))// this configuration possibly candidate
    thisname1=total(i);
    thisname2=total(i+1);  
    if(strcmp(thisname1,thisname2)==0); //string identical
      nfound=nfound+1;
      if(aux1>0)//order s1 s2
        indice1(nfound)=aux1;
        indice2(nfound)=aux2;
      else
        indice1(nfound)=aux4;
        indice2(nfound)=aux3;
      end// order s1 s2
     end // string identical
   end // candidate
end //for i


saisir1=s1(indice1,:);   //rsel
saisir1=div(saisir1);

saisir2=s2(indice2,:);  //rsel
saisir2=div(saisir2);


endfunction
