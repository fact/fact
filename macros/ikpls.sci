function [model,rmsec,rmsecv,b,ypredcv] = ikpls(xi,yi,split,lv,cent)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  x=div(xi);
  y=div(yi);

  if x.i~= y.i then warning('labels in x and y do not match')
  end
  
  if argn(2)==4 then  // par défaut: centrage
      cent=1;
  end;
  
  [model] = cvreg(x,y,split,'ikpls',cent,lv);
    
  rmsec=model.err.d(:,1);
  rmsecv=model.err.d(:,2);
  b=model.b.d;
  ypredcv=model.ypredcv.d;
 


endfunction


