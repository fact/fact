
function path = obi_getpath()
    // Returns the path to the current module.
    //
    // Calling Sequence
    //   path = obi_getpath (  )
    //
    // Parameters
    //   path : a 1-by-1 matrix of strings, the path to the current module.
    //
    // Examples
    //   path = obi_getpath (  )
    //

    path = get_function_path("obi_getpath")
    path = fullpath(fullfile(fileparts(path),".."))

endfunction
