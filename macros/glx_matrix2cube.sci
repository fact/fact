function xcube= glx_matrix2cube(xmatrix,k,xcube_matname)
    
    // xmatrix: une matrice 2D contenant n tableaux de tailles identiques; format .csv 
    // k: nombre de tableaux 
    // xcube_matname: un nom de fichier Matlab; une chaine de caracteres; ex: 'xtemp.mat'
    
    // xcube: une matrice 3D formant un cube; format .mat 
    
    // si xcube_matname est donné:
    // la variable xcube est stockee dans le fichier xcube_matname
    
    
    
    // détermination des dimensions
    [n0,q]=size(xmatrix);
    n=n0/k;
    
    // vérification que n est entier
    if round(n)~=n then
        error('matrix2cube: error in the number of lines of the input matrix')
    end
    
    // initialisation de la matrice xcube
    xcube=zeros(n,q,k);
    
    // remplissage de xcube
    for i=1:k;
        xcube(:,:,i)=xmatrix(1+n*(i-1):n*i,:);
    end
    
    if argn(2)>2 then  
       execstr(msprintf('savematfile %s xcube -v7;',xcube_matname))
    end
    
endfunction
