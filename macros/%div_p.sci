function c=%div_p(c)
    
    // edition d'une Tlist de type 'div'
  
    // edition du .d
    if length(size(c.d))==2 then    // matrice 2 dimensions 
      disp(msprintf('  d: [%sx%s %s]', string(size(c.d,1)), string(size(c.d,2)),typeof(c.d)) )
    elseif length(size(c.d))==3 then  // hypermatrice 3D
      disp(msprintf('  d: [%sx%sx%s %s]', string(size(c.d,1)), string(size(c.d,2)),string(size(c.d,3)),typeof(c.d)) ) 
    elseif length(size(c.d))==4 then  // hypermatrice 4D
      disp(msprintf('  d: [%sx%sx%s %s]', string(size(c.d,1)), string(size(c.d,2)),string(size(c.d,3)),string(size(c.d,4)),typeof(c.d)) )   
    end
    
    // edition du .i
    disp(msprintf('  i: [%sx%s %s]', string(size(c.i,1)), string(size(c.i,2)),typeof(c.i)) )
    
    // edition du .v
    if length(size(c.d))==2 then    // matrice 2 dimensions 
      disp(msprintf('  v: [%sx%s %s]', string(size(c.v,1)), string(size(c.v,2)),typeof(c.v)) )
    elseif length(size(c.d))==3 then  // hypermatrice 3D
      disp(msprintf('  v.v1: [%sx%s %s]', string(size(c.v.v1,1)), string(size(c.v.v1,2)),typeof(c.v.v1)) )
      disp(msprintf('  v.v2: [%sx%s %s]', string(size(c.v.v2,1)), string(size(c.v.v2,2)),typeof(c.v.v2)) )
    elseif length(size(c.d))==4 then  // hypermatrice 3D
      disp(msprintf('  v.v1: [%sx%s %s]', string(size(c.v.v1,1)), string(size(c.v.v1,2)),typeof(c.v.v1)) )
      disp(msprintf('  v.v2: [%sx%s %s]', string(size(c.v.v2,1)), string(size(c.v.v2,2)),typeof(c.v.v2)) ) 
      disp(msprintf('  v.v3: [%sx%s %s]', string(size(c.v.v3,1)), string(size(c.v.v3,2)),typeof(c.v.v3)) ) 
    end
    
    

endfunction
            

