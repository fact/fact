function [conf_matrix,lv_max,metric,scale,class,seuil] = classtestxx(calx,caly,testx,testy,lv0,func,varargin)

  // maj: 28aout24

  // calx et caly: des matrices, pas des div     //12sept18
  // caly est sous forme disjonctive 

  // arguments de varargin recus de cvlass: 
  // metric:     0 = distance de mahalanobis (defaut)   - appele dans membership.sci-
  //             1 = distance euclidien normale
  // scale:      pour X; defaut='cs'                    - appele dans cvclass.sci -  
  // class:      0 (defaut) - argument non utilise      - appele dans membership.sci- 
  // seuil:      défaut=1/nbre de classes;              - appele dans classtestxx.sci = cette fonction -  


  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  // Number of arguments in function call
  [nargout,nargin] = argn(0)

  testdisj=isdisj(caly);

  nbcl = size(caly,2);

  // gestion des arguments d'entrée par défaut (copie dans cvclass)
  if nargin==6 then
     metric=0;
     scale = 'cs';
     class = 0;
     seuil = 1/nbcl;
  elseif nargin==7 then
     metric=varargin(1);
     scale='cs';
     class = 0;
     seuil = 1/nbcl;
  elseif nargin==8 then
     metric=varargin(1);
     scale=varargin(2);
     class=0;
     seuil=1/nbcl;
  elseif nargin==9 then
     metric=varargin(1);
     scale=varargin(2);
     class=varargin(3);
     seuil=1/nbcl;
  elseif nargin==10 then
     metric=varargin(1);
     scale=varargin(2);
     class=varargin(3);
     seuil=varargin(4);    
  else error('classtestxx: wrong number of input arguments in classtestxx') 
  end;
 
  if  seuil=='m' then
    seuil=1/nbcl;
  end
 
  // validation de l'option scale
  if  scale~='c' & scale~='cs' & scale~='cp'then
    error ('classtestxx: scale options are c centered, cs centered+standardized or cp centered+pareto only')
  end
 //pause
  n=size(testx,1);
  n_calx=size(calx,1);   // nbre de variables
  
  
  // ajustement du nombre de dimensions pour la FDA, la PLSDA et la PLSFDA
  lv=lv0;
  if func=='fda' | func=='plsfda' then   
        lv_max=min(lv($),nbcl-1);           // nbre classes-1 
        lv($)=lv_max;
  elseif func=='plsda' | func=='forwda'  | func=='copda' then 
        lv_max=lv0;          
  end

 
  
  // gestion de scaling: centrage / centrage-standardisation / centrage-pareto     // 28aout24 
  vm = mean(calx,'r');
  vst = stdev(calx,'r');
  for i=1:size(calx,2);          //gestion des variances nulles... 
      if vst(i)<10*%eps then vst(i)=0.0000000001;
      end
  end
  vpar=sqrt(vst);
 
  // scale
  if scale=='c' then
     calx =  calx  - ones(size(calx,1),1) * vm;
     testx = testx - ones(n,1) * vm;
 
  elseif scale=='cs' then
     calx =  ( calx - ones(size(calx,1),1)  * vm ) ./ ( ones(size(calx,1),1) * vst );
     testx = ( testx - ones(n,1) * vm ) ./ ( ones(n,1) * vst );
     
  elseif scale=='cp' then
     calx =  ( calx - ones(size(calx,1),1)  * vm ) ./ ( ones(size(calx,1),1) * vpar );
     testx = ( testx - ones(n,1) * vm ) ./ ( ones(n,1) * vpar );   
  end;



  // calcul du modèle sur l'ensemble d'étalonnage
  functionname='class'+func;
  execstr(msprintf('[resclass]= %s (calx,caly,lv);',functionname));    // 12juil18: lv_max remplacé par lv

 
  // xpr vérifie:  t = x * xpr pour MLR(xpr=I), PCR(xpr=P), SIMPLS (xpr=R) 
  if isfield(resclass,'loadings') then
    xpr=resclass.loadings; 
  elseif isfield(resclass,'rloadings') then
    xpr=resclass.rloadings;
  elseif ~isfield(resclass,'loadings') & ~isfield(resclass,'rloadings') then
    p_calx=size(calx,2);      // nbre de variables dans calx
    xpr=eye(p_calx,p_calx);
  end


  
  //définition des sorties
  if  func=='fda' | func=='forwda' | func=='plsda' | func=='copda' then   //21aout23
      lv1=1;
  elseif func=='plsfda' then        
      lv1=lv(1);
  end
  
  ep = zeros(lv_max,lv1); 
  cf = zeros(nbcl,nbcl,lv_max,lv1); // 14 mars 2017
  ep_class=zeros(nbcl,lv_max,lv1);

 
  // -----  ce code se retrouve dans daapply ----------------------------------
 
  // test, pour 1 a lv vecteurs discriminants 
  // ON CALCULE LES SCORES obtenus avec les méthodes fda, plsda, pls2da ou forwda
  cf=zeros(nbcl,nbcl,lv_max,lv1);
 
  if func=='fda' | func=='forwda' | func=='plsda' | func=='plsfda' | func=='copda' then 
      calx_scores=zeros(n_calx,lv_max,nbcl,lv1); 
      testx_scores=zeros(n,lv_max,nbcl,lv1);
  end
  
 
 for m=1:lv1;
  for k = 1:lv_max;  
   //   pause
    calx_scores=calx*xpr(:,1:k,m);
    testx_scores=testx*xpr(:,1:k,m);
                        
    [pr] = memberpred(calx_scores,caly, testx_scores,testy,metric,class);
    // somme de chaque ligne = 1
    

    // décision d'appartenance à une classe: PROBA. MAX avec la classe
    [prm,ind] = max(pr,'c');
    pred = conj2dis(ind,nbcl,'C');  //'C' force à imposer conjonctif
    pred(prm<seuil,:) = 0;     // possible que des obs. ne soient rattachées à aucune classe! 
    
    // calcul de la matrice de confusion
    c = pred'*testy;
    cf(:,:,k,m)=c;
    
  end;  // k
  
  end;  // m
 
  // ----------- fin du code commun avec daapply -------------------------------
  
  conf_matrix=list();
 
 
  if func=='plsfda' then                        //21aout23
        for i=1:lv_max;
            cf_i_temp=cf(1:nbcl,1:nbcl,i,1:lv1);
            cf_i.d=matrix(cf_i_temp,[nbcl,nbcl,lv1]);
            cf_i.i='pred_'+string([1:1:nbcl]');
            cf_i.v.v1='ref_'+string([1:1:nbcl]');
            cf_i.v.v2='LV_'+string([1:lv1]');
            conf_matrix(i)=[];
            conf_matrix(i)=div(cf_i);
        end
  else
        for i=1:lv;
            cf_i.d=cf(1:nbcl,1:nbcl,i);
            cf_i.i='pred_'+string([1:1:nbcl]');
            cf_i.v='ref_'+string([1:1:nbcl]');
            conf_matrix(i)=[];
            conf_matrix(i)=div(cf_i);
        end      
        
  end



endfunction











