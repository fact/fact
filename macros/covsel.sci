function [vs,cumvar,xs,vso] = covsel(x,y,lv);
  
  // [num,crbvar] = covsel(x,y,lv)
  // Selection of variable from covariance maximisation
  //
  // Input arguments
  // ===============
  // x:      matrix n x p
  // y:      matrix n x q
  // lv:     desired number of variables
  //
  // Output arguments
  // ================
  // num    : number (rank) of selected variables
  // crbvar : evolution of cumulated variance of X (first column) and Y (second one)
  //
  // This function is called by 'covariance_select' (SAISIR format)
  // Written by JM Roger, CEMAGREF, Montpellier
  //
  // see also: covariance_select, selectvar
  // last update: 2012, march, 30
  
  xs=div(x);
  ys=div(y);

  [nx,px] = size(xs.d);
  [ny,py] = size(ys.d);

  // default value for lv = all the variables of x
  if  argn(2)==2 then lv=px;
  end
  
  crbvar=zeros(lv,2);  
  
  if nx ~= ny
  error('Unconsistent number of rows')
  end

  lv = min( lv, rank(xs.d));

  // centrage de x , centrage+reduction de y (ainsi on supprime covsel_mncn et covsel_auto)    9jan19	supprime le 7mai20 
  //xs_centre=centering(xs);
  //z=xs_centre.d;
  //ys_centre=centering(ys);
  //ys_centre_reduit=standardize(ys_centre);
  //t=ys_centre.d;
  z=xs.d;
  t=ys.d;

  vartot=[trace(z'*z) trace(t'*t)];

  for i=1:lv
      //[cm,j] = max( diag( z'*t*t'*z ),'r');   // slow
      [cm,j] = max( sum( (z'*t).^2 ,2) );       // faster
      v = z(:,j);
      z = ( eye(nx,nx)-(v*v')/(v'*v) )*z;
      crbvar(i,1) = (vartot(1)-trace(z'*z))/vartot(1);
      t = ( eye(nx,nx)-(v*v')/(v'*v) )*t;
      crbvar(i,2) = (vartot(2)-trace(t'*t))/vartot(2);
      num(i)=j;
  end;

  // extraction of the selected variables from x
  num2=gsort(num,'r','i');
  xsel.d=xs.d(:,num2);
  xsel.i=xs.i;
  xsel.v=xs.v(num2);

  vs=num;
  cumvar=crbvar;
  xs=xsel;
  vso=num2;
  
  //Div
  xs=div(xs);
  

endfunction

