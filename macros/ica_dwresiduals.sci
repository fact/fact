function [res] =  ica_dwresiduals(X0,nF,Options);


  X=div(X0);

  // Do ICA on X

  Signal=list();
  Scores=list();

  for Fac=1:nF;
      if argn(2)==3 then 
        [Signal_nf,Scores_nf] = icascores(X,Fac, Options);
        [Signal_nf,Scores_nf] = icasigns(Signal_nf,Scores_nf,Options);
       elseif argn(2)==2 then 
        [Signal_nf,Scores_nf] = icascores(X,Fac);
        [Signal_nf,Scores_nf] = icasigns(Signal_nf,Scores_nf);          
       end     
    Signal(Fac)=Signal_nf;
    Scores(Fac)=Scores_nf; 
  end;
  

  // Deflate after ICAs
  Residus=list();

  Residus(1)=X;

  for Fac=2:nF
    X_temp=Scores(Fac-1)*Signal(Fac-1)';
    Residus(Fac)=Residus(1)-X_temp;
  end;
  
  // calcul de DW: sur les résidus
  DW=list();
  All_DW=[];

  for Fac=1:nF
    residus_fac=Residus(Fac);
    DW(Fac) = ica_durbwatmatrix_op(residus_fac.d,'r'); //vérifié: DW(Fac) est un vecteur

    DW_Fac=DW(Fac);  // original: All_DW=[All_DW;DW{1,Fac}(1,:)];
    All_DW=[All_DW;DW_Fac'];       // DW est en colonne et on compile les lignes
  end;

  mean_All_DW=mean(All_DW,'c');    // sortie en colonne pour un calcul par dimension


  // figure:
  
  if argn(2)==3 then 
    if isfield(Options,'dwplot') then
        if Options.dwplot==1  then
           grayplot(1:1:22,1:1:6,res.all_dw.d');
           h=gcf();
           h.color_map=jetcolormap(32)
           h.children.x_label.text='Sample index';
           h.children.y_label.text='Nb of ICs';
           h.children.title.text='Durbin-Watson criterion';
        end
    end
  end
  


  // sorties:
    
  res.all_dw.d=All_DW;
  res.all_dw.i=string([1:1:nF]')+'IC';
  res.all_dw.v=X.i;
  
  res.mean_all_dw.d=mean_All_DW;
  res.mean_all_dw.i=res.all_dw.i;
  res.mean_all_dw.v='mean_all_DW';
  
  res.residus=Residus;


endfunction

