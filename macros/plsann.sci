function [model,rmsec,rmsecv,b,ypredcv] = plsann(xi,yi,split,lv,cent,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
  
  // split = type de validation croisée 
  // lv = nbre de variables latentes pour la PLSR 
  // cent= centré (1) ou non centré (0) - défaut=1
  
  // varargin:
  // varargin(1)= nwh = nbre de neurones dans la couche cachée 
  // varargin(2)= opt = options de nns_buildbayes.sci 
  //              opt.RegClass = regularisation 
  //                            0 = aucune régularisation
  //                            1 = 1 classe pour tous les poids 
  //                            2 = 2 classes (inputs et outputs)
  //                            3 = une classe pour chaque input, une classe pour les biais de la couche cachée 
  //                                et une classe pour chaque output (défaut)
  //                            4 = une classe pour chaque poids               
  //              opt.PreProc = préprocessiong 
  //                            0 = aucun
  //                            1 = normalisation
  //                            2 = standardisation   
 
  x=div(xi);
  y=div(yi);

  if x.i~= y.i then 
  	warning('labels in x and y do not match')
  end
  
  [model] = cvreg(x,y,split,'nnpls',cent,lv,varargin(:));
    
   model.weight_regul=varargin(2);

endfunction


