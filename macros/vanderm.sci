function [ Lambda ] = vanderm(q,po)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  Lambda=ones(q,po + 1);
  l1=[1:1:q]';
  for indice=1:po+1;
    Lambda(:,indice)=l1.^(indice-1);
    Lambda(:,indice)= Lambda(:,indice)/sqrt(Lambda(:,indice)'* Lambda(:,indice));
  end;

endfunction

