function [secv,predcv,dof,lot0,secv_stats] = cvxx(x,y,split,func,cent,varargin)

    // maj 17 aout 23

    // =================================================================
    //
    // cvxx:                 validation croisee des fonctions linéaires 
    //                               et non lineaires
    //
    // =================================================================
    // 
    // Input:
    // =====
    // x:           matrix, n X p
    // y:           vector of reference values, n X 1
    // nf:          number of factors (latent variables for PLSR); usually an integer but it
    //              can be a vector for the 'ridge' regression
    // split:       how x is splitted during cross-validation
    //              if split is not a structure:
    //                  a scalar ns provokes a random ns subset CV
    //                  two scalars [ns,ni] provoke a double CV (ns random subsets) x ni iterations
    //                  a vector of n block numbers provokes a guided subset CV
    //                  a matrix of n x k block numbers provokes k guided subset CV
    //                  "vnbN" provokes a N blocks venitian blind CV (e.g. "vnb15")
    //                  "jckN" provokes a N blocks jackkniffe CV (e.g. "jck15")
    //              if split is a structure:
    //                  .method:        a string        the CV method: 'random','jack','venitian' or 'prefixed'
    //                  .nblocks:       the parameter attached to the method
    //                                  'random', 'jack' or 'venitian':'    -> the number of blocks
    //                                  'prefixed -> a conjunctive matrix (n*r) with the numbers of the k blocks repeated r times
    //                                               or a disjunctive matrix (n * k * r) for k blocks repeated r times
    //                  .nbrepeat:      the number of repetitions of the CV
    //                  .sameobs:       the identification of the observations that cannot be split in cal and val datasets
    // func:        function called for each step of the cross validation
    //              possible choices: 'pls' , 'pcr', 'vodka' ,'ridge'
    // cent :       if 1, data are centered
    //              if 0, data are not centered
    //
    //
    //
    // Output:
    // =======
    // secv:    cross validation errors. The size depends on the parameters of the function used (ex: nbr of LV)
    // predcv:  predictions of the cross validation
    // 
    // Example:
    // ========
    // [secv,pred] = xxcvn(x,y,[10 30],'pls',1 ,20); 
    //          cross validation applied to 10 blocks 30 times for a PLSR on x,y with 20 
    //          latent variables and with centering 
    // [secv,pred] = xxcvn(x,y,10,'vodka',20, 2,1); 
    //          cross validation applied to 10 blocks for a VODKA-PLSR on x,y with 20 
    //          latent variables, r option=2 and after centering 
    // 
    // Miscellaneous:
    // ==============
    // JM Roger (IRSTEA) and JC Boulet (INRA), France
    // Last update: 2018, Novembre, 5
    // -----------------------------------------------  

    
    [mx,nx] = size(x);
    [my,ny] = size(y);
    if mx ~= my
        error('The number of samples of x and y does not match')
    end

    if func ~= 'ridge'  &  func ~= 'nnpls'  &  func ~= 'nnpca' & argn(2)>5 then 
        nvl=varargin(1);                  // nbre de variables latentes 24nov22
    elseif func=='ridge' | func == 'nnpls' | func == 'nnpca' then 
        nvl=max(size(varargin(1)));       // 17aout23  lv est un vecteur de valeurs lambda 
    else
        nvl=1;                            // cas MLR p.ex.   17aout23
    end
   
  
    // ----------------------------------------------------------------------
    // lot0 = blocs en disjonctif
    lot0=cvgroups(mx,split);

    //----------------------------------------------------------------------
    // application de la CV avec les modèles
    [n,ni,nj] = size(lot0);                                  // n=mx...

    // 23nov22: disjonctif -> conjonctif 
    if  nj==1 then            // disjonctif sans répétitions 
        lot=dis2conj(lot0);
    elseif nj >1 then            // disjonctif avec répétitions 
        lot=zeros(n,nj);
        for i=1:nj;
            lot(:,i)=dis2conj(lot0(:,:,i));
        end;    
    end
    [n,ni] = size(lot);

    press_all=zeros(nj,nvl);               //    nj =nbr repet  size(s,1)=nbr var. latentes 

    // 16dec22 
    prcv=zeros(n,nvl,nj);     
    n_prcv=zeros(n,nvl,nj);

    for j=1:nj;                                             // pour chaque répétition
        for k=1:ni                                          // pour chaque lot
            ns = max(lot(:,k));                             // nombre de lots
            for i=1:ns
                // splitting cal and test sets
                indval =  find(lot(:,j)==i);      
                indcal =  find(lot(:,j)~=i);
                // Calling of the calibration+test function:
                if ~isempty(indval) &  ~isempty(indcal) then
                    [s,pr,biaisjc,last_argout]=caltestxx(x(indcal,:),y(indcal,:),x(indval,:),y(indval,:),func,cent,varargin(:));
                    dof=last_argout.dof;
                end
                if (i==1 & k==1 & j==1)  then                      // 1° appel -> initialisation  CONTINUER
                    press = 0*s;
                    sz = size(pr); 
                    sz=sz(2:$); 
                end;
                press = press + (s.^2) * length(indval);
                press_all(j,:)=press_all(j,:) + (s.^2)' * length(indval);              
                //16dec22
   //pause
                prcv(indval,:,j) =  prcv(indval,:,j) + pr;   
                n_prcv(indval,:,j)=n_prcv(indval,:,j)+1;	// on compte le nombre d'ajouts dans chaque case 		 
                                                     
            end
        end;
    end;

    // reshape of the output arguments
    secv = sqrt(press/(mx*ni*nj));
    
    //16dec22 -----------
    //calcul de predcv = moyenne de predcv sur les différentes répétitions    // 16dec22
    prcv2=zeros(n,nvl);
    n_prcv2=zeros(n,nvl);

    for i=1:nj;
        prcv2=prcv2+prcv(:,:,i);
        n_prcv2=n_prcv2+n_prcv(:,:,i);
    end;
    
    flag=find(n_prcv2==0);   // avec des % faibles on pourrait ne pas sélectionner toutes les observations 
    if flag==[] then
        predcv=prcv2./n_prcv2;
    else
        predcv=[];
    end;
    // ------------------
 
    secv_all=sqrt(press_all/(mx*ni));                           
    secv_stats.mean=mean(secv_all,'r')';
    secv_stats.stdev=stdev(secv_all,'r')';
 

endfunction



