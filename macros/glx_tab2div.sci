function [data] = glx_tab2div(filename)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  // reads a .csv file and converts it to .div
  // the first line and the first column of the .csv file must be labels
  // the cell in position (1,1) is ommited

    m=csvRead(string(filename),'\t','.','string');                           //  JCB 15oct20 + 3mars21 à la place de: read_csv
    data.d=strtod(strsubst(string(m(2:$,2:$)),'D','E'));
    data.i=stripblanks(string(m(2:$,1)));
    data.v=m(1,2:$)';
    data=div(data);
    

endfunction
