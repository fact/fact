function [saisir] = covmat(x1,x2)
    
  //covmap 			- assesses the covariances between two tables
  //function [cov] = covmat(X1,X2)
  //
  //Input arguments
  //--------------
  //X1 and X2: SAISIR matrix dimensioned n x p1 and n x p2 respectively
  //
  //Output argument:
  //---------------
  //cov: matrix of covariance dimensioned p1 x p2)
  //the tables must have the same number of rows
  //An element cov.d(i,j) is the covariance between the column i of X1 and j of X2

  saisir1=div(x1);
  saisir2=div(x2);

  [nrow1 ncol1]=size(saisir1.d);
  [nrow2 ncol2]=size(saisir2.d);
  if(nrow1~=nrow2); error('The number of rows must be equal');end
  a=saisir1.d -ones(nrow1,1)*mean(saisir1.d,1);
  b=saisir2.d -ones(nrow1,1)*mean(saisir2.d,1);
  saisir.d=(1/nrow1)*(a'*b);
  saisir.i=saisir1.v;  
  saisir.v=saisir2.v;  
   
  //Div
  saisir=div(saisir);
 
 

endfunction

