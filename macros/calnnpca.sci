function [rloads,wh1,wo1]=calnnpca (calx,caly,lv,varargin)

    //function ypred=calnnpca (calx,caly,testx,lv,nh,no)
    // nh = nbre de neurones cachés 
    // no = nbre de neurons en sortie = nbre de colonnes de caly => calcule automatiquement 
   
    // variables latentes: 
    // - soit une seule valeur
    // - soit un intervalle, ex. [1:5:21] 
    // voir list_lv ligne 26
  
    
    nh=varargin(1);
    opt=varargin(2);
 
 
    // neural network: 
    wh1=list();
    wo1=list();
        
    // PCA
    lv_max=max(lv);
    res_pcana=pcana(calx); 
    rloads=res_pcana.eigenvec.d(:,1:lv_max); 
    xscores=res_pcana.scores.d(:,1:lv_max);
    // [b0,xscores,ploads,dof,rloads]=calikpls(calx,caly,lv_max); // ligne de PLS
             
        
    n_lv=max(size(lv));
 
    
    for i=1:n_lv;
        [wh,wo]=nns_init(xscores(:,1:lv(i)),caly,nh,'n');
        [res_nns]=nns_buildbayes(wh,wo,xscores(:,1:lv(i)),caly,opt);
        wh1(i)=res_nns.wh.d;
        wo1(i)=res_nns.wo.d;
    end
    
endfunction
