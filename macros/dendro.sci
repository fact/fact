function [xdist2,xlink]=dendro(x,dtype,nbr_branches)
    
    
    x1=div(x);
    x2=x1.d;
    [n,q]=size(x2);
    
    if argn(2)<2 then
        dtype='euc';   // distance euclidienne par défaut 
    end
    
    if dtype=='' then
        dtype='euc';
    end

    // calcul de la distance
    xdist.d=dendro_pdist(x2,dtype)';       // sortie=vecteur vertical
    
    // calcul de x_dist.i
    x_d1=repmat(string(1),n-1,1);
    x_d2=string([2:n]');
    for i=2:n-1;
        xtemp=repmat(string(i),n-i,1);
        x_d1=[x_d1;xtemp];
        xtemp2=string([i+1:n]');
        x_d2=[x_d2;xtemp2];
    end
    xdist.i=x_d1+'-'+x_d2;
    
    xdist.v='dist:' + dtype;
   //pause 
    xdist=div(xdist);
 
    
    // calcul de xdist2 pour la sortie 
    xdist2.d=[strtod(x_d1) strtod(x_d2) xdist.d];
    xdist2.v=['no_obs';'no_obs';xdist.v];
    xdist2=div(xdist2);   
  
    
    // calcul des liens 
    xlink.d=dendro_linkage(xdist.d);

    xlink.v=['no_group'; 'no_group';xdist.v];
    
    xlink=div(xlink);
    
    //visualisation du dendrogramme: 
    if argn(2)==3 then
        dendro_dendrogram(xlink.d,nbr_branches);
    else
        dendro_dendrogram(xlink.d);
    end
    

    
endfunction
