function lot_out = cvgroups(mx,split_in)
 
  // maj: 29 aout 23
  
  // mx:                nbre total d'observations 
  // split (ancien):    la manière dont le regroupement est obtenu
  //                    ex: 'jck5'  5 groupes par Jack Knife (= 5 groupes contigus)
  //                        'vnb5'  5 groupes par Stores Vénitiens
  //                            5   5 groupes tirés au hasard
  //                         [2,50] 2 groupes tirés au hasard, répété 50 fois
  //                    ou bien un vecteur/ ou une matrice en codage conjonctif avec chaines de caractères (rajouté le 25nov22)
  // split (nouveau):   split.method           "jack", "random", "venitian" ou "prefixed"
  //                    split.nblocks           nbre de classes à construire 
  //                    split.nbrepeat          uniquement pour random
  //                    split.sameobs			longueur mx   les observations ne devant pas être séparées en CV (pas valable pour method="prefixed")
  // lot_out:       matrice 2D/3Dcodant les groupes de CV en disjonctif uniquement; 2D=1 répétition, 3D=plusieurs répétitions


  // ex: 
  //        split.method="jack";
  //        split.nblocks=3
  //        split.sameobs=[1;1;1;1;2;2;2;3;3;4;4;4;4;4;5;5;6;6;6;7;7;8;8;8;9;9;9;10;10;10;11;11;11;12;12;12]
   
  //        split.sameobs=[1;1;1;1;2;2;2;3;3;4;4;4;5;5;5;6;6;6;7;7;7;8;8;8;9;9;9;10;10;10;11;11;11;12;12;12]
  //        split.method="random";
  //        split.nblocks=3
  //        split.nbrepeat=2
   
  //        split.sameobs=[1;1;1;1;2;2;2;3;3;4;4;4;4;4;5;5;6;6;6;7;7;8;8;8;9;9;9;10;10;10;11;11;11;12;12;12]
  //        split.method="venitian";
  //        split.nblocks=3

  //        split.method="prefixed";
  //        split.classes=[1;1;1;1;2;2;2;3;3;4;4;4;4;4;5;5;6;6;6;1;1;1;2;2;2;3;3;3;4;4;4;5;5;5;6;6]



    // ------------------------------
    // étape 1: créer split=structure
    // ------------------------------


    split=split_in;

    if typeof(split) ~='st' then              // st = structure    ici split n'est PAS une structure

        // ---------- ancienne version des paramètres de CV + tableau disjonctif en chaines de caractères  
        if typeof(split)=="string" then             
            if max(size(split))==1 then             // ex: jack4, ...
                mth=part(split,1:3);
                if mth=='jck' | mth=='blk' ; then
                    split0.method="jack" 
                elseif mth=='vnb' then 
                    split0.method="venitian";
                end
                split0.nblocks=strtod(part(split,4:$));
                split0.nbrepeat=1;
                split0.sameobs=string([1:mx]');
                split=split0;
            elseif max(size(split))>1               // un tableau conjonctif contenant des chaines de caractères                   //25nov22
                [ntemp,ptemp]=size(split);          // ntemp = nbre d'observations  ptemp = nbre de répétitions de la CV 
                groups_temp=unique(split(:,1));
                ngroups_temp=max(size(groups_temp));    //ngroups_temp= nbre de groupes différents 
                lot_temp=zeros(ntemp,ngroups_temp,ptemp); 
                for i=1:ptemp;
                    xtemp=conj2dis(str2conj(split(:,i)));
                    lot_temp(:,:,i)=xtemp;
                end;
                split.method="prefixed";
                split.nblocks=lot_temp;
            end    
                
        elseif typeof(split)=="constant"    // une valeur 
            
            [nt,pt,qt]=size(split);
            
            if qt==1 then                   // ex. 4 ou [2,50] ou matrice 2D
               
                split0.sameobs=string([1:mx]');
                if max(nt,pt)==1 then       // une seule valeur 
                    split0.method='random';
                    split0.nblocks=split;
                    split0.nbrepeat=1;
                elseif max(nt,pt)==2 then   // 2 valeurs
                    split0.method='random';
                    split0.nblocks=split(1);
                    split0.nbrepeat=split(2);
                elseif max(nt,pt)>2 then    // codage disjonctif avec des entiers 
                    split0.method="prefixed"
                    split0.nblocks=split;
                end
                split=split0;
        
            elseif qt>1                     // 3D
            
                if isdisj(split)=="T" then 
                    split0.method="prefixed";
                    split0.nblocks=split;
                else 
                    error('cvgroups: split is not a disjunctive 3D matrix')
                end
                split=split0;
            end
        end
    end
        


    // ------------------------------------
    // fin de l'étape 1 :
    // split est une structure
    // ------------------------------------

     
    // --------------------------------------------------------
    // étape 2: extraire lot_out = matrice 2D/3D disjonctive
    // -------------------------------------------------------- 
     
     
    // observations à ne pas séparer en CV                                   
    if isfield(split,"sameobs") then    // 23nov22
        sameobs=split.sameobs;
        sameobs=string(sameobs);
        split.sameobs=str2conj(sameobs);
    else
        split.sameobs=string([1:mx]');
    end;

 
    // -----------------------------
    // method = 'jack' ou 'venitian'
    // -----------------------------
    if split.method=="jack"| split.method=="venitian" then                                           
            nsplit = split.nblocks;                     // nbre de classes
            bysplit = round(mx/nsplit);                 // taille d'une classe
                
            mx_unique=unique(split.sameobs);
            n_mx_unique=max(size(mx_unique));
            size_classe=floor(n_mx_unique/nsplit);
            
            // identifications des classes par échantillon (1 ech= plusieurs répétitions )
            if split.method=="jack" then
                no_classe=[];
                for i=1:nsplit;
                    no_classe=[no_classe;ones(size_classe,1)*i];   
                end
                no_classe=[no_classe;ones(size_classe,1)*nsplit]; // pour gerer les derniers echantillons 
                no_classe=no_classe(1:n_mx_unique);
            elseif split.method=="venitian" then 
                no_classe=[];
                venitian=[1:nsplit]';
                for i=1:bysplit;
                    no_classe=[no_classe;venitian];
                end;
                no_classe=[no_classe;venitian];
                no_classe=no_classe(1:n_mx_unique);
            end;
     
            // attribution des classes            
            lot0=zeros(mx,1);     
            for i=1:n_mx_unique;
                tri_temp=find(split.sameobs==mx_unique(i));
                lot0(tri_temp,:)=no_classe(i);
            end
      
            // lot0 -> mise en disjonctif
            lot_out=conj2dis(lot0);     
        
        
          
    // -----------------
    // method = random'
    // -----------------
    
    elseif split.method=='random' then          // tirage aléatoire  ----------------------
                                                                                                // maj 22aout23

            if split.nblocks >= mx  then            // pas plus de blocs que d'observations
                split.nblocks=mx;                   // cas du loo
            end;

            x_date=getdate("s");                    // pour avoir chaque fois un tirage totalement aléatoire
            rand("seed",x_date);

            lot0=zeros(mx,split.nbrepeat);           
            
            mx_unique=unique(split.sameobs);        // les repetitions analytiques ont le meme numero dans split.sameobs
            n_mx_unique=max(size(mx_unique));       // nbre d'observations uniques
            tri=[1:1:n_mx_unique]';
            
            for i=1:split.nbrepeat;        // ----------boucle split.nbrepet ---------------------         
                                           //nbrepeat = uniquement avec "random" ; ex: [2,50]= 2 blocs, 50 repet=50colonnes
      
                 [nul,tri_temp]=gsort(rand(n_mx_unique,1),'g','i');   // tri_temp=1:n_mx_unique non ordonne
                                                 // tri_temp: ordre aleatoire de 1 à n_mx_unique
                 tri2(:,1)=tri(tri_temp,:);              // nouvel ordre 
                 
                 size_classes=floor(n_mx_unique/split.nblocks);    // valeur par défaut (pour avoir une classe en + à la fin)
                                                                   // correction bug 29aout23 
                  
                 tri_obs=0*split.sameobs;
                  
                 for j=1:split.nblocks-1;
                     tri_bloc_unique= tri2(1+(j-1)*size_classes:j*size_classes);  // selection d'un bloc
                     for k=1:size_classes;
                         tri_temp=find(split.sameobs==tri_bloc_unique(k));        
                         tri_obs(tri_temp,1)=j;                                   // j=no de bloc
                     end
                 end
                 
                 tri_temp=find(tri_obs==0);
                 tri_obs(tri_temp)=split.nblocks;          // le dernier bloc souvent plus petit 
                         
                 lot0(:,i)=tri_obs;

                     
            end   // ----------------fin de la boucle split.nbrepet---------------------------------
            
            // lot0 -> mise en disjonctif
            rtemp=size(lot0,2);                     // rtemp=nbre de répétitions
            disj_temp=conj2dis(lot0(:,1));       
            [ntemp,qtemp]=size(disj_temp);          //ntemp=nbre d'observations  qtemp=nbre de groupes
            lot00=zeros(ntemp,qtemp,rtemp);
            for i=1:rtemp;
                lot00(:,:,i)=conj2dis(lot0(:,i));
            end;
            lot_out=lot00;
 
 
 
    // ----------------------
    // method = random_pcent'
    // ----------------------
    elseif split.method=='random_pcent' then
            mx_unique=unique(split.sameobs);
            n_mx_unique=max(size(mx_unique));
           
            // nombre d'observations à retenir pour le test
            nobs_test=round(n_mx_unique*split.nblocks/100);  // une valeur en %, ex: 30
            
            for i=1:split.nbrepeat;                 //nbrepeat = uniquement avec "random" ; ex: [2,50]= 2 blocs, 50 repet=50colonnes
 
                // pour avoir chaque fois un tirage totalement aléatoire
                x_date=getdate();
                x_date2=x_date(10);  // temps en secondes; problème: le calcul dure des millisecondes   
                x_date3=x_date2*round(100*abs(rand(1,1)));     // niveau aléatoire supplémentaire             
                rand("seed",x_date3);
                   
                // codage conjonctif pour les observations uniques
                [nul,tri_temp]=gsort(rand(n_mx_unique,1),'g','i');
                selected_obs=tri_temp(1:nobs_test);
                no_classe_unique= zeros(n_mx_unique,1);
                no_classe_unique(selected_obs)=1;          
              
                //observations uniques => répétitions 
                no_classe_all=zeros(mx,1);
                
                for j=1:n_mx_unique;
                    tri_temp2=find(split.sameobs==mx_unique(j));
                    no_classe_all(tri_temp2)=no_classe_unique(j);
                end

                lot0(:,i)=no_classe_all;       
            end
            
            // lot0 -> lot00 = lot_out = mise en disjonctif
            rtemp=size(lot0,2);                     // rtemp=nbre de répétitions
            lot00=zeros(mx,2,rtemp);
            for i=1:rtemp;
                lot00(:,:,i)=[lot0(:,i) 1-lot0(:,i)];
            end;
            
            lot_out=lot00;
  
 
 
 
    // -------------------
    // method = 'prefixed'
    // -------------------
                
    elseif split.method=='prefixed' then                        // les groupes de CV sont prédéfinis 
            lot0_temp=split.nblocks;                                
            if max(size(size(lot0_temp)))==2 then                   // matrice 2D
                if typeof(lot0_temp)=="constant" then      
                    if isdisj(lot0_temp)=="T" then                  // disjonctif
                        lot0=lot0_temp;
                    elseif isconj(lot0_temp(:,1))=="T" then         // conjonctif avec des nombres 
                        xtemp=conj2dis(lot0_temp(:,1));
                        [n,q]=size(lot0_temp);
                        [n,r]=size(xtemp);
                        lot0=zeros(n,r,q); 
                        for j=1:q; 
                            lot0(:,:,j)=conj2dis(lot0_temp(:,j));
                        end;
                    end;       
                elseif typeof(lot0_temp)=="string" then             // conjonctif avec des chaines de caractères 
                    xtemp=conj2dis(str2conj(lot0_temp(:,1)));
                    [n,q]=size(lot0_temp);
                    [n,r]=size(xtemp);
                    lot0=zeros(n,r,q); 
                    for j=1:q; 
                        lot0(:,:,j)=conj2dis(str2conj(lot0_temp(:,j)));
                    end;
                end          
            elseif max(size(size(lot0_temp)))==3 then
                [n,p,q]=size(lot0_temp);
                for i=1:q;
                    xtemp=lot0_temp(n,p,i);
                    if isdisj(xtemp)=="F" then 
                        error('cvgroups: nblocs should be a 3D matrix of disjunctive 2D matrices') 
                    end;
                end;
                lot0=lot0_temp;
            end;
            lot_out=lot0;
        
        
       
    end         // fin de la boucle 'random' 'jack' 'venitian' 'prefixed' etc
  
  
    // ------------------------
    // etape 3: verification                    // 29aout23 
    // ------------------------
    
    // chaque ligne doit contenir une fois 1 + que des 0
    // aucune colonne ne doit être vide (pas de 1) 
    // le total des 1 doit donner mx = nombre d'observations
    sum_line=sum(lot_out,'c');
    sum_col=sum(lot_out,'r');
    sum_all=sum(lot_out);
    
    if max(sum_line)~=1 | min(sum_line)~=1 then
        error('cvgroups: observations do not have any group' )
    end

    if min(sum_line)==0 then 
        error('cvgroups: some groups are empty' )
    end
        
    if  sum_all ~= mx then
        error('cvgroups: the numbers of observations does not fit')
    end
    
 

endfunction
