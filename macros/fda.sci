function [model] = fda(xi,yi,split,lv,varargin)

  //function [conf_matrix,err_cv,err_cal] = fda(x,y,split,lv,metric,scale,seuil)
  
  // cette fonction appelle cvxxcl avec la fonction 'fda'
  // et met au format Saisir
 

  [model]= cvclass(xi,yi,split,lv,'fda',varargin(:))

  
endfunction
