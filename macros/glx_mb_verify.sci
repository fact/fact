function col2=glx_mb_verify(col)
    
      // vérifie que col est bien au format MB et ajuste si besoin 
    
      col2=col;
      // verification des colnames et rownames
      ncol=max(size(col2));                          // nbre de tableaux
      ndim=size(col2(1).data);                       // ndim est de dimensions (1 x 2)
      n_cwn=col2(1).colwaynames; 
      n_rwn=col2(1).rowwaynames; 
      n_bn=col2(1).blocknames; 
      for i=2:ncol; 
        ndim=[ndim;size(col2(i).data)];              // (ncol x 2)
        n_cwn=[n_cwn;col2(i).colwaynames];           // (ncol x 1)
        n_rwn=[n_rwn;col2(i).rowwaynames];           // (ncol x 1)
        n_bn=[n_bn;col2(i).blocknames];              // (ncol x 1)
      end; 
      
       // labels des lignes ----------------
      if size(unique(ndim(:,1)),1)==1 then ;             // même nbre de lignes pour tous les tableaux
      if size(unique(n_cwn),1)~=size(n_cwn,1);      // au moins 2 labels de ligne égaux => tous mis égaux
            for i=2:ncol; 
                col2(i).rowwaynames=col2(1).rowwaynames; 
            end; 
      end; 
      elseif size(unique(ndim(:,1)),1)~=1;         // nbre de lignes different
        if size(unique(n_cwn),1)~=1;                 // au moins 2 labels de ligne identiques => tous mis differents
            for i=1:ncol; 
                col2(i).rowwaynames=col2(i).rowwaynames+'_mb' +string(i); 
            end; 
        end; 
      end; 
      
       // labels des colonnes  ----------------
      if size(unique(ndim(:,2)),1)==1;                 // même nbre de colonnes pour tous les tableaux
        if size(unique(n_rwn),1)~=ncol;                // au moins 2 labels de colonne égaux => tous mis égaux 
            for i=2:ncol; 
                col2(i).colwaynames=col2(1).colwaynames; 
            end; 
        end; 
        elseif size(unique(ndim(:,2)),1)~=1;            // nbre de colonnes different
        if size(unique(n_cwn),1)~=ncol;                  // au moins 2 labels de colonne identiques => tous mis différents
            for i=1:ncol; 
                col2(i).colwaynames=col2(i).colwaynames+'_mb' +string(i); 
            end; 
        end; 
      end; 
      
       // noms des tableaux: -----------------
      if size(unique(n_bn),1)~=ncol;          // au moins 2 noms de tableaux egaux => tous mis differents
        for i=1:ncol; 
            col2(i).blocknames=col2(i).blocknames +'_mb' +string(i); 
        end; 
      end; 
    
      // justification = même longueur donnée à chaque élement 
      all_colwaynames=col2(1).colwaynames;
      all_rowwaynames=col2(1).rowwaynames;
      all_blocknames=col2(1).blocknames;
      for i=2:ncol;
          all_colwaynames=[all_colwaynames;col2(i).colwaynames];
          all_rowwaynames=[all_rowwaynames;col2(i).rowwaynames];
          all_blocknames=[all_blocknames;col2(i).blocknames];
      end
      all_colwaynames=justify(all_colwaynames,'l'); 
      all_rowwaynames=justify(all_rowwaynames,'l');
      all_blocknames=justify(all_blocknames,'l');
      for i=1:ncol;     
          col2(i).colwaynames=all_colwaynames(i);      
          col2(i).rowwaynames=all_rowwaynames(i);
          col2(i).blocknames=all_blocknames(i);
      end    
      
    
    
endfunction
