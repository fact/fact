function [xs2]=div(x,xi,xv,infos);


  //function [xs2]=div(x,xi,xv,infos);

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
  
  
  // déjà un Div
  if typeof(x)=='div' & argn(2)==1 then

    xs2=x;  


  // une structure correspondant à un div classique (vecteur ou matrice)
  elseif typeof(x)=='st' & isfield(x,'d') & length(size(x.d))<=2 then        

            if type(x.d)~= 1 then 
                  error('Input data is not a matrix'); 
            else  xs.d=x.d;  
                  [n1,p1]=size(xs.d);                      
                    
                  if (~isfield(x,'v'))|x.v==[] then 
                        xs.v='v'+ string([1:1:p1]'); 
                                            
                  else [n2,p2]=size(x.v)
                        if p2>n2 then x.v=x.v';     
                        end
                        [n2,p2]=size(x.v)
                        if (n2~=p1| p2~=1) then 
                            error('error in the number of variable labels');
                            else xs.v=string(x.v);
                        end
                        
                  end;
                      
                  if (~isfield(x,'i'))| x.i==[]  then 
                        xs.i='i' + string([1:1:n1]'); 
                  else [n2,p2]=size(x.i);
                        if p2>n2 then x.i=x.i'; 
                           [n2,p2]=size(x.i)
                        end
                        if (n2~=n1| p2~=1) then 
                            error('error in the number of samples labels');
                            else xs.i=string(x.i);
                        end
                        
                  end;
            end;


     // gestion du champ infos 
     if argn(2)<4 then  
       infos=[]; 
     end
     
     if size(xs.d,1)==size(xs.i,1) & size(xs.d,2)==size(xs.v,1) then
       xs2=tlist(['div','d','i','v','infos'],xs.d,string(xs.i),string(xs.v),infos);
     else
       error ('div dimensions do not agree') 
     end
 



   // une structure correspondant à une hypermatrice 3D
   elseif typeof(x)=='st' & isfield(x,'d') & length(size(x.d))>=3 then        
 
            // ci-dessous: hypermat 5.5 -> constant 6.0 

            if typeof(x.d)~='constant' & typeof(x.d) ~='hypermat' then 
                  error('Input data is not an hypermatrix'); 
            else  xs.d=x.d;  
                  [n1,p1,p2,p3]=size(xs.d);                      
                    
                  if (~isfield(x,'v'))| x.v==[] then 
                        xs.v.v1='v1_'+ string([1:1:p1]');
                        xs.v.v2='v2_'+ string([1:1:p2]');
                        if p3>1 then
                            xs.v.v3='v3_'+ string([1:1:p3]'); 
                        end                                 
                  else 
                      if isfield(x.v,'v1') then 
                         [n2,q]=size(x.v.v1)
                         if q>n2 then 
                             xs.v.v1=x.v.v1'; 
                         else
                             xs.v.v1=x.v.v1;    
                         end
                         [n2,q]=size(xs.v.v1);
                         if (n2~=p1 | q~=1) then 
                            error('error in the number of variable labels');
                         end
                      else
                         xs.v.v1='v1_' + string([1:1:p1]');
                      end
                                          
                      if isfield(x.v,'v2') then 
                         [n2,q]=size(x.v.v2)
                         if q>n2 then 
                             xs.v.v2=x.v.v2'; 
                         else
                             xs.v.v2=x.v.v2;    
                         end
                         [n2,q]=size(xs.v.v2);
                         if (n2~=p2| q~=1) then 
                            error('error in the number of variable labels');
                         end
                      else
                         xs.v.v2='v2_' + string([1:1:p2]');
                      end
                                        
                      if p3>1  then                                         // 21aout23 ajout de la 3° dimension
                            if isfield(x.v,'v3') then    
                                [n2,q]=size(x.v.v3)
                                if q>n2 then 
                                    xs.v.v3=x.v.v3'; 
                                else
                                    xs.v.v3=x.v.v3;    
                                end
                                [n2,q]=size(xs.v.v3);
                                if (n2~=p3| q~=1) then 
                                    error('error in the number of variable labels');
                                end
                            else
                                xs.v.v3='v3_' + string([1:1:p3]');
                            end
                       end 
                        
                   
                        
                        
                  end;
                  
                      
                  if (~isfield(x,'i'))| x.i==[]  then 
                        xs.i='i' + string([1:1:n1]'); 
                  else [n2,q]=size(x.i);
                        if q>n2 then x.i=x.i'; 
                           [n2,q]=size(x.i)
                        end
                        if (n2~=n1| q~=1) then 
                            error('error in the number of samples labels');
                            else xs.i=string(x.i);
                        end
                        
                  end;
            end;


     // gestion du champ infos 
     if argn(2)<4 then  
       infos=[]; 
     end
   
     if size(xs.d,1)==size(xs.i,1) & size(xs.d,2)==size(xs.v.v1,1) & size(xs.d,3)==size(xs.v.v2,1) then
       xs2=tlist(['div','d','i','v','infos'],xs.d,string(xs.i),xs.v,infos);
     else
       error ('div dimensions do not agree') 
     end
 
 
   
   // une matrice de données   
   elseif typeof(x)=='constant' & length(size(x))<=2 then       // 25juil16
       [n1,p1]=size(x);
       if argn(2)==1 then 
         xs.d=x;
         xs.i='i'+string([1:1:n1]');
         xs.v='v'+string([1:1:p1]');   
       elseif argn(2)>=3 then 
           if size(xi)==[1 n1] then xi=xi'; end
           if size(xv)==[1 p1] then xv=xv'; end
           if size(xi)~=[n1 1] | size(xv)~=[p1 1] then error ('no proper dimensiosn for i or v');
           else  xs.d=x;
                 xs.i=string(xi);
                 xs.v=string(xv);
           end
       end
   
       if argn(2)<4 then 
           infos=[];
       end
   
       
       if size(xs.d,1)==size(xs.i,1) & size(xs.d,2)==size(xs.v,1) then
         xs2=tlist(['div','d','i','v','infos'],xs.d,string(xs.i),string(xs.v),infos);
       else
         error ('div dimensions do not agree') 
       end
   

   // une hypermatrice à 3 dimensions                                  
   elseif typeof(x)=='constant' & length(size(x))==3    then     

       [n1,p1,p2]=size(x);
       if argn(2)==1 then 
         xs.d=x;
         xs.i='i'+string([1:1:n1]');
         xs.v.v1='v1_'+string([1:1:p1]');
         xs.v.v2='v2_'+string([1:1:p2]');   
       elseif argn(2)>=4 then 
           if size(xi)==[1 n1] then 
               xi=xi'; 
           end

           if isfield(xv,'v1') & size(xv.v1,1)==1 then 
               xv.v1=xv.v1'; 
           elseif isfield(xv,'v1') & size(xv.v1,2)==1 then
               xv.v1=xv.v1;
           elseif ~isfield(xv,'v1') then
               xv.v1='v1_'+string([1:1:p1]');
           else 
               error('not proper field v1')
           end
           
           if isfield(xv,'v2') & size(xv.v2,1)==1 then 
               xv.v2=xv.v2';
           elseif isfield(xv,'v2') & size(xv.v2,2)==1 then 
               xv.v2=xv.v2;    
           elseif ~isfield(xv,'v2') then
               xv.v2='v2_'+string([1:1:p2]');
           else 
               error('not proper field v2')
           end
           
           
           if size(xi)~=[n1 1] | size(xv.v1)~=[p1 1] | size(xv.v2)~=[p2 1] then error ('no proper dimensiosn for i or v');
           else  xs.d=x;
                 xs.i=string(xi);
                 xv.v1=string(xv.v1);
                 xv.v2=string(xv.v2);
                 xs.v=xv;
           end
       end
   
       if argn(2)<4 then 
           infos=[];
       end
    
       if size(xs.d,1)==size(xs.i,1) & size(xs.d,2)==size(xs.v.v1,1) & size(xs.d,3)==size(xs.v.v2,1)   then
         xs2=tlist(['div','d','i','v','infos'],xs.d,string(xs.i),xs.v,infos);
       else
         error ('div dimensions do not agree') 
       end
  

  
    // une hypermatrice à 4 dimensions           // 21aout23
   elseif typeof(x)=='constant' & length(size(x))==4    then     

       [n1,p1,p2,p3]=size(x);
       if argn(2)==1 then 
         xs.d=x;
         xs.i='i'+string([1:1:n1]');
         xs.v.v1='v1_'+string([1:1:p1]');
         xs.v.v2='v2_'+string([1:1:p2]');   
         xs.v.v3='v3_'+string([1:1:p3]'); 
       elseif argn(2)>=4 then 
           if size(xi)==[1 n1] then 
               xi=xi'; 
           end

           if isfield(xv,'v1') & size(xv.v1,1)==1 then 
               xv.v1=xv.v1'; 
           elseif isfield(xv,'v1') & size(xv.v1,2)==1 then
               xv.v1=xv.v1;
           elseif ~isfield(xv,'v1') then
               xv.v1='v1_'+string([1:1:p1]');
           else 
               error('not proper field v1')
           end
           
           if isfield(xv,'v2') & size(xv.v2,1)==1 then 
               xv.v2=xv.v2';
           elseif isfield(xv,'v2') & size(xv.v2,2)==1 then 
               xv.v2=xv.v2;    
           elseif ~isfield(xv,'v2') then
               xv.v2='v2_'+string([1:1:p2]');
           else 
               error('not proper field v2')
           end
           
           if isfield(xv,'v3') & size(xv.v3,1)==1 then 
               xv.v2=xv.v3';
           elseif isfield(xv,'v3') & size(xv.v3,2)==1 then 
               xv.v2=xv.v3;    
           elseif ~isfield(xv,'v3') then
               xv.v3='v3_'+string([1:1:p3]');
           else 
               error('not proper field v3')
           end

           
           if size(xi)~=[n1 1] | size(xv.v1)~=[p1 1] | size(xv.v2)~=[p2 1] | size(xv.v2)~=[p3 1] then error ('no proper dimensions for i or v');
           else  xs.d=x;
                 xs.i=string(xi);
                 xv.v1=string(xv.v1);
                 xv.v2=string(xv.v2);
                 xv.v3=string(xv.v3);
                 xs.v=xv;
           end
       end
   
       if argn(2)<4 then 
           infos=[];
       end
    
       if size(xs.d,1)==size(xs.i,1) & size(xs.d,2)==size(xs.v.v1,1) & size(xs.d,3)==size(xs.v.v2,1)   & size(xs.d,4)==size(xs.v.v3,1) then
         xs2=tlist(['div','d','i','v','infos'],xs.d,string(xs.i),xs.v,infos);
       else
         error ('div dimensions do not agree') 
       end
   

   
   
   // autres cas possibles
   else error('the input is not and cannot be set to a div tlist')    
       
   end;

   // suppression des blancs avant et après les chaines de caractères (pas au milieu):
   xs2.i=stripblanks(xs2.i);
   if typeof(xs2.v)=='st' then
      xs2.v.v1=stripblanks(xs2.v.v1);
      xs2.v.v2=stripblanks(xs2.v.v2);
   else 
      xs2.v=stripblanks(xs2.v); 
   end
   


endfunction

