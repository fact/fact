function res=nns_nnplsrepeat(x,xplus,y,vl,hn)
    
    // cree 100 modèles pour vl variables latentes et hn neurones cachés
   
    res.rmsec=zeros(100,1);
    res.wh=[];
    res.wo=[];
    res_pls=ikpls(x,y,2,vl);
    x2=[xplus res_pls.scores.d];
        
    for i=1:100;    
        [wh,wo]=nns_init(x2,y,hn);
        [res_nn]=nns_train(wh,wo,x2,y);
        ypred=c_nnsimul(res_nn.wh.d,res_nn.wo.d,x2);
        differ=ypred-y;
        res.rmsec(i)=sqrt(differ'*differ/size(x,1));
        res.wh=[res.wh res_nn.wh.d(:)];
        res.wo=[res.wo res_nn.wo.d(:)];
    end


    
    
endfunction
