function[result]=pcana(x,centered,stdze,dim)

  // s_pca        principal component analysis

  // this function replaces the 'pca' and 'pca1' Saisir functions;
  // however the 'pca' function (number of observations < number of variables)
  // has been dropped because of its low numerical stability

  // updated: 2016, July, 20

  // vérification des arguments en entrée, du nombre (facultatif) de vecteurs-propres à utiliser
  
  x0=div(x);
  [nout,nin] = argn(0)

  // définition de centered et stdze par défaut 
  if nin==1 then 
      centered=0;
      stdze=0;
  elseif nin<=2 then
      stdze=0;
  end
  
  // définition de dim par défaut 
  if nin<=3 then
      r=rank(x0.d);
  elseif nin==4 then
      r=min(dim,rank(x0.d));
  else
      error('between 1 and 4 input arguments expected')
  end;

 

  // application of the centering and standardization options
  if centered==0 then
      xc=x0;
  elseif centered==1 then
      xc=centering(x0);
  end

  if stdze==0 then
      saisir=xc;
  elseif stdze==1 then
      saisir=standardize(xc);
  end

  [n,p]=size(saisir.d);

  x_mean.d=mean(x0.d,1);  // corrige 16/07/15: saisir remplace par x0
  x_stdev.d=std(x0.d);

  xcentred=saisir.d; //-ones(n,1)*x_mean.d*centered;


  if n < p
      [U,S,V]=svd( xcentred*xcentred' );
      V = xcentred'*V;
      V=V./(ones(size(V,1),1)*sqrt(diag(V'*V)')); //normalise les loadings
  else
      [U,S,V]=svd( xcentred'*xcentred );
  end;
  // xcpxc=xcentred'*xcentred;    // meilleure SVD sur X'X que sur X
  // xcpxc=(xcpxc+xcpxc')/2;      // donc X'X est bien parfaitement symétrique

  eigenval.d=diag(S);
  eigenval.d=eigenval.d(1:r);   
   
  eigenvec.d=V(:,1:r);

  ind_scores.d=xcentred*eigenvec.d;

  // identifiers
  eigenvec.i=saisir.v;
  
  //8avril22
  if r<9; 
     labels_pc='PC' + ' ' + string([1:1:r]');
  else 
     labels_pc=repmat('X',r,1);
     for i=1:9;
        labels_pc(i)='PC' + ' ' + string(i);
     end;
     for i=10:r;
        labels_pc(i)='PC' + string(i); 
     end;
  end;    

  eigenval.i=labels_pc;
  
  
  ev_pcent=eigenval;  // valeurs propres en pourcentage
  ev_pcent.d=100*ev_pcent.d/sum(ev_pcent.d);
  
  ev_pcent.v='variance %';

  //aux=round(eigenval.d'/sum(eigenval.d)*1000)/10;
  aux=(eigenval.d'/sum(eigenval.d)*1000)/10;
  aux=aux';
  
  ind_scores.i=saisir.i;
  ind_scores.v=repmat('',r,1);        // 26 aout 16
  
  for j=1:r;
     ind_scores.v(j)=labels_pc(j) + '  '+ msprintf('%3.3g',aux(j)) + '%';
  end

  eigenvec.v=ind_scores.v;
  eigenval.v='eigenvalues'; 

  x_mean.d=x_mean.d';
  x_mean.i=saisir.v;
  x_mean.v='x_mean';

  x_stdev.d=x_stdev.d';
  x_stdev.i=saisir.v;
  x_stdev.v='x_stdev';

  var_scores.d=eigenvec.d .* (ones(p,1)*sqrt(eigenval.d)');
  var_scores.i=eigenvec.i;
  var_scores.v=eigenvec.v;


  // sorties div:

  result.scores=[];
  result.scores=div(ind_scores);
  result.scores(5)=['observations';'scores of the observations onto the eigenvectors'];

  result.var_scores=[];
  result.var_scores=div(var_scores);
  result.var_scores(5)=['initial variables';'scores of the raw eigenvectors'];

  result.eigenvec=[];
  result.eigenvec=div(eigenvec);
  result.eigenvec(5)=['initial variables';'scores of the normalized eigenvectors'];

  result.eigenval=[];
  result.eigenval=div(eigenval);
  result.eigenval(5)=['dimension';'eigenvalues'];

  result.ev_pcent=[];
  result.ev_pcent=div(ev_pcent);
  result.ev_pcent(5)=['number of dimensions';'explained variance'];

  result.x_mean=[];
  result.x_mean=div(x_mean);

  result.x_stdev=[];
  result.x_stdev=div(x_stdev);

  result.centred=centered;

  result.std=stdze;



endfunction
