function [xc] = detrending(x,varargin)

 
 xc=div(x);
 
 [n,q]=size(xc.d);
 
 if argn(2)==2 then
     po=varargin(1);
 elseif argn(2)==1 then
     po=2;
 else 
     error('wrong number of input arguments in detrending')
 end
 
 v=vanderm(q,po);
 
 xc.d=xc.d*(eye(q,q)-v*inv(v'*v)*v');
 
 
endfunction

