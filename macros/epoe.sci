function [res] = epoe(xg,classes_echant,xtest,ytest,split,lv,varargin)
    
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  if argn(2)==6 then
   centrage=1;
  elseif argn(2)==7 then
   centrage=varargin(1);
  end
   
  [dmatrix] = pop_dextract('epoe',xg,classes_echant);
  res=pop_dtune(dmatrix,20,xg,classes_echant,xtest,ytest,split,'pls', centrage,lv)   
         

endfunction
