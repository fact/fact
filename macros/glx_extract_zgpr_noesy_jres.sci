
function [x_zgpr,x_noesy,x_jres_sum,x_jres_sum_smooth,x_jres_all]=glx_extract_zgpr_noesy_jres(list_rmn_spectra_names, out_spectra_names)
    
    // maj 9jan24
    // list_rmn_spectra_names: une liste (n x 1) donnant les noms des repertoires contenant les spectres RMN 
    // out_spectra_names: si on veut donner un autre nom aux spectres RMN de sortie 
    // x_zgpr: les spectres zgpr, en ligne              (131072 variables = valeurs de ppm)
    // x_noesy: les spectres noesy, en ligne            (131072 variables = valeurs de ppm)
    // x_jres_sum: les spectres jres, sommés, en ligne  (16384 variables = valeurs de ppm, 1/8 du zgpr ou noesy)
    // x_jres_sum_smooth  idem, spectres extrapolés sur 131072 variables et lissés/ plage=0,001 = 5 fois 0,0002
    // x_jres_all, une hypermatrice                     (16384 x 128 x n)
    
    // un spectre = un répertoire 
    // la liste contient des noms de répertoire
    // dans chaque répertoire le spectre est dans: 
    //    zgpr:  /10/pdata/1/1r
    //    noesy: /11/pdata/1/1r
    //    jres:  /12/pdata/1/2rr
    // les parametres ppm sont calculés à partir de /10/pdata/procs
  
    // ------------------------------------
    // calcul des ppm d'apres le 1° spectre
    // ------------------------------------
    
    // mise de list_rmn_spectra_names sous forme de vecteur-colonne
    if size(list_rmn_spectra_names,1)==1 then
        list_rmn_spectra_names=list_rmn_spectra_names';
    end
 
    if argn(2)==1 then
        out_spectra_names=list_rmn_spectra_names;
    end
    
    n=size(list_rmn_spectra_names,1);
    
   
    flag_0=0;
    for i=1:n;

        if flag_0==0; 
            dir_temp_10=list_rmn_spectra_names(i)+'/10/pdata/1/';    
            if isfile(dir_temp_10+'procs') then 
                h=mopen(dir_temp_10+'procs','rt'); 
                x=mgetl(h,-1);   // -1 = toutes les lignes 
                mclose(h);

                // extraction des 4 paramètres: OFFSET, SW_p, SI et SF 
                // extraction de DTYPP et BYTORDP (encodage)
                index_offset=grep(x,'##$OFFSET=');
                index_sw_p=grep(x,'##$SW_p=');
                index_si=grep(x,'##$SI=');
                index_sf=grep(x,'##$SF=');
                index_dtypp=grep(x,'##$DTYPP=');
                index_bytordp=grep(x,'##$BYTORDP=');
               
                flag1=0;
                flag2=0;
                flag3=0;
                flag4=0;
                flag5=0;
                flag6=0;
        
                if flag1==0 & index_offset~=[] then
                    xtemp=tokens(x(index_offset,:),'=');
                    OFFSET=strtod(xtemp(2));
                    flag1=1;
                end
             
                if flag2==0 & index_sw_p~=[] then
                    xtemp=tokens(x(index_sw_p,:),'=');
                    SW_p=strtod(xtemp(2));
                    flag2=1;
                end
                
                if flag3==0 & index_si~=[] then
                    xtemp=tokens(x(index_si,:),'=');
                    SI=strtod(xtemp(2));
                    flag3=1;
                end
                
                if flag4==0 & index_sf~=[] then
                    xtemp=tokens(x(index_sf,:),'=');
                    SF=strtod(xtemp(2));
                    flag4=1;
                end
              
                if flag5==0 & index_dtypp~=[] then
                    xtemp=tokens(x(index_dtypp,:),'=');
                    dtypp_temp=strtod(xtemp(2));
                    if dtypp_temp==0 then 
                        DTYPP='i'; // little 
                    else
                        DTYPP='l'; // big endian  
                    end     
                    flag5=1;
                end
                
                if flag6==0 & index_bytordp~=[] then
                    xtemp=tokens(x(index_bytordp,:),'=');
                    bytordp_temp=strtod(xtemp(2));
                    if bytordp_temp==0 then 
                        BYTORDP=' l'; // little 
                    else
                        BYTORDP=' b'; // big endian  
                    end     
                    flag6=1;
                end

                encodage=DTYPP + BYTORDP;
                
                // calcul de la plage de ppm
                SW=SW_p/SF;
                pmin=OFFSET-SW;
                pmax=OFFSET; 
                dppm=SW/(SI-1);
    
                x_ppm=[pmin:dppm:pmax];
   
                // inversion de l'axe 
                x_ppm=x_ppm($:-1:1);
                
                flag_0=1;        // arrêt de la boucle       
      
            end                 // fin boucle isfile(dir_temp_10+'procs')
        end                     // fin boucle flag0==0
    end                 // fin boucle i=1:n  
     

    
    // ----------------------
    // extraction des signaux
    // ---------------------- 
  
    x_zgpr.d=[];
    x_zgpr.i=[];
    x_zgpr.v=x_ppm;
    x_noesy.d=[];
    x_noesy.i=[];
    x_noesy.v=x_ppm;
    x_jres_sum.d=[];
    x_jres_sum.i=[];
    x_jres_sum.v=string(x_ppm(1:8:$)');
    x_jres_all.d=[];    // remplace zeros(16384,128,n);                                 // 16384 x 128 x n 
    x_jres_all.i=x_ppm(1:8:$);
    x_jres_all.v.v1='v'+string([1:128]');
    x_jres_all.v.v2=[];
      

    for i=1:n;
        dir_temp_10=list_rmn_spectra_names(i)+'/10/pdata/1/';
        dir_temp_11=list_rmn_spectra_names(i)+'/11/pdata/1/';
        dir_temp_12=list_rmn_spectra_names(i)+'/12/pdata/1/';

        if isfile(dir_temp_10+'1r') then                            // 131072
                h=mopen(dir_temp_10+'1r'); 
                xi=mget(150000,encodage);
                mclose('all'); 
                x_zgpr.d=[x_zgpr.d;xi];   
                x_zgpr.i=[x_zgpr.i;out_spectra_names(i)];
        end
        
        if isfile(dir_temp_11+'1r')                                 // 131072
                h=mopen(dir_temp_11+'1r');
                xi=mget(150000,encodage);
                mclose('all');
                x_noesy.d=[x_noesy.d;xi];   
                x_noesy.i=[x_noesy.i;out_spectra_names(i)];
        end
        
        if isfile(dir_temp_12+'2rr')
                h=mopen(dir_temp_12+'2rr');
                xi=mget(15000000,encodage);
                mclose('all');
                xi2=matrix(xi,[16384,128]);                         // 16384 x 128
                x_jres_sum.d=[x_jres_sum.d;sum(xi2,'c')'];
                x_jres_sum.i=[x_jres_sum.i;out_spectra_names(i)];
                if x_jres_all.d==[] then                             //9jan24
                    x_jres_all.d=xi2;
                else
                    ntemp=size(x_jres_all.d,3);
                    x_jres_all.d(:,:,ntemp+1)=xi2;
                end   
                x_jres_all.v.v2=[x_jres_all.v.v2;out_spectra_names(i)];
        end
        

    end
 
 
    // export
    if x_zgpr.d ~=[] then  
        x_zgpr=div(x_zgpr);
    else
        x_zgpr=[];
    end
    
    if x_noesy.d ~=[] then
        x_noesy=div(x_noesy);
    else
        x_noesy=[];
    end
  
    
    if x_jres_sum.d ~=[] then
        x_jres_sum=div(x_jres_sum);
    else
        x_jres_sum=[];
    end

    if x_jres_all.d ~=[] then
        x_jres_all=div(x_jres_all);
    else
        x_jres_all=[];
    end
      
    // enlever au dessous de -0,5 et au dessus de 10? Etape d'après
 
    
    // -----------------------------------
    // extrapolation des ppm jres -> noesy      2jan24 
 
    // valeurs de départ 
    x=x_jres_sum;
    
    // valeurs-cible
    x_target=x_noesy;
   
    // extrapolation des points 
    width=0.001;                                                    // 5 fois 0,0002 
    bandwidth.v=x_target.v;
    n=size(x_target.v,1); 
    
    bandwidth.d=ones(1,n)*width; 
    bandwidth=div(bandwidth);
       
    x_jres_sum_smooth= simufilters_xl(x,bandwidth,width);    
    
    x_jres_sum_smooth=savgol_xl(x_jres_sum_smooth,21,0);
 
    
    
endfunction
