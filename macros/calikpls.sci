function [b,T,P,dof,W] = calikpls(x,y,nf0)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  nf = min(rank(x),nf0);

//  W=[]; 
//  R=[];
//  P=[];
//  Q=[];
//  T=[];
 
  b=zeros(size(x,2),nf0);
  T=zeros(size(x,1),nf0);
  P=zeros(size(x,2),nf0);
  W=zeros(size(x,2),nf0);
  Q=zeros(1,nf0);
  R=zeros(size(x,2),nf0);  
  dof=zeros(nf0,1);
  
  XY=x'*y;
  for i=1:nf;
    w=XY;
    w=w/sqrt(w'*w);
    r=w;
    for j=1:i-1;
      r=r-(P(:,j)'*w)*R(:,j);
    end
    t=x*r;
    tt=t'*t;
    p=x'*t/tt;
    q=(r'*XY)'/tt;
    XY=XY-(p*q')*tt;
    T(:,i)=t;
    P(:,i)=p;
    Q(i)=q;
    R(:,i)=r;
    W(:,i)=w;  				
  end

  b(:,1:nf)=cumsum(R(:,1:nf)*diag(Q(:,1:nf)'),2);
   
  dof(1:nf,1)=[1:1:nf]';
   
  ndiff=nf0-nf;  
  if ndiff>1 then
      b(:,$-ndiff+1:$)=b(:,nf)*ones(1,ndiff);
      T(:,$-ndiff+1:$)=T(:,nf)*ones(1,ndiff);
      P(:,$-ndiff+1:$)=P(:,nf)*ones(1,ndiff);
      W(:,$-ndiff+1:$)=W(:,nf)*ones(1,ndiff);
      dof($-ndiff+1:$,1)=dof(nf,1)*ones(1,ndiff);
  end

  // Rappel: 
  // le r de De Jong vérifie: t=x*r
  // pour le recalculer: 
  // t=x*w*inv(p'*w) donc r=w*inv(p'*w) 
  

endfunction
