function [res_classpls2da] = classplsda(x,y,lv1)


    // distributed under the CeCILL-C license
    // copyright INRA-IRSTEA 2013    

    // Entrées =============================================
    // x : une matrice (n x q)  
    // y : une matrice disjonctive (n x k)   (k classes)  
    // lv1 = nbre de dimensions de la PLS2
           
    // Sorties: ============================================
    // res_classpls2da.scores: une matrice (n x lv1)
    // res_classpls2da.loadings: une matrice (q x lv1)
    
    // maj 21 aout 23 (rajout de da 3° dimension) 
 

    y=conj2dis(y);                 
    // ngroups=size(y,2);
    
    [b,t,p,dof,r]=calsimpls(x,y,lv1);
    
    // Sorties:
    res_classpls2da.scores=t;
    res_classpls2da.rloadings=r;

    
endfunction
