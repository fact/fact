function res=spls(x,y,lv,obs_split,var_split,varargin)
    
     if  argn(2) <= 5 then
       cent=1;
     else
       cent=varargin(1);
     end
   
     res=cvreg(x,y,obs_split,'stackedpls',cent,lv,obs_split, var_split);
     
endfunction
