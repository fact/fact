function [x_zgpr,x_noesy,x_jres_sum,x_jres_sum_smooth,x_jres_all]=glx_extract_zgpr_noesy_jres_from_zip(zip_file,which_directory)
    
    // maj: 2jan24
    // zip_file:            un fichier .zip contenant un répertoire par spectre RMN  
    // which_directory:     un répertoire temporaire pour décompresser le .zip
    // x_zgpr:              spectres zgpr                                               (n x 131072)
    // x_noesy:             spectres noesy                                              (n x 131072)
    // x_jres_sum:          spectres jres sommés sur les 128 lignes                     (n x 16384)
    // x_jres_sum_smooth    spectres jres sommés sur les 128 lignes + ppm extrapolés    (n x 131072)
    // x_jres_all           spectres jres complets                                      (16384 x 128 x n)
    
    
    // extraction dans un repertoire temporaire
    if argn(2)<2 then
        which_directory='./temp/';
    end
         
    rmdir(which_directory,'s');    // cas ou le repertoire existe deja
    mkdir(which_directory);

    files=decompress(zip_file,which_directory);
     
    // contenu du repertoire
    dir1=dir(which_directory);
    dir_list=dir1(2);    
   
    path_dir=which_directory+'/'+dir_list;
 
    // liste des repertoires et fichiers 
    // note: les repertoires et fichiers n'etant pas des spectres RMN sont automatiquement ecartes
    [x_zgpr,x_noesy,x_jres_sum,x_jres_sum_smooth,x_jres_all]=glx_extract_zgpr_noesy_jres(path_dir,dir_list);
    
    // nettoyage du répertoire temporaire
    rmdir(which_directory,'s');
     
endfunction
