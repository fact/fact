function [data0] = scsv2sci(filename)                                    
  
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
                                                
  filename=convstr(filename,'u');
  aux=strindex(filename,'.CSV');
  if(isempty(aux))
     filename=[filename + '.CSV'];
  end

  a=mgetl(filename);
  a=strsubst(a,'""','');     // enlève d'éventuels guillemets
                                    
  for i=1:size(a,1);                                                                  
     xline=a(i); 
     aux1=strindex(xline,';');   
     separation=strsplit(xline,';');   
     separation=separation';
     data0(i,:)=separation;  
  end                                                                                      
  

endfunction                                                                              
                                        

