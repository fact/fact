function [saisir] = msc(saisir0,reference)
 
  //msc        - Multiplicative scatter correction on spectra
  // function [X1] = msc(X,(reference))
  //
  //Input arguments:
  //================
  //X:SAISIR matrix of data (n x p)
  //reference (optional): SAISIR vector of a reference spectrum (default:
  //average of X)
  //
  //Output:
  //======
  //X1: data corrected by the Multiplicative scatter correction method.
  //
  // fonction ecrite par D.Bertrand
  

  saisir1=div(saisir0);

  if argn(2)==1 then
     model=mean(saisir1.d,'r');
  else
     reference=div(reference);
     model=reference.d'; 
  end;   
  
  [nmodel,pmodel]=size(model);
  
  if nmodel==1 then
      model=model';
  end
  
  // verification des dimensions
  [nmodel,pmodel]=size(model);
  [n,p]=size(saisir1.d);
  
  if nmodel ~= p then 
     error('the raw spectra and the reference spectrum should have the same number of variables' )
  end
  
  
  // initialisation
  saisir.d=zeros(n,p);
  
   
  // construction de la MSC:
  y=[ones(p,1) model];

  for i=1:n;
     bcoeffs=calmlr(y,saisir1.d(i,:)');
     saisir.d(i,:)=(saisir1.d(i,:)-bcoeffs(1)*ones(1,p))/bcoeffs(2);
  end   

  saisir.v=saisir1.v;
  saisir.i=saisir1.i;
  
  saisir=div(saisir);

  endfunction


