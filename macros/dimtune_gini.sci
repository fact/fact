function [res] = dimtune_gini(A00);
// [Gini_X] = Gini_Op(dir,A);
//
// Calculates the Gini criterion for a matrix A.
//
// 23.04.2020
//
// INPUT :
// dir   'row' for calculation by rows and any other string or value for
//       calculation by columns
// A     signal (e.g., a loading vector matrix, or a matrix of vectors of
//       regression coefficients)
//
// OUTPUT :
// Gini_X : Matrix with the values for the Gini criterion
// for each row or column

// REFERENCES :
//
//

    // JCB on travaille sur les lignes -> enlevé l'option ligne/colonne  
    //
    //    if strcmp(dir,'row')
    //        // By Rows
    //    else
    //        // By Columns
    //        A=A';
    //    end
    
    A0=div(A00);
    A0=A0';
    A=A0.d;

    [nR, nC] = size(A);

    p=[1:nC]';

    for i=1:nR
        w=abs(A(i,:))';
        Gini_X(i,:) = dimtune_ginicoeff(p, w);    
    end

    res.d=Gini_X;
    res.i=A0.i;
    res.v='gini';
    res=div(res);


endfunction
