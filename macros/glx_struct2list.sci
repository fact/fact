function  xoutput=glx_struct2list(xinput)

   if xinput.typeof=='list' then
      xoutput=list();
      fields_list=fieldnames(xinput);
      nlist=size(fields_list,1)-1;   // on enlève le champ réservé à typeof  

      for i=1:nlist;
          execstr(msprintf('listi=xinput.list%s;' , string(i)));

        if typeof(listi)=='st' & isfield(listi,'typeof') then  
          execstr(msprintf('listitypeof=xinput.list%s.typeof;' , string(i)));
          if listitypeof=='div' then 
              xoutput(i)=[];
              xoutput(i)=glx_struct2div(listi);  
          elseif listitypeof=='constant' | listitypeof=='hypermat' | listitypeof=='string' then 
              xoutput(i)=xinput.list1;
          elseif listitypeof=='st' then
              //pause
              //execstr(msprintf('xoutput(i)=glx_mat2div_struct2div(%s);',listi));
              xoutput(i)=glx_mat2div_struct2div('listi');
              
          end
        else 
            error('type not supported')
        end
      end
   else
      error('typeof==list expected')
   end
 

endfunction
