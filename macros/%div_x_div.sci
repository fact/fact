function c=%div_x_div(a,b)

       // multiplication élément par élément
       
       c.d=a.d.*b.d;
       c.i=a.i;
       c.v=a.v;
 
       c=div(c);
       
       c.infos=a.infos;

endfunction
            

