function res=ccswa(col,xndim);

  //accps      - Analyse en composantes communes et poids spectifiques
  //function accps_type=accps(col,(ndim));
  //
  //Input arguments
  //===============
  //col        : vector of SAISIR matrices with the same number of rows
  //ndim       : number of dimensions. Default: number of tables
  //
  //Output arguments
  //===============
  //accps_type with fields
  //   global : global results with fields
  //           score : scores of the compromise
  //           weight: weights of the dimensions for each table
  //   individual : vector of results for each table with fields
  //           score : scores of the considered table
  //           loadings: loadings of the table
  //           TableNorm: norm of the table
  //           mean : average of the table
  //
  // ACCPS is the french name of "Common component and specific weights
  // analysis."
  //
  // Source of the algorithm: Hanafi M, Qannari Q, Kohler A, Mazerolles G
  // Common component and specific weights analysis: new properties and
  // 10ème Journée Européennes Agro Industries et méthodes Statistiques.
  // Louvain La Neuve (Belgium), 23, 24 25 janvier 2008.
  //
  // Argument "col" is obtained by commands such as:
  // col(1)=data1; col(2) = data2; ...;col(m)=datam.
  //In which data1, data 2,...,datam are SAISIR matrices with the same number
  //of rows.
  // see also: apply_accps, comdim (accps with an other algorithm)

  // get Scilab version 
  sci_version=part(getversion(),[8,9,10]);  // a string
  sci_version=strtod(sci_version);          // a number 
  if sci_version < 6 then
      accolade1='(';
      accolade2=')';
  else 
      accolade1='{';
      accolade2='}';
  end

  ntable=size(col); 
  for i=1:ntable;
      xs=div(col(i));
      col(i)=xs;
  end


  U=cell();
  xscore=cell();
  [nargout,nargin] = argn(0) // added by matlab2sci
  threshold=10^(-30);////default value of the threshold

  if(nargin<2) then ndim=ntable;   /// default values of the dimensions
     else  ndim=xndim;
  end

  [nobs,p]=size(col(1).d);//// to know the number of observations
 
  //// centered table and same inertia
  xmean_table=list();
  
  for i=1:ntable
      col_i=col(i);
      [col_i,xmean_i]=centering(col_i);
      xmean_table(i)=xmean_i;
      aux=col_i.d;
      inertia(i)=sqrt(sum(aux.*aux));
      col_i.d=col_i.d/inertia(i);//// necessary to eventually keep the inertia
      col(i)=col_i;
      
  end

  kept=col; //// patch to compute the score at the end for a simple multiplication
  q=ones(nobs,1);  //// to keep the compromise
  
  for i=1:ntable
      [n1,p1]=size(col(i).d);
      aux=ones(p1,1);   
      execstr(msprintf('U%s i %s.entries=aux/sqrt(sum(aux.*aux));', accolade1,accolade2))   //// accolade needed: heterogeneous sizes
      execstr(msprintf('xscore%s i %s.entries =zeros(nobs,1);',accolade1,accolade2))   //// needed purely to give a value

  end

  aux=ones(nobs,1);
  Q(:,1)=aux/sqrt(sum(aux.*aux));             //// initialization of the compromise scores

  
  //// main loop ==========================
  for dim=1:ndim    //// loop on dimensions
    //xdisp('Dimension ', dim,' among ', ndim);
    for i=1:ntable
      [n1,p1]=size(col(i).d);
      aux=sum(col(i).d.*col(i).d,1)';     //// good initalization ??
      execstr(msprintf('aux1=U%s i %s.entries;',accolade1,accolade2))
      aux1(:,dim)=aux/sqrt(sum(aux.*aux,1));
      execstr(msprintf('U%s i %s.entries=aux1;',accolade1,accolade2))  //// accolade needed: heterogeneous sizes
    end

    aux=ones(nobs,1);
    Q(:,dim)=aux/sqrt(sum(aux.*aux,1));
    iter=0;
    alpha=zeros(1,ntable);
    PreviousCov4=-10;//// criterion of breaking, intialised at dummy value
    while(%T);//// loop for convergency
      iter=iter+1;
      table=[];
      score=[];
      for j=1:ntable ////  cumulating the current individual scores
          execstr(msprintf('u=U%s j %s.entries;',accolade1,accolade2))
          u=u(:,dim);//// current value of u for table i
          //st_deviation( xxxx,1 [REMINDER needs explicit dimensionsu)
          aux=col(j).d*u;                 // projection
          alpha(j)=sum(aux.*q)/nobs;      // step e in page 112
          if(alpha(j)<0)
             u=-u;
             alpha(j)=-alpha(j);          // modification from the article
             aux=-aux;
          end
          table=[table alpha(j)*aux];     // step f in page 112
      end

      thispca=pcana(div(table));          //matrix2div
      aux=thispca.scores.d(:,1);
      q=aux/sqrt(sum(aux.*aux));          // step g in page 112
             // regression coefficient between q and individual scores
             //rcoeff=cormap(matrix2saisir(q),matrix2saisir(table));
      for j=1:ntable ////step h page 112
          aux=col(j).d'*q;
          aux=aux/sqrt(sum(aux.*aux));
          execstr(msprintf('aux1=U%s j %s.entries;',accolade1,accolade2))  //// reactualising the matrices U, which are specific of each table !!
          aux1(:,dim)=aux;//// filling the right dimension
          execstr(msprintf('U%s j %s.entries=aux1;',accolade1,accolade2))  //// reactualising the arrays of cells containg matrices U
            //size(U(i).entries [******** some work required])
 
      end
      //// testing the convergency by computing cov4 (formula page 110 and 112)
      cov4=sum(alpha(i).^4);
      aux=abs(cov4-PreviousCov4);
      PreviousCov4=cov4;
      deltamodif=aux/cov4;
      //xdisp('Delta modification (//)', deltamodif*100, ' expecting less than', threshold*100 );
      //end
      Q(:,dim)=q;
      //// the convergency loop is exited when the cov power4
      //// changes to a proportion less than the threshold.
      if(deltamodif<threshold) then break;   //// criterion for breaking;
      end
    end //// loop on convergency
          //xdisp('converged with dimension #', dim);
    
          // matrix deflation and keeping the alphas and the scores
    for j=1:ntable
        aux=col(j).d;
        [n1,p1]=size(aux);
        col(j).d=col(j).d-q*q'*col(j).d;
        xalpha(j,dim)=alpha(j);//// here the weights table x dim;
    end
  end//// loop on dimension
  //// keeping the result
  //// individual results
  aux=string((1:ndim)');
  xdim='dim'+aux;  // replaces addcode
  //// keeping the individual results
  for table =1:ntable
      execstr(msprintf('scores.d=xscore%s table %s.entries;',accolade1,accolade2))
      scores.i=col(table).i;
      scores.v=xdim;
      execstr(msprintf('loadings.d=U%s table %s.entries;',accolade1,accolade2))
      loadings.i=col(table).v;
      loadings.v=xdim;
      loadings=div(loadings);
      xinertia=inertia(table);
      //// it is necessary to compute again the scores
      individual(table).scores=[];
      individual(table).scores=kept(table)*loadings;  //smult
  //    individual(table).scores=div(individual(table).scores);
      loadings=div(loadings);
      individual(table).loadings=[];
      individual(table).loadings=loadings;
      individual(table).tablenorm.d=xinertia;
      individual(table).tablenorm.i='Frobenius norm of this table';
      individual(table).tablenorm.v='Frobenius norm of this table';
      individual(table).tablenorm=div(individual(table).tablenorm);
      individual(table).mean=[];
      individual(table).mean=xmean_table(i);
  end
  
  xglobal.scores.d=Q;
  xglobal.scores.i=col(1).i;
  xglobal.scores.v=xdim;
  xglobal.scores=div(xglobal.scores);
  //xglobal.loadings=loadings;
  xglobal.weights.d=xalpha;
  xglobal.weights.v=xdim;
  aux=string((1:ntable)');
  xglobal.weights.i='table' + aux;
  xglobal.weights=div(xglobal.weights);
  res.global=[];
  res.global=xglobal;
  res.individual=individual;
 
  // format Div:
 // res.global.scores=div(res.global.scores);
 // res.global.weights=div(res.global.weights);

 
endfunction


