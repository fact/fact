function [w] =wilks(x1,classe)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  x=div(x1);
  y=div(classe);
  
  testdisj=isdisj(y.d);
  testconj=isconj(y.d);
  
  if testdisj=='T' then
  elseif testconj=='T' then
      y.d=conj2dis(y.d);
  elseif testdisj =='F'& testconj=='F' then
      y.d=str2conj(y.d);
      y.d=conj2dis(y.d);
  end

  n1 = size(x.d,1);
  n2 = size(y.d,1);

  if  n1~=n2 then error('x and y do not have the same number of observations')
  end

  // centering of x  (indispensable pour que t soit une matrice de variance-covariance)
  x = centering(x); 


  // inter-variance 
  b = x.d'*y.d*inv(y.d'*y.d)*y.d'*x.d;
  // total variance 
  t = x.d'*x.d;

  w = trace(b) / trace(t);


endfunction
