function  x_out = div2csv(x_in,filename,separator,decimal)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  // x_out = div2csv(x_in,filename,separator,decimal)
 
  // writes a div structure (x_in) in a csv file (filename)
  // decimal and separator are optional parameters 
  // default values: '.' and ',' respectively
  // updated: 2015, august, 06. by JMR


  x=div(x_in);

  if argn(2) < 4 then
      decimal = '.';
  end
  if argn(2) < 3 then
      separator = ',';
  end

  m = [["\",x.v'];[x.i,string(x.d)]]

  if decimal~='.' then
      m=strsubst(m,'.',decimal);    
  end

  csvWrite(m, filename, separator);
    
  // sortie div
  x_out=div(x);  // ne sert que pour valider le script de test 



endfunction

 
