function [cld] = conj2dis(c_in,varargin);

 // function [cld] = conj2dis(cl,(nbcl),(T);
 // This function transforms a conjunctive vector into a disjunctive matrix
 // (nbcl columns)
 // 
 // Input:
 // ======
 // cl:                vector of P integers corresponding to classes
 // nbcl (optionnal):  used to force the dimension of cld
 //
 // Output:
 // =======
 // cld :              matrix of Booleans of dimensions P X ()number of classes)
 //   
 // Miscellaneous:
 // ==============
 // JM Roger (IRSTEA) and JC Boulet (INRA), France
 // Last update: 2014
 // -----------------------------------------------

 
 
   if argn(2)==1 then 
      nbcl = max(c_in);
   elseif argn(2)>=2 then
      nbcl=varargin(1)
   end;
  
   
   // vérif. si conjonctif ou disjonctif et attribuer lettre C ou D
   test_c=isconj(c_in);
   test_d=isdisj(c_in);
   if test_c=='T' & test_d=='F' then
       test='C';        // expliciter que conjonctif
   elseif test_c=='F' & test_d=='T' then
       test='D';
   end
   
   // pour forcer 'T' (cas d'un échantillon où toutes les classes ne sont pas représentées )
   if argn(2)==3 then       
       test=varargin(2);
   end   
   
   // transformation conj->disj
   if test=='C' then
     nbech = size(c_in,1);
     cld = zeros(nbech,nbcl);
     for i=1:nbcl
       x=find(c_in==i);
       if ~isempty(x) then
         cld(x,i)=1;
       end;
     end;

   elseif isdisj(c_in)=='T' then 
     cld=c_in;
   
   else 
     error('input classes are neither conjunctive nor disjunctive') 

 end


endfunction
