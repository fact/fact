function x_out = pareto(x)

  // maj: 28aout24
  
  // pareto = division par la racine carrée de l'ecart-type 
  
  // arguments: 
  // x:     matrice (n x q) ou div
  // x_out: div (n x q)
  
  
  x2=div(x);
  [n,q]=size(x2.d);

  x_stdev=stdev(x2.d,'r');   // ecart-tyme 
  x_pareto=sqrt(x_stdev); 

  aux=ones(n,1)*x_pareto;    // n x q

  x_out=x2;
  x_out.d=x_out.d ./ aux;  


endfunction

