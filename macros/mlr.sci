
function [model,rmsec,rmsecv,b,ypredcv] = mlr(xi,yi,split,cent)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  x=div(xi);
  y=div(yi);
  clear test;
  if x.i~= y.i then warning('labels in x and y do not match')
  end
     
  if argn(2)==3 then     // par défaut: centrage
      cent=1;
  end;
  
  [model] = cvreg(xi,yi,split,'mlr',cent);
    
  rmsec=model.err.d(:,1);
  rmsecv=model.err.d(:,2);
  b=model.b.d;
  ypredcv=model.ypredcv.d; 
  
 


endfunction


