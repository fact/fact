function [testvectsaisir,col]=islist(x);

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  testvectsaisir='T';
  
  if  type(x)==15 then   // 15 = liste (vecteur)
      nclasses=size(x); 
      col=list(); 
      for i=1:nclasses;
          xi=x(i);
          xi2=div(xi);
          col(i)=xi2;
      end
  else testvectsaisir='F';    
  end
         

endfunction

