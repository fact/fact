function z = dendro_linkage(d, method)

 [nargout,nargin]=argn(0);

  if (nargin < 1) | (nargin > 2)
    error ("Wrong number of inputs!");
  end

  if (isempty (d))
    error ("First input cannot be empty!");
  elseif ( nargin < 3 & ~ (size(d,1)==1 | size(d,2)==1)) then
    error ("First input must be a vector!");
  end
  if nargin<2 then
    method="single";
  end;
   method = ascii(convstr (method));
   method=ascii(method(1:3));
  method_name= [ "ave"; "cen"; "com"; "med"; "sin"; "war"; "wei"];
  
  if (~ or (method==method_name))
    error(msprintf("%s: unknown method!", method));
  end

 sz=(1+sqrt(1+8*max(size(d))))/2;
 dz=zeros(sz,sz);
 for i=1:sz-1
     iz=(sz*(i-1)+i+1):(sz*i);
     dz(iz)=d(iz-sum(1:i));
 end
 d=dz+dz';
  d(1:(sz+1):(sz*sz)) = %inf; 
  d=d($:-1:1,:);
  d=d(:,$:-1:1);
  clst = sz:-1:1; 
  wtm = ones (1, sz); 
  z = zeros (sz-1, 3); 
  for i = sz+1:2*sz-1
    [mn, indx] = mtlb_min (d(:));
    [r, c] = ind2sub (size(d), indx);
    z(i-sz, :) = [clst(r) clst(c) d(r, c)];
    clst(r) = i;
    clst(c) = [];
    // Computing the new distances
    x=d([r c], :);
   
    select(method)
     case "ave" then 
         q=wtm([r,c]);
         newd=mtlb_sum(diag(q)*x)/mtlb_sum(q);
     case "cen" then 
         newd=cdist(x, r, c, wtm);
     case "com" then
          newd=mtlb_max(x);
     case "med" then 
         newd=cdist(x, r);
     case "sin" then 
     newd=mtlb_min(x);
     case "war" then 
         x =x.^ 2;
         wr = wtm(r); 
         wc = wtm(c); 
         src = wr + wc;
         c2 = x(2,r) * src / wr / wc; 
         s = [wr + wtm; wc + wtm]; 
         p = [wr * wtm; wc * wtm];
         x = x.*( s ./ p); 
         cw = wr/src;
         x = cw*x(1,:) + (1-cw)*(x(2,:) - cw*c2);
         newd = sqrt (x * src .* wtm ./ (src + wtm));
     case "wei" then 
         newd=mtlb_mean(x); 
    end
    newd(r) = %inf;
    d(r,:) = newd;
    d(:,r) = newd';
    d(c,:) = [];
    d(:,c) = [];
    wtm(r) = wtm(r)+wtm(c); // weight updation
    wtm(c) = [];
  end
  z(:,1:2) = mtlb_sort (z(:,1:2), 2);

  if (or (diff (z(:,3)) < 0))
    warning (sprintf("Cluster distance monotonically not increasing!\n    A method different from %s may be used\n", method));
  end

endfunction


function y = cdist (d, p, q, w)
 [nargout,nargin]=argn(0);
  d = d.^2; 
  dd = d(2, p); 
  if (nargin < 4) 
    cw = 0.5; 
  else 
    cw = 1 / (1 + w(q) / w(p)); 
  end
  y = sqrt (cw*d(1,:) + (1-cw)*(d(2,:) - cw*dd));
endfunction


