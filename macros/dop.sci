function [ploads,ev_pcent] = dop(xi,yi,xo,yo,kernel_thresh,pca_thresh)

  //  (x,y):         jeu d'étalonnage; matrice (n x p) et vecteur (n x 1)
  //  xobs:          spectres observés en présence d'une perturbation; matrice (nobs x p)
  //  yobs:          valeurs de référence; vecteur (nobs x 1)
  //  kernel_thresh: seuil pour la taile du noyau dans DOP
  //  pca_thresh:    seuil pour le nombre de vecteurs propres retenus


  // gestion des arguments d'entrée
  if  argn(2)<6 then            // seuils par défaut
      pca_thresh=0.01;
  end
  if argn(2)<5 then
      kernel_thresh=0.01;
  end
  if argn(2)<4 | argn(2)>6 then
      error('wrong number of input arguments')
  end

  // gestion du format Saisir ou non 
  x2=div(xi);
  y2=div(yi);
  xobs2=div(xo);  
  yobs2=div(yo);
  
  x=x2.d;
  y=y2.d;
  xobs=xobs2.d;
  yobs=yobs2.d;
  
  // initialisation
  ploads.d=[];
  ploads.i=[];
  ploads.v=[];
  ev_pcent.d=[];
  ev_pcent.i=[];
  ev_pcent.v='eigenvalues';
  

  // début de DOP
  xcalc=kernel_rebuild(x,y,yobs,kernel_thresh);
  
  D=xobs-xcalc;

  model_pca=pcana(D);
  ploadings=model_pca.eigenvec.d;
  eigenval=model_pca.eigenval.d;
  eigenval=eigenval/sum(eigenval);        // expression en pourcentage; somme=1
  ndim=max(size(eigenval));
  
  cumul_eigenval=zeros(ndim,1);           // somme cumulée des valeurs propres
  for i=1:ndim;
      cumul_eigenval(i)=sum(eigenval(1:i));
  end
 
  lv=[];
  for i=1:ndim;
    if (cumul_eigenval(i)-(1-pca_thresh))>10*%eps then 
      lv=i;
      break
    else lv=ndim;
    end
  end
  
  // sorties Div
  
  ploads.d=ploadings(:,1:lv);
  ploads.i= x2.v;
  ploads.v='load'+ string([1:1:size(ploads.d,2)]') ;
  ploads=div(ploads);

  ev_pcent.d=eigenval;
  ev_pcent.i='ev'+string([1:1:size(ev_pcent.d,1)]'); 
  ev_pcent.v='ev_pcent';
  ev_pcent=div(ev_pcent);
   

endfunction

