function VIF=dimtune_var_inflat_factor(X,max_LVs);
// USAGE :
// [VIF, VIF_adj]=VIF_DNR_2020(X,max_LVs);
// This function calculates the Variance Inflation Factor
//   Inputs:
//        X = the scaled predictor block (rows, cols)
//
//   Outputs:
//        R2 = vector of Coefficients of Determination for the variables of X
//        VIF = vector of Variance Inflation Factors for the variables of X
// // Calculated doing PLS2_ED with from 1 to (at most) max_LVs
// Calculated doing PLS2_DNR_2020 with (at most) max_LVs
// between the matrix less each variable and the variable removed

//// Centre X
//Options.CN='N';
//
////////////// ADDED BY DNR
//release=version('-release');
//if str2num(release(1,1:4))>2014
//    parallel='T';
////     // Temporary
//    parallel='F';
//else
//    parallel='F';
//end
////////////// ADDED BY DNR

//////////// ADDED BY DNR
//if parallel=='T'
//    //////////// ADDED BY MdF
//    // Opens the parallel computing pool if not already opened
//    p=gcp('nocreate');
//    if isempty(p)==1 // Checks if a pool already exists
//        c = parcluster('local');
//        c.NumWorkers = feature('numcores'); // No. of workers to be used
//        parpool(c, c.NumWorkers);
//    end
//end

    // JCB: X est une matrice deflatée de max_LVs 


    [rows, cols] = size(X);
// LVs=1;
    max_LVs=min(max_LVs,floor(min(rows, cols)/2));

//////////// ADDED BY DNR
//if parallel=='T'
//    // SWITCHED TO PARFOR LOOP
//    parfor i = 1:cols
//        ////////// CHANGED BY MdF
//        //     [PLS2_Res]=PLS2_DNR_2020(X_cal,y_cal,max_LVs);
//        [PLS2_Res]=PLS2_DNR_2020(X(:,1:end~=i),X(:,i),max_LVs,Options);
//        ////////// CHANGED BY MdF
//        
//        y_mean=mean(X(:,i));
//        SStot=sum((X(:,i)-y_mean).*(X(:,i)-y_mean));
//        SSres=sum((X(:,i)-PLS2_Res.Yhat).*(X(:,i)-PLS2_Res.Yhat));
//        R2=1-(SSres./SStot);
//        VIF(:,i)=1/(1-R2);
//        
//        R2_adj=1-((1-R2)*(rows-1)/(rows-max_LVs-1));
////         R2_adj(:,i)=1-((SSres/(rows-max_LVs))./(SStot/(rows-1)));
//        VIF_adj(:,i)=1/(1-R2_adj);
//    end
//    
//else

    vect_vars=[1:cols]';

    for i = 1:cols;  //cols 
        ////////// CHANGED BY MdF
        //     [PLS2_Res]=PLS2_DNR_2020(X_cal,y_cal,max_LVs);
        //[PLS2_Res]=PLS2_DNR_2020(X(:,1:$~=i),X(:,i),max_LVs,Options);
        ////////// CHANGED BY MdF
        tri=find(vect_vars~=i);
        Xi=X(:,tri);
        Yi=X(:,i);
        b=calikpls(Xi,Yi,max_LVs);
        Yhat=Xi*b(:,$);

        
        y_mean=mean(Yi);
        SStot=sum((Yi-y_mean).*(Yi-y_mean));
        SSres=sum((Yi-Yhat).*(Yi-Yhat));
        
        //R2=1-(SSres./SStot);
        //VIF(:,i)=1/(1-R2);
        
        VIF(:,i)=SStot./SSres;
        
        
        //R2_adj=1-((1-R2)*(rows-1)/(rows-max_LVs-1));
        //R2_adj(:,i)=1-((SSres/(rows-max_LVs))./(SStot/(rows-1)));
        //VIF_adj(:,i)=1/(1-R2_adj);
    end


endfunction
