function   [b,t,p,dof,rloadings] = calstackedpls(xi,yi,lv,obs_split,var_split,cent); 
    
    // initialization
    dof=[1:1:lv]';
    t=[];           // no t or p as outputs!
    p=[];
    
    [n,q]=size(xi);
    
    // selection of the groups of variables 
    
    lot=cvgroups(size(xi,2),var_split);   // lot est disjonctif 
    
    if argn(2)>=8 then
        gama=varargin(1);
    else 
        gama=2;
    end
    
    
    // perform cross-validation on each interval to get combining weights
    
    nlot=size(lot,2);     // puisque lot est disjonctif  17aout23  
    
    eec=zeros(lv,nlot);   // transposed form of SDB Eec
    
    b0=zeros(q,lv,nlot);
    t0=zeros(n,lv,nlot);
    p0=zeros(q,lv,nlot);
    
    for i=1:nlot;
       var_i=find(lot(:,i)==1);     // 17aout23
       xsel=xi(:,var_i);
       [model_i] = cvreg(xsel,yi,obs_split,'ikpls',cent,lv);
       eec(:,i)=model_i.err.d(:,2);
       
       bi=model_i.b.d;
       bi2=zeros(q,lv);
       
       for j=1:max(size(var_i));
           bi2(var_i(j),:)=bi(j,:);         
       end

       b0(:,:,i)=bi2;
       
    end
    
    g2=(ones(lv,nlot)./ (eec.^2)).^gama;
    g2s=sum(g2,'c');
    g=g2./ (g2s*ones(1,nlot));
 
    
    for i=1:q;
        b0_i=matrix(b0(i,:,:),[lv nlot]);
        b0_i=b0_i.*g;       
        b0(i,:,:)=b0_i;
    end
    
    b=zeros(q,lv);

    
    for i=1:nlot;
        b=b+b0(:,:,i);
    end
    
    rloadings=[];
    
endfunction
