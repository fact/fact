function [x_saisir] = nandel(x,row_or_col)
//nan_delete		- suppresses "not a number" data in a saisir structure
//function [X1] = delete_nan(X,(row_or_col))
//
//Input argument:
//==============
//X:Saisir data matrix
//row_or_col:
// if row_or_col=='r' tries to have the maximum number of rows
// if row_or_col=='c' tries to have the maximum number of columns
//
//Output argument:
//===============
//X1:Saisir data matrix with no NaN values
//
//Only useful when a very few numbers of rows or columns contain NaN values

[nargout,nargin] = argn(0) 
saisir=div(x);
[n,p]=size(saisir.d);

 if(nargin>1) then  select_col=zeros(1,p);
                if(row_or_col=='c') then 
                   select_col=zeros(1,p);
                   for col=1:p
                       if(sum(isnan(saisir.d(:,col)))~=0); select_col(col)=1; end
                   end
                   x_saisir=cdel(saisir,select_col==1);
                   //disp([string(sum(select_col)) ' columns eliminated']);
                   return;
                elseif  (row_or_col=='r') then
                   select_row=zeros(1,n);
                   for row=1:n
                       if(sum(isnan(saisir.d(row,:)))~=0); select_row(row)=1; end
                   end
                   x_saisir=rdel(saisir,select_row==1);   
                   //disp([string(sum(select_row)) ' rows eliminated']);
                   return;
                else 
                end
 else     // while(1) then 
                   [n,p]=size(saisir.d);
                   row_choice=ones(1,n)*p-sum(isnan(saisir.d'));   // maximum data kept
                   col_choice=ones(1,p)*n-sum(isnan(saisir.d));
                   [minrow,indexrow]=min(row_choice);
                   [mincol,indexcol]=min(col_choice);
                   [minabs,indexabs]=min([minrow mincol]);
                      if(sum(sum(isnan(saisir.d)))==0) then
                         x_saisir=saisir;                            // no more Nan data
                      end
                   select indexabs //indexabs
                          case 1    x_saisir=cdel(saisir,indexcol);
                          case 2    x_saisir=rdel(saisir,indexrow);

                   end

 end

 x_saisir=div(x_saisir);


endfunction

