function [ASCA,ASCA2] = asca(X0,F0,centred)

// ASCA does a principal component analysis on the level averages of each
// experimental factor in a designed experiment with balanced data.
// Interactions between two factors can also be calculated
// Nomenclature: 
// Factor : experimental factor such as time or dose
// Level  : treatment belonging to a factor, for example three time points
//          correspond to three levels of the factor time, or two doses
//          correspond to two levels of the factor dose.
// Cell   : combination of two factors/levels         

// The program takes a data matrix X and a matrix F that describes the
// experimental setup as input.

// Input
// data matrix X  :   each row is a measurement, each column a variable
// design matrix F:   columns correspond to factors rows to levels. This
//                    matrix must be constructed by the researcher.
// interactions   :   vector with per row the factors for which interactions
//                    are to be calculated
// center         :   center == 1: center data; 
//                    center == 2 standardize data
//
// Output
// structure ASCA that contains scores, loadings singular values and
// projections of the
// factors and interactions
//
// GZ, 17 January 2012. Matlab 7.11.0
// Version 1.0
// GZ, 22 May 2012. Matlab 7.11.0
// Version 1.1
// - Added calculation of percentage explained
// - Added plotting of the interactions and their residuals
// GZ, 18 June, 2012
//Version 1.2
// - Allowed for cells with single subject. This caused a problem when 
//   calculating the interaction means. Put in an 'if' statement to catch
//   single subject cells.
// GZ, 2 October, 2012
//Version 1.3
// - Added percentage 'explained' for overall mean, factors, interactions
// and residuals.
// Version 1.3.1
// - Option center==0 now correctly implemented.
// GZ, 26 February, 2013
// Version 1.3.2
// - Fixed bug to calculate multiple interactions
// GZ, 19 July, 2013
// GZ, 16 Oct. 2013, removed option center==0
//
//
// Copyright: Gooitzen Zwanenburg
// This code is available under APACHE Licence 2.0
// http://www.apache.org/licenses/LICENSE-2.0.html


    X0=div(X0);
    F0=div(F0);


    // preparation au format div
    X=X0.d;
    F=F0.d; 
    
    if argn(2)<3 then
        centred=1;              // centré-non standardisé par défaut 
    end
    
    // calcul des combinaisons d'interactions 
    n=size(F,2); ...
    interactions=[]; ...
    for i=1:n-1; ...
        for j=i+1:n; ...
            itemp=[i j]; ...
            if interactions==[] then ...
                interactions=itemp; ...
            else ...
                interactions=[interactions; itemp]; ...
            end; ...
        end; ...
    end; ...

    size_data           = size(X);                   // size of the data matrix 
    n_levels            = max(F,'r');                    // number of levels / factor
    n_interactions      = size(interactions,1);      // number of interactions
    n_factors           = size(F,2);                 // number of factors
    f_factors             = 1 : n_factors;             // factors to be evaluated
    X_raw               = X;                         // Input data matrix
    X_level_means       = cell(n_factors,1);         // level_means per factor  
    SSQ_factors         = zeros(1,n_factors,1);      // sum of squares for factors
    X_interaction_means = cell(n_interactions);      // cell_means for interactions 
    SSQ_interactions    = zeros(1,n_interactions);   // sum of squares for interactions
    
    // Structure with results
    ASCA.factors.scores               = cell(n_factors,1);
    ASCA.factors.loadings             = cell(n_factors,1);
    ASCA.factors.projected            = cell(n_factors,1);
    ASCA.factors.singular             = cell(n_factors,1);
    ASCA.factors.explained            = cell(n_factors,1);
    ASCA.interactions.scores          = cell(n_interactions);
    ASCA.interactions.loadings        = cell(n_interactions); 
    ASCA.interactions.singular        = cell(n_interactions); 
    ASCA.interactions.explained       = cell(n_interactions); 
    
    // In column space
    ASCA.factors.means                = cell(n_factors,1);
    ASCA.interactions.means           = cell(n_interactions);
    
    // center/standardize the data
 
    if centred == 1 then
        Mean = ones(size_data(1),1)*mean(X_raw,'r');        // Overall mean
        X = (X_raw - Mean);                             // Center
        SSQ_mean = sum(sum(Mean.^2));                   // SSQ overall means
        SSQ_X = sum(sum(X_raw.^2));                     // Sum of squares data matrix
    elseif centred == 2 then 
        Mean_std = (ones(size_data(1),1)*mean(X_raw,'r')) ./ (ones(size_data(1),1)*std(X_raw));
        X_std = X_raw./(ones(size_data(1),1)*std(X_raw));
        X = (X_std - Mean_std);                         // Standardize
        SSQ_mean = sum(sum(Mean_std.^2));               // SSQ overall means
        SSQ_X = sum(sum(X_std.^2)); 
        pause
    end
    X_residuals         = X;                            // initial matrix with residuals    
    ASCA.data           = X;
    ASCA.design         = F;
   
    // Collect level means for the factors indicated in the model 
    for asca_factor = f_factors 
        X_level_means{asca_factor} = zeros(size_data(1),size_data(2));
        for level = 1 : n_levels(asca_factor)
            tmp = zeros(size_data(1),1);
            found = find(F(:,asca_factor) == level);      // find rows that belong to level       
            m = mean(X(found,:),'r');                    // calculate level mean 
            tmp(found) = 1;                          // flag the rows found
            X_level_means{asca_factor} = X_level_means{asca_factor} + tmp*m;
        end
        SSQ_factors(asca_factor) = sum(sum(X_level_means{asca_factor}.^2));
        X_residuals = X_residuals - X_level_means{asca_factor};
        ASCA.factors.means{asca_factor} = X_level_means{asca_factor};
    end
   
    // Interactions
    for i = 1 : n_interactions                                            
        factor_1 = interactions(i,1);
        factor_2 = interactions(i,2);
        X_interaction_means{i} = zeros(size_data(1),size_data(2));
        for level_factor_1 = 1 : n_levels(factor_1)          // levels for first factor             
            for level_factor_2 = 1 : n_levels(factor_2)      // levels for second factor 
                tmp = zeros(size_data(1),1);
                found = find((F(:,factor_2) == level_factor_2) & ...  // find rows 
                             (F(:,factor_1) == level_factor_1));
                if max(size(found)) == 1                        // only one subject/cell
                    m = X(found,:);                          // average is row
                else
                    m = mean(X(found,:),'r');                    // average over cell
                end
                tmp(found) = 1;
               // pause
                X_interaction_means{i} = X_interaction_means{i} + tmp*m;
            end
        end
        X_interaction_means{i} = X_interaction_means{i} - ...
                                 X_level_means{factor_1} - X_level_means{factor_2};
        SSQ_interactions(i) = sum(sum(X_interaction_means{i}.^2));   
        ASCA.interactions.means{i} = X_interaction_means{i};
        X_residuals = X_residuals - X_interaction_means{i};                     
    end   
         
    SSQ_residuals = sum(sum(X_residuals.^2));
    perc_effects = asca_effect_explains(SSQ_X, SSQ_mean, SSQ_factors, ...
                                   SSQ_interactions, SSQ_residuals);
    ASCA.effects = perc_effects;
    ASCA.residuals = X_residuals;
    

    // ---------------------------------------
    //Do PCA on level averages for each factor
    ASCA2.factors.scores=list();
    ASCA2.factors.loadings=list();
    ASCA2.factors.singular=list();
    ASCA2.factors.residual_scores=list();
    ASCA2.factors.explained=list();
    ASCA2.factors.scores_and_resid=list();
    
    for i = 1 : n_factors
        [t,s,p] = asca_do_pca(X_level_means{i});
        projected_data = (X_residuals*p);       // project residuals on loadings
        ASCA.factors.scores{i}    = t;
        ASCA.factors.loadings{i}  = p;
        ASCA.factors.singular{i}  = s;
        ASCA.factors.projected{i} = projected_data;
        s_explained=asca_pc_explains(s);
        ASCA.factors.explained{i} =s_explained;
        tri=find(s_explained>10* %eps);
        n=max(size(tri));      
        
        ASCA2.factors.scores(i).d=t(:,1:n);
        ASCA2.factors.scores(i).i=X0.i;
        ASCA2.factors.scores(i).v='score'+string([1:n]');
        ASCA2.factors.scores(i)=div(ASCA2.factors.scores(i));
        ASCA2.factors.loadings(i).d=p(:,1:n);
        ASCA2.factors.loadings(i).i=X0.v;
        ASCA2.factors.loadings(i).v='loading'+string([1:n]');
        ASCA2.factors.loadings(i)=div(ASCA2.factors.loadings(i));
        ASCA2.factors.singular(i).d=s(1:n,:);
        ASCA2.factors.singular(i).i='loading'+string([1:n]');
        ASCA2.factors.singular(i).v='eigenval'+string(i);
        ASCA2.factors.singular(i)=div(ASCA2.factors.singular(i));
        ASCA2.factors.residual_scores(i).d=projected_data(:,1:n);
        ASCA2.factors.residual_scores(i).i=X0.i;
        ASCA2.factors.residual_scores(i).v='loading'+string([1:n]');
        ASCA2.factors.residual_scores(i)=div(ASCA2.factors.residual_scores(i));
        ASCA2.factors.explained(i).d=s_explained(1:n,:);
        ASCA2.factors.explained(i).i='loading'+string([1:n]');
        ASCA2.factors.explained(i).v=' %explained by factor' + string(i);
        ASCA2.factors.explained(i)=div(ASCA2.factors.explained(i)); 
        ASCA2.factors.scores_and_resid(i).d=ASCA2.factors.scores(i).d + ASCA2.factors.residual_scores(i).d;
        ASCA2.factors.scores_and_resid(i).i=ASCA2.factors.scores(i).i;
        ASCA2.factors.scores_and_resid(i).v=ASCA2.factors.scores(i).v;
        ASCA2.factors.scores_and_resid(i)=div(ASCA2.factors.scores_and_resid(i));
        
    end

    // -------------------------------
    //Do PCA on interactions
    ASCA2.interactions.scores=list();
    ASCA2.interactions.loadings=list();
    ASCA2.interactions.singular=list();
    ASCA2.interactions.residual_scores=list();
    ASCA2.interactions.explained=list();
    ASCA2.interactions.scores_and_resid=list();

    for i = 1 : n_interactions
        [t,s,p] = asca_do_pca(X_interaction_means{i});
        projected_data = (X_residuals*p);       // project residuals on loadings
        ASCA.interactions.scores{i}    = t;
        ASCA.interactions.loadings{i}  = p;
        ASCA.interactions.singular{i}  = s;
        ASCA.interactions.projected{i} = projected_data;
        s_pc_explained=asca_pc_explains(s);
        ASCA.interactions.explained{i} =  s_pc_explained;
        tri=find(s_pc_explained>10* %eps);
        n=max(size(tri)); 
  
        ASCA2.interactions.scores(i).d=t(:,1:n);
        ASCA2.interactions.scores(i).i=X0.i;
        ASCA2.interactions.scores(i).v='score'+string([1:n]');
        ASCA2.interactions.scores(i)=div(ASCA2.interactions.scores(i));
        ASCA2.interactions.loadings(i).d=p(:,1:n);
        ASCA2.interactions.loadings(i).i=X0.v;
        ASCA2.interactions.loadings(i).v='loading'+string([1:n]');
        ASCA2.interactions.loadings(i)=div(ASCA2.interactions.loadings(i));
        ASCA2.interactions.singular(i).d=s(1:n,:);
        ASCA2.interactions.singular(i).i='loading'+string([1:n]');
        ASCA2.interactions.singular(i).v='eigenval'+string(i);
        ASCA2.interactions.singular(i)=div(ASCA2.interactions.singular(i));
        ASCA2.interactions.residual_scores(i).d=projected_data(:,1:n);
        ASCA2.interactions.residual_scores(i).i=X0.i;
        ASCA2.interactions.residual_scores(i).v='loading'+string([1:n]');
        ASCA2.interactions.residual_scores(i)=div(ASCA2.interactions.residual_scores(i));
        ASCA2.interactions.explained(i).d=s_pc_explained(1:n,:);
        ASCA2.interactions.explained(i).i='loading'+string([1:n]');
        ASCA2.interactions.explained(i).v=' %explained by interaction' + string(i);
        ASCA2.interactions.explained(i)=div(ASCA2.factors.explained(i)); 
        ASCA2.interactions.scores_and_resid(i).d=ASCA2.interactions.scores(i).d + ASCA2.interactions.residual_scores(i).d;
        ASCA2.interactions.scores_and_resid(i).i=ASCA2.interactions.scores(i).i;
        ASCA2.interactions.scores_and_resid(i).v=ASCA2.interactions.scores(i).v;
        ASCA2.interactions.scores_and_resid(i)=div(ASCA2.interactions.scores_and_resid(i));
        
    end
    
    // résidus
    ASCA2.residuals.d=ASCA.residuals;
    ASCA2.residuals.i=X0.i;
    ASCA2.residuals.v=X0.v; 
    ASCA2.residuals=div(ASCA2.residuals);
    
    // effets 
    ASCA2.effects.d=ASCA.effects';
    ASCA2.effects.i=['ssq_X';'ssq_factor'+string([1:n_factors]');'ssq_interaction'+string([1:n_interactions]'); 'ssq_residual'];
    ASCA2.effects.v='explained ssq, %';
    ASCA2.effects=div(ASCA2.effects);  
  
    

endfunction




 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  function [scores, singular_values, loadings] = asca_do_pca(D)
        // This function does the work: do PCA through singular value
        // decomposition on input matrix. Returns scores, loadings and
        // singular values.
        [u, sv, v] = svd(D);
        scores = u*sv;
        singular_values = diag(sv);
        loadings = v;

  endfunction
 
 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function perc_explained = asca_pc_explains(sv)
 
    // Function to calculate the percentage of variance explained by each PC
    
    // Input
    // vector with singular values 
    
    // Output
    // vector with percentages explained variance
    
        sv_squared     = sv.^2;     
        total_variance = sum(sv_squared);
        perc_explained = (sv_squared/total_variance)*100;

    endfunction

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     function perc_explained_effect = asca_effect_explains(ssq_X, ssq_mean, ssq_factors, ssq_interactions, ssq_residuals)
        
        // Function to calculate the percentage of variance explained by
        // each effect.
        
        // Input
        // sums of squares of the data matrix, the factors and the
        // interactions
        
        // Output
        // vector with percentages explained variance for each effect.
        
        ssq = [ssq_mean ssq_factors ssq_interactions ssq_residuals];
        // JCB 13mars20
        //perc_explained_effect = 100*(ssq./ssq_X);
        perc_explained_effect=100*ssq/sum(ssq);

    endfunction 
         

