function [model] = forwda(xi,yi,split,lv,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
   //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary) 
  //-----------------------------------------------------------------
  x=div(xi);

  
  // pour enlever les colonnes dont les valeurs sont toujours nulles
  [n,q]=size(x.d);
  for i=1:q;
    if max(size(split))==1 then
       thresh0=100/split;
    else
       thresh0=0.1*n;
    end
    sumzeros=sum(x.d(:,i)==0);
      if sumzeros>thresh0 then 
        if ~isdef('todelete') then 
            todelete=i;
        else     
            todelete=[todelete; i];
        end
      end
  end

  if isdef('todelete') then
      x=cdel(x,todelete);
  end
 
  if  size(x.d,1)<2 then error ('input data must contain at least 2 observations')
  end


   // calcul du modèle forwda 
  [model]= cvclass(x,yi,split,lv,'forwda',varargin(:)) 




endfunction
