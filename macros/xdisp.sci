function  zstring=xdisp(varargin) 
                                                                   
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013                                                                                    

  nargs=length(varargin); //length replace size                                                                       
  [nargin, nargout] = argn(0)                                                
  lines(10000000);

  str='';                                                                                          
  for i=1:nargs                                                                                    
      aux=varargin(i);                                                                                   
      if(type(aux)==10) then      //replaces the function ischar                                                                             
         str=(str+ aux);                                                                                   
      end                                                                                              
      if(type(aux)==1) then       //replaces the function isnumeric                                                                         
         str= (str+ ' ' + string(aux));                                                                
      end                                                                                              
  end                                                                                              
                                                                                  
  //disp(str);                                                                                       
                                                                                             
  zstring=str;                                                                                     

endfunction                                                                                      
                                            
