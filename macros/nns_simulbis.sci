function [y,std_ypred]=nns_simulbis(wh,wo,x,std_res,cov_w)
    
    // mise de la fonction c_nnsimulbis au format Div
    
    wh=div(wh);
    wo=div(wo);
    x=div(x);
    std_res=div(std_res);
    cov_w=div(cov_w);
    
    [y.d,std_ypred.d]=c_nnsimulbis(wh.d,wo.d,x.d,std_res.d,cov_w.d);

    y.i=x.i;
    y.v=wo.v;    
    y=div(y);
    
    std_ypred.i=y.i;
    std_ypred.v=wo.v;
    std_ypred=div(std_ypred);

    
endfunction
