function [classif,confus] = memberpred(calx,caly,testx,testy,metric,class)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  // calx et testx sont des scores!

  // Number of arguments in function call
  [%nargout,%nargin] = argn(0)

  [nx,px] = size(calx);
  [ny,cy] = size(caly);
  [nxv,pxv]=size(testx);
  [nyv,cyv]=size(testy);
  if nx~=ny   then error('the number of samples in calx and caly do not match'); end;
  if nxv~=nyv then error('the number of samples in testx and testy do not match'); end;
  if px~=pxv  then error('the number of variables in calx and testx do not match'); end;
  if cy~=cyv  then error('the number of classes in caly and testy do not match'); end; 
    
  n=nx;

  
  x = [calx;testx];
  y = [caly;testy *0];

  
  if typeof(x) =='hypermat' | (typeof(x) =='constant' & max(size(size(x)))==3 )    then  
      nclasses=size(x,3);
      k=zeros(nx+nxv,nclasses);
      j=0;
      for i=1:nclasses;
          ki=membership(x(:,:,i),y,metric,class); // la proba du groupe i est calculée avec le modèle i 

          k(:,i)=ki(:,i); 
      end
  else           
      [k] = membership(x,y,metric,class);      
  end
  
  a = k(n+1:$,:);
  cf = a' * testy;
  
  classif=a;
  confus=cf;
  
endfunction
