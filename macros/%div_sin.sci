function b=%div_sin(a)
    
    // sinus 
    b=a;
    b.d=sin(a.d);
    
endfunction
