function [col_groups] = groupcreate(x,classes)

  //function [col_groups] = group_create(xi,classes)
  //ancienne fonction: function[col_groups]=dat_creategroup2(xi,classes)
  //
  //Inputs:
  //-------
  // xi:                 matrix (n x p) or Saisir structure 
  // classes:            classes; conjunctif (vector/Saisir) or disjunctif (matrix/Saisir)
  //
  //Outputs:
  //--------
  //col_groups:          vector of structures, each of ones represents a group 

  
  xi=div(x);
  //[test,disjunctive_matrix2]=isdiv(classes);
    
  [testdisj]=isdisj(classes);
  [testconj]=isconj(classes);

  if  testdisj=='T'  then
    disj_mat=classes;  
  elseif testdisj=='F' & testconj=='T' then 
    disj_mat=conj2dis(classes);
  elseif testdisj=='F' & testconj=='F' then
    error('neither conjunctive nor disjunctive')
  end
  
  [n,p]=size(xi.d);

  [nd,pd]=size(disj_mat);    //disj_mat = 0 ou 1
  
  disj_mat2=([1:1:nd]'*ones(1,pd)).*disj_mat; //disj_mat2=1 à nd
 
  disj_mat3=list();
  for j=1:pd;
      tri=[];
      for i=1:nd;
          if disj_mat2(i,j) >10*%eps then tri=[tri;disj_mat2(i,j)];
          end
      end
      
      disj_mat3(j)=tri;
  end
 
  
  // sorties div:
  
  col_groups=list();
  
  for i=1:pd; 
      table_temp.d=xi.d(disj_mat3(i),:);
      table_temp.i=xi.i(disj_mat3(i),:);
      table_temp.v=xi.v;
      table_temp=div(table_temp);
      col_groups(i)=table_temp;
  end
 
 
 

endfunction

