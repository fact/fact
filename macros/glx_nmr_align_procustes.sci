
    function x_out0=glx_nmr_align_procustes(x_in0,target_ppm,range_nbr_points)
    
            
            // x_in0 est une matrice de spectres en colonne : 105001 x n 
            
            // x_in est le i° spectre de x_in0 
            // x_in est un spectre de 105001 variables; en colonne
            //             début = -0,5 ppm  fin = 10,0 ppm  pas = 0,0001 ppm
            // alignement:  par décalage du pic de TMSP mis sur 0
            //              ppm=0   -> 5001° variable
            
            // puis multiplication :
            //              target_ppm=4,8  -> 53001° variable 
            //              range_nbr_points: ex 1000 points
           
            // fonction issue de: script_align_jc_11jan24.sci
           
            
            // objectif de calage:   ppm=0   sur la variable 5001
            //                       ppm=4,8 sur la variable 53001 


            // utilise: simufilters_xl.sci
            
            // n° de variables pour ppm=0 et target_ppm (ex:4,8)
            ppm0=strtod(x_in0.i);
            c=find(ppm0==min(abs(ppm0)))                                // 5001   ppm=0
            c2=find((ppm0-target_ppm)==min(abs(ppm0-target_ppm)))       // 53001  ppm=4,8
                                                                        // différence = 48000
  
            // référence pour simufilters_xl
            xref=x_in0(:,1)';
            xref.d=ones(1,105001)*2*(ppm0(5001)-ppm0(5000));             // 2x l'écart entre 2 points
            xref=div(xref);
                                                                    
            x_out0=x_in0; 
            x_out0.d=0*x_out0.d;
            
            n=size(x_in0.d,2);
            for i=1:n;
                x_in=x_in0(:,i);
                                    
                plage1=[c-range_nbr_points; c+range_nbr_points];            // [4001,6001]
                plage2=[c2-range_nbr_points;c2+range_nbr_points];           // [52001,54001]
            
                a0=find(x_in.d(plage1(1):plage1(2),:)==max(x_in.d(plage1(1):plage1(2),:))); 
                b0=find(x_in.d(plage2(1):plage2(2),:)==max(x_in.d(plage2(1):plage2(2),:)));   
                a1=plage1(1)+a0-1;                                          // ex: 4784
                b1=plage2(1)+b0-1                                           // ex: 53053
                diff_x_in=b1-a1;
            
                // augmentation des données pour anticiper le décalage 
                x_before.i=string([-1:0.0001:-0.4999]');                    // 5000 points à gauche
                n_before=size(x_before.i,1);        
                x_before.d=ones(n_before,1)*mean(x_in.d(1:100));
                x_before.v=x_in.v
                x_before=div(x_before);
            
                x_after.i=string([10.0001:0.0001:10.5]');                   // 5000 points à droite
                n_after=size(x_after.i,1);
                x_after.d=ones(n_after,1)*mean(x_in.d($-100:$));
                x_after.v=x_in.v
                x_after=div(x_after);
            
                x2=[x_before;x_in;x_after];
            
                // ajustement de ppm=0 par décalage
                diff_var=a1-c;             // c=5001
                x3=x2;
                x3.i=string(strtod(x2.i)-diff_var*(ppm0(5001)-ppm0(5000)));     // ppm0 bien placé ; 5000 et 5001 pris au hasard dans le spectre
                                                                                // plage -1:10,5
            
                // ajustement de target_ppm (ex: 4,8) par homothétie 
                coeff_ppm=(c2-c)/(b1-a1); 
                x3.i=string(coeff_ppm * strtod(x3.i));

                x_out=simufilters_xl(x3',xref);                              // 1 x 105001            
 
                x_out0.d(:,i)=x_out.d';
            end
 
    
    endfunction
