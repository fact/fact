function [res,num] = covsel_fda(xin,yin,split,lv,varargin);
 
 
    // varargin=metric,scale,class,seuil
    
    //  [sec,secv,pred,num,crbvar] = covselfdacv(x,y,sp,nv,scal,fonc)
    //
    // selection + FDA on lv variables, followed by a cross validation 
    //
    //   x     : data input (n individuals x p variables)
    //   y     : membership degrees (n individuals x c classes)
    //   sp    : block splitting for cross validation
    //      if sp is a scalar, sp blocks are randomly generated
    //      si sp is a column vector (n lines), it gives the block number of each individual
    //   nv    : number of variables to keep
    //   scal, fonc : Cf cvfda.m

    //   pred : predictions (one column by number of variables)
    //   secv : cross validation error
    //   sec  : calibration error
    //   num  : index of the selected variables
    //   crbvar : evolution of cumulated variance for X (1st col) and Y (2nd col)
    
    //   JM Roger, Cemagref 2011
  

    // Initialisation
    xin2=div(xin);
    x=xin2.d;
    yin2=div(yin);
    
    if isconj(yin2)=='T' then
        y=conj2dis(yin2.d);
    elseif isdisj(yin2)=='T' then
        y=yin2.d;
    else
        error('covsel_fda: yin should be conjuctive or disjunctive')
    end

    [nx,px] = size(x);
    [ny,py] = size(y);
    if nx ~= ny
    error('covsel_fda: le nombre d''individus ne correspond pas')
    end

    // gestion des arguments en entree
    if argn(2)==4 then
        metric=0;
        scale = 'c';
        class = 0;
        seuil = 1/py;
    elseif argn(2)==5 then
        metric=varargin(1);
        scale='c';
        class = 0;
        seuil = 1/py;
    elseif argn(2)==6 then
        metric=varargin(1);
        scale=varargin(2);
        class=0;
        seuil=1/py;
    elseif argn(2)==7 then
        metric=varargin(1);
        scale=varargin(2);
        class=varargin(3);
        seuil=1/py;
    elseif argn(2)==8 then
        metric=varargin(1);
        scale=varargin(2);
        class=varargin(3);
        seuil=varargin(4); 
    end

    //nbre reel de variables latentes
    lv = min( lv, rank(x) );
    
    // centrage des données x (systématique et uniquement pour covsel)  			// 28aout24
    if scale=='c' then 
        ztemp=centering(x);  
    elseif scale=='cs' then 
        ztemp=standardize(centering(x));
    elseif scale=='cp' then 
        ztemp=pareto(centering(x));
    else 
        error("scale options are: c-centering, cs centering+standardize and cp centering+pareto")
    end
    z=ztemp.d;


    // rien à faire sur y -> t car ce sont des classes  
    t=y;
    
    // variance (inertie) totale
    vartot=[trace(z'*z) trace(t'*t)];

    for i=1:lv
        // recherche max de covariance
        //[cm,j] = max( diag( z'*t*t'*z ) );  // verifié le 11/09/18: même résultat que ligne suivante
        [cm,j] = max( sum( (z'*t).^2 ,2) );  // cm=valeur, j=indice
        
        v = z(:,j);
    
        //z = z - (v*v')./(v'*v)*z;
        z=z-v*inv(v'*v)*v'*z;           // JC  + lisible mais même resultat
        
        crbvar(i,1) = (vartot(1)-trace(z'*z))/vartot(1);
    
        //t = t - (v*v')./(v'*v)*t;   
        t=t-v*inv(v'*v)*v'*t;           // JC

        crbvar(i,2) = (vartot(2)-trace(t'*t))/vartot(2);
        num(i)=j;
        
    end;


    // classification par ordre croissant (utilise!)
    num_order=gsort(num,'g','i');
    
    // initialisation de la sortie
    res.conf_cal_nobs=list();
    for i=1:py-1;
        res.conf_cal_nobs(i).d=(-1)*ones(py,py,lv);
        res.conf_cal_nobs(i).i='pred_'+string([1:py]');
        res.conf_cal_nobs(i).v.v1='ref_'+string([1:py]');
        res.conf_cal_nobs(i).v.v2='NbVar' + string([1:lv]');
     //pause
        res.conf_cal_nobs(i)=div(res.conf_cal_nobs(i));
    end
    
    res.conf_cal=list();
    for i=1:py-1;
        res.conf_cal(i).d=(-1)*ones(py,py,lv);
        res.conf_cal(i).i='pred_'+string([1:py]');
        res.conf_cal(i).v.v1='ref_'+string([1:py]');
        res.conf_cal(i).v.v2='NbVar' + string([1:lv]');
        res.conf_cal(i)=div(res.conf_cal(i));
    end
    
    res.conf_cv=list();
    for i=1:py-1;
        res.conf_cv(i).d=(-1)*ones(py,py,lv);
        res.conf_cv(i).i='pred_'+string([1:py]');
        res.conf_cv(i).v.v1='ref_'+string([1:py]');
        res.conf_cv(i).v.v2='NbVar' + string([1:lv]');
        res.conf_cv(i)=div(res.conf_cv(i));
    end
    
    res.err.d=(-1)*ones(2,py-1,lv);  // -1 = valeur manquante
    res.err.i=['error of classification, p.cent';'error of cross-validation, p.cent'];
    res.err.v.v1='DV' +string([1:py-1]');
    res.err.v.v2='NbVar' + string([1:lv]');
    res.err=div(res.err);
    
    res.errbycl_cal.d=(-1)*ones(py,py-1,lv);
    res.errbycl_cal.i='CL'+string([1:py]');
    res.errbycl_cal.v.v1='DV'+string([1:py-1]');
    res.errbycl_cal.v.v2='NbVar' + string([1:lv]');
    res.errbycl_cal=div(res.errbycl_cal);
    
    res.errbycl_cv.d=(-1)*ones(py,py-1,lv);
    res.errbycl_cv.i='CL'+string([1:py]');
    res.errbycl_cv.v.v1='DV'+string([1:py-1]');
    res.errbycl_cv.v.v2='NbVar' + string([1:lv]');
    res.errbycl_cv=div(res.errbycl_cv);
    
    res.notclassed.d=(-1)*ones(py-1,lv);
    res.notclassed.i='DV'+string([1:py-1]');
    res.notclassed.v='NbVar' + string([1:lv]');
    res.notclassed=div(res.notclassed);
    
    res.notclassed_bycl.d=(-1)*ones(py-1,py,lv);
    res.notclassed_bycl.i='DV'+string([1:py-1]');
    res.notclassed_bycl.v.v1='CL'+string([1:py]');
    res.notclassed_bycl.v.v2='NbVar' + string([1:lv]');
    res.notclassed_bycl=div(res.notclassed_bycl);
    
    res.method='covsel_fda';
    
    res.xcal=xin2;
    
    res.ycal=yin2;
    
    res.scores.d=zeros(nx,py-1,lv);
    res.scores.i=xin2.i;
    res.scores.v.v1='DV' +string([1:py-1]');
    res.scores.v.v2='NbVar' + string([1:lv]');
    res.scores=div(res.scores);
    
    res.rloadings.d=zeros(px,py-1,lv);
    res.rloadings.i=xin2.v;
    res.rloadings.v.v1='DV' +string([1:py-1]');
    res.rloadings.v.v2='NbVar' + string([1:lv]');
    res.rloadings=div(res.rloadings);
    
    res.classif_metric=metric;
    
    res.scale=scale;
    
    res.classif_opt=class;
    
    res.threshold=seuil;
  
   
    
    for i=1:length(num);
              
        res_i = fda(xin2(:,num(1:i)),y,split,min(i,py-1),metric,scale,class,seuil);      // JCB 7sept18
        
        // adaptation de rloadings pour le nbre de variables initiales
        rloadings2=zeros(px,min(i,py-1));
        [num_i,indice_num_i]=gsort(num(1:i),'g','i');                                   // 7sept18    
        rloadings2(num_i,1:min(i,py-1))=res_i.rloadings.d(indice_num_i,:);              // 7sept18
        
        res_i.rloadings.d=rloadings2;
        res_i.rloadings.i=xin2.v;
        res_i.rloadings.v.v1='dim' + string([1:min(i,py-1)]');
        res_i.rloadings.v.v2='nb of lv' + string([1:i]');
        res_i.rloadings=div(res_i.rloadings);
        
        // report des donnees dans res
        n_loadings=size(res_i.rloadings.d,2);       // pour gerer les valeurs manquantes (i < py)
        
        //for j=1:min(i,py-1);                     // 17sept18
        for j=1:py-1;
            res.conf_cal_nobs(j).d(:,:,i)=res_i.conf_cal_nobs(min(i,j)).d;   // 17sept18
            res.conf_cal(j).d(:,:,i)=res_i.conf_cal(min(i,j)).d;
            res.conf_cv(j).d(:,:,i)=res_i.conf_cv(min(i,j)).d;

        end

        //res.err.d(:,1:n_loadings,i)=res_i.err.d';         // 17sept18   // supprime le 19aout21
        res.errbycl_cal.d(:,1:n_loadings,i)=res_i.errbycl_cal.d;
        res.errbycl_cv.d(:,1:n_loadings,i)=res_i.errbycl_cv.d;
        res.notclassed.d(1:n_loadings,i)=res_i.notclassed.d;
        res.notclassed_bycl.d(1:n_loadings,:,i)=res_i.notclassed_bycl.d;
        res.scores.d(:,1:n_loadings,i)=res_i.scores.d;
        res.rloadings.d(:,1:n_loadings,i)=res_i.rloadings.d;

    end;        // 19aout21

    // calcul des valeurs pour res.err.d
    for i=1:2;
        for j=1:py-1;
            for k=1:py;   // pas la peine d'aller jusqua lv
                if res.err.d(i,j,k)==-1 then
                    if i==1 then
                        mat_conf=res.conf_cal(j).d(:,:,k);
                    elseif i==2 then
                        mat_conf=res.conf_cv(j).d(:,:,k);
                    end
                    sum_diag=sum(diag(mat_conf));         // note: la somme des colonnes de res.conf_cv(j) fait 100
                        sum_mat=sum(mat_conf);
                        res.err.d(i,j,k)=100*(sum_mat-sum_diag)/sum_mat;
                end
            end
        end
    end

    //end;      //19aout21

endfunction
