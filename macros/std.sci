function [res] = std(X0,n1)
    
  //std      - Standard deviation of a matrix
  //function res=s_std(X,n1);
  //Input arguments
  //==============
  //X: n x p matrix
  //n1 (optional) : normalise by n (default : n-1)
  //
  //Output argument
  //===============
  //res=vector (1 x p) of standard deviations of the columns
  //crude simulation of function std from Matlab
  
  [nargout,nargin] = argn(0)
  
  X=div(X0);
  
  res = stdev(X.d,1);    // modifie le 21 mai 2014 car st_deviation obsolete

  if nargin>1 then
    [n,p] = size(X.d);
    res = (res*sqrt(n-1))/sqrt(n);
  end;

endfunction

