function [wh1,wo1]=nns_init(Inp0,Out0,Nh,meth)


// Neural Network for Static models -- Initialization
//
// Simplified usage:
//     [Wh,Wo]=NNSInit(Inp,Out)
//
// Advanced usage:
//     [Wh,Wo]=NNSInit(Inp,Out,Nh,meth)
//
// Var   Size         Significance
// ----  ----------   -----------------------------------------
// Ni   (1 x 1)       Number of network inputs (without bias)
// Nh   (1 x 1)       Number of neurons in the hidden layer (without bias)
// No   (1 x 1)       Number of network outputs
// Nd   (1 x 1)       Number of experimental data points (observations)
//
// Wh   (Ni+1 x Nh)   Weight matrix for the hidden layer (tanh transfer function)
// Wo   (Nh+1 x No)   Weight matrix for the output layer (identity transfer function)
// Inp  (Nd x Ni)     Input data matrix. One row per observation.
// Out  (Nd x No)     Output data matrix. One row per observation.
// Nh   (1 x 1)       Number of neurons in the hidden layer (default=5)
// meth (string)      Method:
//         'l' = linear model for the first neuron, small random for the others
//         'n' = Nguyen-Widrow (default)
//         'r' = random values, with small variance

// (c) 1999 Ioan Cristian TRELEA

  // Initialisation / check of the Div structures -------
  Inp1=div(Inp0);
  Inp=Inp1.d;
  Out1=div(Out0);
  Out=Out1.d;

  [Nd,Ni]=size(Inp);
  [N,No]=size(Out);
  if N~=Nd then
     error(msprintf('Array ''Inp'' has %d rows, while ''Out'' has %d rows',Nd,N));
  end

  if Ni==0 then
     error('There are no inputs in ''Inp'' '); 
  end
  if No==0 then 
     error('There are no outputs in ''Out'' '); 
  end

  // meth
  if argn(2)<4 then
     meth='n'; 
  end

  if meth ~='l' & meth ~= 'n' & meth ~= 'r' then 
   error('Available values for ''meth'' are ''l'', ''n'' or ''r'' ');
  end



  // ---------- Select normalization method ----------
  select meth
   case 'n'    // Nguyen Widrow: make min=-1 and max=1
      // Inputs
      
      
      mI=min(Inp,'r');  // Min
      MI=max(Inp,'r');  // Max
      if mI(mI==MI) ~= [] then
         error('Array ''Inp'' has a constant column'); 
      end
      aI=2 ./(MI-mI);
      bI=-(MI+mI)./(MI-mI);

      // Outputs
      mO=min(Out,'r');  // Min
      MO=max(Out,'r');  // Max
      if mO(mO==MO) ~= [] then 
         error('Array ''Out'' has a constant column'); 
      end
      aO=2 ./(MO-mO);
      bO=-(MO+mO)./(MO-mO);
   else       // Linear or random: make mean=0 and variance=1
      // Inputs
      mI=mean(Inp,'r');    // Mean
      sI=stdev(Inp,'r',mI);  // Standard deviation
      if sI(sI==0) ~= [] then 
          error('Array ''Inp'' has a constant column'); 
      end
      aI=1 ./sI;
      bI=-mI./sI;

      // Outputs
      mO=mean(Out,'r');    // Mean
      sO=stdev(Out,'r',mO);  // Standard deviation
      if sO(sO==0) ~= [] then
         error('Array ''Out'' has a constant column'); 
      end
      aO=1 ./sO;
      bO=-mO./sO;
      AO=diag(aO);
   end
   
   AI=[diag(aI) zeros(Ni,1); bI 1];  // Wh=AI*wh
   AO=diag(aO);   // wo=Wo*AO+[0; bo]
   u=ones(Nd,1);


  // ---------- Select initialization method ----------
  select meth
   case 'l'    // Linear
      wh=rand(Ni+1,Nh,'normal'); // In normalized units
      wo=rand(Nh+1,No,'normal'); // In normalized units
      p=((Inp-u*mI)./(u*sI)) \ ((Out-u*mO)./(u*sO));  // Linear regression
      i=1:min(Nh,No);   // An output per hidden neuron
      wh(1:Ni,i)=wh(1:Ni,i)+p(:,i);
      for j=i; 
         wo(j,j)=wo(j,j)+1; 
      end
   case 'n'    // Nguyen Widrow
      // Hidden layer
      wh=2*rand(Ni+1,Nh)-1; // In normalized units
      lh=0.7*Nh^(1/Ni); // Length of each column in wh
      wh(1:Ni,:)=lh*wh(1:Ni,:)./(ones(Ni,1)*sqrt(sum(wh(1:Ni,:).^2,1)));
      wh(Ni+1,:)=lh*wh(Ni+1,:);
      // Output layer
      wo=2*rand(Nh+1,No)-1; // In normalized units
      lo=0.7*No^(1/Nh); // Length of each column in wo
      wo(1:Nh,:)=lo*wo(1:Nh,:)./(ones(Nh,1)*sqrt(sum(wo(1:Nh,:).^2,1)));
      wo(Nh+1,:)=lo*wo(Nh+1,:);

   case 'r'    // Random
      // Normalize inputs
      wh=0.1*rand(Ni+1,Nh,'normal'); // In normalized units
      wo=0.1*rand(Nh+1,No,'normal'); // In normalized units
 
  end

  // ---------- Transform to physical units ----------
  Wh=AI*wh;
  Wo=wo;
  Wo(Nh+1,:)=Wo(Nh+1,:)-bO;
  Wo=Wo/AO;


  // Div outputs:
  
  wh1.d=Wh;
  wh1.i=[Inp1.v;'h_biais'];
  wh1.v='hid_neuron'+string([1:1:Nh]');
  wh1=div(wh1);
  
  wo1.d=Wo;
  wo1.i=[wh1.v;'hid_biais'];
  wo1.v=Out1.v;
  wo1=div(wo1);  

endfunction



