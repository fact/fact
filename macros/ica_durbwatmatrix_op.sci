function [DW] = ica_durbwatmatrix_op(A,row_col)
    

// [DW] = DurbWatMatrix_Op(dir,A);
// 
// 5.7.2011
// 
// Calculates the Durbin-Watson criterion for a matrix A.
//
// INPUT
// dir   'row' for calculation by rows and any other string or value for 
//       calculation by columns
// A     signal (e.g., a loading vector matrix, or a matrix of vectors of 
//       regression coefficients)
//
// OUTPUT
// DW row vector with the values for the Durbin-Watson criterion for each
// row or column
//           dw = 0 : there is a strong correlation between
//           successive points
//           dw = 2 : there is a weak correlation (random distribution)
//    n       between successive points.

  
  if row_col=='r' then  
    B=((A(:,2:$)-A(:,1:$-1)).^2);      // soustrait 2 colonnes consécutives
    C=(A.^2);
    DW=(sum(B,'c')./sum(C,'c'));

  else
    B=((A(2:$,:)-A(1:$-1,:)).^2);      // soustrait 2 lignes consécutives
    C=(A.^2);

    DW=sum(B,'r')./sum(C,'r');
    DW=DW';
  end

endfunction

