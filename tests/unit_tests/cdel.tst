// This file is released into the public domain
//=================================
// load saisir
if ~isdef('cdel')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

   x=rand(20,30);

  index=[4;2;6];
   
  
  [xdelr]=cdel(x,index);
  
  if size(xdelr.d,2) ~=27 then pause;
  end
  
  clear xdelr x index
//=================================
