// This file is released into the public domain
//=================================
// load fact
if ~isdef('covsel_mlr')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    load('x_corn.dat')
  
  
    // 1° cas: loo-------------------------------------------------------------
    res1=covsel_mlr(x1,y,80,20,1);   // sortie 
    sec_jc=res1.err.d(1,:,:);
    sec_jc=matrix(sec_jc,[4 20]);
    secv_jc=res1.err.d(2,:,:);
    secv_jc=matrix(secv_jc,[4 20]);
  
    ypred=regapply(res1,x1,y);
  
    load('x_corn_res_covselmlr_loo.dat')
  
    // comparaison des sec-secv
    [sec_jc' res_jm.sec'] 
 
    // comparaison des variables sélectionnées   
    var_jc=gsort(res1.var_selected.d(1,:),'g','i');
    var_jm=gsort(res_jm.num(1,:),'g','i');
    [var_jc' var_jm']           // les memes variables ont ete selectionnees
    
    [res1.var_selected.d' res_jm.num']  // mais par variable de y l'ordre n'est pas le meme 
     
    // comparaison des predictions
    figure;
    plot(y(:,1),ypred.ypred.d(:,3,1),'r.')
    plot(y(:,1),res_jm.pred(:,3,1),'b.')
    plot([9.4 11],[9.4 11],'k')
    h=gcf()
    h.background=-2                     // mieux avec Scilab
  
    //2° cas: double CV -------------------------------------------------------
    res2=covsel_mlr(x1,y,[2 5],20,1);   // sortie 
    sec2_jc=res2.err.d(1,:,:);
    sec2_jc=matrix(sec2_jc,[4 20]);
    secv2_jc=res2.err.d(2,:,:);
    secv2_jc=matrix(secv2_jc,[4 20]);
  
    ypred2=regapply(res2,x1,y);
  
    load('x_corn_res_covselmlr_dbl_2_5.dat')
  
    // comparaison des sec
    [sec2_jc' res_jm.sec'] 
  
    // comparaison des variables sélectionnées   
    var_jc2=gsort(res2.var_selected.d(1,:),'g','i');
    var_jm2=gsort(res_jm.num(1,:),'g','i');
    [var_jc' var_jm']           // les memes variables ont ete selectionnees
      
    [res2.var_selected.d' res_jm.num']  // mais par variable de y l'ordre n'est pas le meme 
  
    figure;
    plot(y(:,1),ypred2.ypred.d(:,3,1),'r.')
    plot(y(:,1),res_jm.pred(:,3,1),'b.')
    plot([9.4 11],[9.4 11],'k')
    h2=gcf()
    h2.background=-2
  
    // 3° cas: non régression -> essai avec 1 seule dimension de y
    res0_centre=covsel_mlr(x1,y(:,1),80,20,1);
    res0_noncentre=covsel_mlr(x1,y(:,1),80,20,0);
    
    somme1=res0_centre.var_selected.d(1,:)-res0_centre.var_selected.d(2,:);
    max(somme1)     // doit être égal à 0
    somme2=res0_noncentre.var_selected.d(1,:)-res0_noncentre.var_selected.d(2,:);
    max(somme2)     // doit être égal à 0
    somme3=max(abs(somme1-somme2));      // doit être égal à 0 => moi et JM: mêmes résultats
    
    // vérification 
    
    if ~isdef('ypred') | ~isdef('ypred2') | var_jc ~= var_jm then 
        disp('ligne 83')
        pause;
    end
    
    if max(abs(var_jc - var_jm)) >%eps then  // donc pas les mêmes variables sélectionnées
        disp('ligne 88')
        pause
    end
    
    if max(abs(var_jc2 - var_jm2)) >%eps then  // donc pas les mêmes variables sélectionnées
        disp('ligne 93')
        pause
    end

    if max(somme1) >%eps | max(somme2) >%eps then  // donc pas les mêmes variables sélectionnées
        disp('ligne 98')
        pause
    end
  
    if max(somme3) >%eps then  // donc pas les mêmes variables sélectionnées
        disp('ligne 103')
        pause
    end
  
  
    delete(h)
    delete(h2)                 // supprime les figures 
  
    clear b h p res1 res2 res_jm sec_jc sec2_jc secv2_jc 
    clear sec2cv_jc secv_jc t x1 x2 x3 y ypred ypred2
    clear var_jc var_jm h h2 var_jc2 var_jm2

    clear res0_centre res0_noncentre somme1 somme2 somme3 ans

//==============================================
