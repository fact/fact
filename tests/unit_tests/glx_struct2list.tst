// This file is released into the public domain
//=================================
// load fact
if ~isdef('glx_struct2list')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================


  // --------------------------------------------
  // on crée d'abord les données avec list2struct

  // chargement des données: 
  loadmatfile x_acom1.mat
  x2=list();
  x2(1)=x.col1;
  x2(2)=x.col2;
  x2(3)=x.col3;
  x2(4)=x.col4;
  x2(5)=x.col5;
   
  x2bis=list();
  x2bis(1)=div(x2(1)); 
  x2bis(2)=div(x2(2)); 
  x2bis(3)=div(x2(3)); 
  x2bis(4)=div(x2(4)); 
  x2bis(5)=div(x2(5)); 
     
  x3=glx_list2struct(x2);
  x3bis=glx_list2struct(x2bis);

  // --------------------------------
  // on valide maintenant struct2list
  x4=glx_struct2list(x3);
  x4bis=glx_struct2list(x3bis);

  if ~isdef('x4') | ~isdef('x4bis') then pause;
  end
    
  clear model x y x1 x2 x3 x2bis x3bis x4 x4bis groupes xfarines
//==============================================================
