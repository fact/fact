// This file is released into the public domain
//=================================
if ~isdef('patch_pls_c_yloadings')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  
   load('x_corn.dat')
   
   // commande matlab ayant donné les résultats (JMR):
   //[m,ssq,p,q,w,t,u,b] = pls(x1,y(:,1),20,1);
   
   // commandes Scilab:
   res0=pls(x1,y(:,1),5,10,0);
   

   // calcul des loadings de y :
   c0=patch_pls_c_yloadings(res0);
  
   // rapports entre les scores (pas la même normalisation)
   moy_rapports= mean(t(:,1:10) ./ res0.scores.d(:,1:10),'r');
  
   // correction de c0:
   c0_corr=c0 ./ moy_rapports';
   
   // correlation entre obtenu et attendu:
   r=correl(c0_corr,b(1:10)); // calcul du coefficient de corrélation 
   
   if r<0.9999 then 
       pause
   end
   

   //deletefile('killme.csv')
   clear x1 x2 x3 y res0 c0 c0_corr r moy_rapports b t p res
   clearglobal
//=================================================================
