// This file is released into the public domain
//=================================
// load fact
if ~isdef('nns_simulter')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
// Demo 03 IC Trelea AgroParisTech 
 
Inp=[linspace(0,3,11) linspace(6,10,21)]';
Tar=10*sin(Inp).*exp(-0.2*Inp)  +  0.5*rand(size(Inp,1),size(Inp,2));

[Nd,Ni]=size(Inp);
[Nd,No]=size(Tar);
Nh=10;
[Wh,Wo]=nns_init(Inp,Tar,Nh,'n');

Opt=[];
//Opt.RegClass=[1 3 2];  // Uncomment for custom classes
Opt.RegClass=3;
Opt.MaxTime=30000;
Opt.DisplayMode=3;
Opt.DisplayFreq=20;
Opt.MomentParam=0.8;
Opt.PreProc=2;
//Opt.StdResMin=0.5;     // Uncomment to set the expected output standard error
//Opt.StdResMax=1;       // Uncomment to set the expected output standard error

// Training
ypred=nns_simul(Wh,Wo,Inp);

std_res=stdev(ypred.d,'r');
nw=size(Wh.d,1)*size(Wh.d,2)+size(Wo.d,1)*size(Wo.d,2);
cov_w=rand(nw,nw);

[sse,sgra,hes]=nns_simulter(Wh,Wo,Inp,ypred);
  
  if ~isdef('sse') then pause;
  end
  
  clear ypred Opt Nh No Nd Ni Tar Wh Wo Inp sse sgra hes cov_w nw std_res 
//======================================================================
