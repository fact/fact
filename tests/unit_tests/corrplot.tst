// This file is released into the public domain
//=================================
// load fact

// <-- TEST WITH GRAPHIC -->

if ~isdef('corrplot')  then
  root_tlbx_path = SCI+'\contrib\fact\';                 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  xscores=[1 4 5 3 4 9 7 1;1.02 1.08 -0.9 0.06 3.08 -0.82 -0.86 0.06 ]';
  X=[6 7 4 5 3 9 8 0; 2 3 4 3 4 6 5 8]';

  h=corrplot(xscores,1,2,X);   
  
  if ~isdef('h') | isempty(h)  then pause;
  end
  
  close()
 
  clear X xscores h
  clearglobal
//=================================
