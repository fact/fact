// This file is released into the public domain
//=================================
if ~isdef('dcsv2div2')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x=rand(20,30);
  x0=div2dcsv(x,'killme.csv');

  x2=dcsv2div2('killme.csv');
  
  if ~isdef('x2') then pause;
  end

  deletefile('killme.csv');
  clear x0 x x2
//=================================
