// This file is released into the public domain
//=================================
if ~isdef('simufilters')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  
   load('x_simufilters.dat')
   x10disp=ch11.x10disp;
   x10ft=ch11.x10ft;
   x10ft.v=string(10000000 ./ strtod(x10ft.v));
   
   //figure;
   //curves(x10disp','b')
   //curves(x10ft','r')
   
   lam=strtod(x10disp.v);
   [nft,pft]=size(x10ft.d);
   
   bandepass.d=4*ones(pft,1);
   bandepass.v='bande_passante';
   bandepass.i=x10ft.v;  
   //pause
   bandepass=div(bandepass);
   bandepass=bandepass';
   width=4;
   
   [x10ftdisp,b]=simufilters(x10disp,bandepass,width);

//   mean_sig.d=mean(x10disp.d,'r')';
//   mean_sig.i=x10disp.v;
//   mean_sig=div(mean_sig);
//    
//   mean_x.d=mean(x10ftdisp.d,'r')';
//   mean_x.i=x10ft.v;
//   mean_x=div(mean_x);
   
   if ~isdef('x10ftdisp') then 
       pause
   end

   //deletefile('killme.csv')
   clear ch11 x10disp x10ft lam nft pft bandepass b mean_sig mean_x  
   clear x10ftdisp width
   clearglobal
//=================================================================
