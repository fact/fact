// This file is released into the public domain
//=================================
// load saisir
if ~isdef('num2str')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=[1:1:10]';

  [xs]=num2str(x,3);
  if ~isdef('xs') then pause;
  end
  
  clear xs x 
//=================================
