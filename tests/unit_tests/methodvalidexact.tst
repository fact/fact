// This file is released into the public domain
//=================================
// load fact
if ~isdef('methodvalidexact')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  x=csv2div('x_cafeine.csv',',','.');

  [x_exact_incert_fct,x_calc_incert,x_loq,x_calc]=methodvalidexact(x,0.85, 0.1, 3.2,1/1.2);
  
  if ~isdef('x_exact_incert_fct') then pause;
  end
  
  clear x x_exact_incert_fct x_calc_incert x_loq x_calc
//=====================================================


