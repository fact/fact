// This file is released into the public domain
//=================================
// load fact 

// <-- TEST WITH GRAPHIC -->

if ~isdef('scmap')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x.d=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8];
  x.i=['a';'a';'b';'b'];
  h=scmap(x,2,4,1,1);  
  
  if ~isdef('h') | isempty(h)  then pause;
  end
  
  close()
  clear model x h
  clearglobal
//=================================
