// This file is released into the public domain
//=================================
// load fact
if ~isdef('reorder')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  d1.d=[1 2; 3 4; 5 6; 7 8; 9 10];
  d1.v=['v1';'v2'];
  d1.i=['i3';'i4';'i2';'i1';'i6'];
  d2.d=[11 12; 13 14; 15 16; 17 18; 19 20];
  d2.v=['v1';'v2'];
  d2.i=['i7';'i2';'i3';'i4';'i8'];
 
  [d1r,d2r]=reorder(d1,d2);
  
  if ~isdef('d1r') then pause;
  end
  
  clear d1 d2 d1r d2r 
//=================================
