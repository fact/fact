// This file is released into the public domain
//=================================
// load saisir
if ~isdef('arand_index')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x_rand=csv2div('x_rand.csv',',','.');
  
  val1=arand_index(x_rand.d(:,1),x_rand.d(:,1));
  val2=arand_index(x_rand.d(:,1),x_rand.d(:,2));
  
  if ~isdef('val1') then 
      pause;
  else
      if val1 ~=1 | val2 ~=0 then 
          pause
      end
  end
  
  clear test_rand val1 val2 x_rand
//=================================
