// This file is released into the public domain
//=================================
// load saisir
if ~isdef('glx_matrix2cube')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x1=ones(5,7);
  
  x=[x1;2*x1;3*x1];
  
  xcube=glx_matrix2cube(x,3,'xtemp.mat');
  
  if ~isdef('xcube') then 
      pause;
  end
  
  clear x1 x xcube 
  deletefile('xtemp.mat')
//=================================
