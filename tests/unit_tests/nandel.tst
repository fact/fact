// This file is released into the public domain
//=================================
// load fact
if ~isdef('nandel')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  
  x=[1 2 3 4 %nan 6;11 12 13 14 15 16;21 22 23 24 25 26; %nan 32 33 34 35 36];
   
  
  [xdelnan]=nandel(x,'r');
  
  if ~isdef('xdelnan') then pause;
  end
  
  clear xdelnan x 
//=================================
