// This file is released into the public domain
//=================================
// load fact
if ~isdef('savgol')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  x1=[1:1:100];
  x2=rand(1,100);
  x=x1+x2;
  res_savgol=savgol(x,15,1);
  
  if ~isdef('res_savgol') then pause;
  end
  
  clear res_savgol x x1 x2
//=================================


