// This file is released into the public domain
//=================================

  if ~isdef('c_nnsimulter')  then
    root_tlbx_path = SCI+'\contrib\fact\';      
    exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================

 
  Nd=100; 
  Ni=5; 
  No=3; 
  Nh=10;
  
  Inp=rand(Nd,Ni); 
  OutExp=rand(Nd,No);
  Wh=rand(Ni+1,Nh);   
  Wo=rand(Nh+1,No);

  [SSE,GRA,HES]=c_nnsimulter(Wh,Wo,Inp,OutExp);      
  
  if ~isdef('SSE') then
      pause
  end
  
  // Nettoyage
  clear Nd Ni No Nh Inp OutExp Wh Wo SSE GRA HES
 
//==============================================
