// This file is released into the public domain
//=================================
// load fact
if ~isdef('conj2dis')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  xconj=[1;1;2;2;3;3;4;4;4;3];

  
  [xdisj]=conj2dis(xconj);
  
  if ~isdef('xdisj') then pause;
  end
  
  clear xdisj xconj
//=================================
