// This file is released into the public domain
//=================================
// load fact
if ~isdef('classplsda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  load('x_plsda.dat');
    
  [model]=classplsda(xc,classe,3);
  
  if ~isdef('model') then pause;
  end
  
  clear model xc classe ;

//=================================
