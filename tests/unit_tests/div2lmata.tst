// This file is released into the public domain
//=================================
// load fact
if ~isdef('div2lmata')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  x1.d=rand(10,30); 
  x1=div(x1); 
  div2lmata(x1,'tempfile');
  x2=lmata2div('tempfile');
  
  if ~isdef('x2') then pause;
  end
  
  clear x1 x2 
  deletefile("tempfile")
//=================================


