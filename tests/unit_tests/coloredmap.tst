// This file is released into the public domain
//=================================
// load saisir 

// <-- TEST WITH GRAPHIC -->

if ~isdef('coloredmap')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8];

  h=coloredmap(x,2,4,2,3);  
  
  if ~isdef('h') | isempty(h)  then pause;
  end
  
  clear x h
  close()
  clearglobal newfig
//=================================
