// This file is released into the public domain
//=================================
if ~isdef('div')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  // nombre
  x1=4;
  x1d=div(x1);
  
  
  // matrice
  x2=[1 3 4;7 8 9];
  x2d=div(x2);
  
  x3.d=x2;
  x3.i=['a';'b'];
  x3d=div(x3);
  
  x4.d=x2;
  x4.v=['e';'r';'t'];
  x4d=div(x4);
  
  x5.d=x2;
  x5.i=['a';'b'];
  x5.v=['e';'r';'t'];
  x5d=div(x5);
  
  info=['ligne1';'ligne2'];
  x6d=div(x5.d,x5.i,x5.v,info);
  
  
  // hypermatrice
  x7=rand(2,3,4);
  x7d=div(x7);
  
  x8.d=x7;
  x8.i=['obs1';'obs2'];
  x8d=div(x8);
  
  x9.d=x7;
  x9.v.v1=['var1';'var2';'var3'];
  x9d=div(x9);
  
  x10.d=x7;
  x10.v.v2=['p1';'p2';'p3';'p4'];
  x10d=div(x10);
  
  x11.d=x7;
  x11.i=['obs1';'obs2'];
  x11.v.v1=['var1';'var2';'var3'];
  x11d=div(x11);
  
  x12.d=x7;
  x12.i=['obs1';'obs2'];
  x12.v.v2=['p1';'p2';'p3';'p4'];
  x12d=div(x12);
  
  x13.d=x7;
  x13.v.v1=['var1';'var2';'var3'];
  x13.v.v2=['p1';'p2';'p3';'p4'];
  x13d=div(x13);
  
  x14.d=x7;
  x14.i=['obs1';'obs2'];
  x14.v.v1=['var1';'var2';'var3'];
  x14.v.v2=['p1';'p2';'p3';'p4'];
  x14d=div(x14);
  
  x15d=div(x7,x14.i, x14.v,info);
  
  
  if ~isdef('x1d') | ~isdef('x2d') | ~isdef('x3d') | ~isdef('x4d') | ~isdef('x5d') then 
      pause;
  end
  if ~isdef('x6d') | ~isdef('x7d') | ~isdef('x8d') | ~isdef('x9d') | ~isdef('x10d') then 
      pause;
  end
  if ~isdef('x11d') | ~isdef('x12d') | ~isdef('x13d') | ~isdef('x14d') | ~isdef('x15d') then 
      pause;
  end
  

  clear x1 x1d x2 x2d x3 x3d x4 x4d x5 x5d x6 x6d x7 x7d x8 x8d x9 x9d x10 x10d
  clear x11 x11d x12 x12d x13 x13d x14 x14d x15 x15d info
  
//=============================================================================
