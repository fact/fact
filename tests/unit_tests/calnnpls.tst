// This file is released into the public domain
//=================================
// load fact
if ~isdef('calnnpls')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  load('x_pca_pls.dat')
  x=x_pca_pls.x;
  y=x_pca_pls.y;
  
  opt.RegClass=0;
  opt.PreProc=0;

  rloads=calnnpls(x,y,15,7,opt);
    // 15 = VL
    // 7 = wh = nbre de neurones caches
  
  
  rloads2=calnnpls(x,y,[3,5,7],6, opt);
  
  if ~isdef('rloads') | ~isdef('rloads2') then pause;
  end
  
  clear rloads rloads2 x y x_pca_pls
//======================================================================
