// This file is released into the public domain
//=================================
// load fact
if ~isdef('icajade')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x=rand(20,100);
  [b]=icajade(x,5);
  
  if ~isdef('b') then pause;
  end

  clear x b 
//=================================
