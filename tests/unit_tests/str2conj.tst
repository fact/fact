// This file is released into the public domain
//=================================
// load saisir
if ~isdef('str2conj')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x=['ABCD'; 'BBCE'; 'EAAR';'TAAE';'YBCQ';'NAAR'];
 
  [groups]=str2conj(x,2,3);
  
  if ~isdef('groups') then pause;
  end
  
  clear groups x
//=================================
