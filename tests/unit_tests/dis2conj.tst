// This file is released into the public domain
//=================================
// load fact
if ~isdef('dis2conj')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  xdis=[1 0 0;1 0 0; 0 1 0; 0 1 0;1 0 0 ;0 0 1;0 0 1;0 0 1];

  
  [xconj]=dis2conj(xdis);
  
  if ~isdef('xconj') then pause;
  end
  
  clear xdis xconj
//=================================
