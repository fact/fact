// This file is released into the public domain
//=================================

  if ~isdef('ica_dwresiduals')  then
    root_tlbx_path = SCI+'\contrib\fact\';      
    exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================

  // Creation des données:
  x=rand(20,50);
 
  // Application de la fonction:
  res=ica_dwresiduals(x,6);
  
  if ~isdef('res') then
      pause
  end
  
  // Nettoyage
  clear x res
 
//=================================
