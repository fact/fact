// This file is released into the public domain
//=================================
// load fact
if ~isdef('indexseek')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=[1 4 5 6 4 8 7 9];
  
  
  [res_index]=indexseek(x,6.2);
  
  if ~isdef('res_index') then pause;
  end
  
  clear res_index x
//=================================
