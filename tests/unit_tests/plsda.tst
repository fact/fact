// This file is released into the public domain
//=================================
// load fact
if ~isdef('plsda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  // chargement des données de JMR:
  loadmatfile x_plsda_res_test_matlab_JMR.mat
  
  // chargement des données JC: 
  load x_140farines.dat
  
  //x=xfarines(:,1:10:$);
  x=xfarines;
    
  // groupes définis par des noms
  groupes2=[repmat('a',35,1); repmat('b',27,1);repmat('c',38,1);repmat('d',40,1)];  
 
 
  // deux codages des groupes  
  [model]=plsda(x,groupes,2,4);
  
  [model2]=plsda(x,groupes2,2,4);

  // matrices de confusion de JMR, en étalonnage:
  m1=matrix(conf_cal(1,1,:,:),4,4);
  m2=matrix(conf_cal(2,2,:,:),4,4);
  m3=matrix(conf_cal(3,3,:,:),4,4);
  
  // matrices de confusion de JCB, en étalonnage: 
  s1=model.conf_cal_nobs(1).d;
  s2=model.conf_cal_nobs(2).d;
  s3=model.conf_cal_nobs(3).d;
  
  // différences de mal classés:
  difference=zeros(4,1);
  difference(1)=sum(diag(s1))-sum(diag(m1));
  difference(2)=sum(diag(s2))-sum(diag(m2));
  difference(3)=sum(diag(s3))-sum(diag(m3));
  
  // garantie que la fonction tourne
  if ~isdef('model') then 
      pause;
  end
 
  // garantie que Fact fait aussi bien ou mieux que Matlab:
  if min(difference)<0 then
      pause
  end
  
  
  clear model model2 x groupes groupes2 xfarines difference
  clear diference s1 s2 s3 s4 m1 m2 m3 m4 conf_cv conf_cal err_cal err_cv
  clear vp_cal err_cal_2 err_cv_2 conf_cv_2 vp_cal_2 conf_cal_2
//=======================================================================
