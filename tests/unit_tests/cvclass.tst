// This file is released into the public domain
//=================================
// load fact
if ~isdef('cvclass')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  load('x_140farines.dat');
    
  x=xfarines(:,1:50:$);
  
  groupes2=[repmat('gr1',35,1);repmat('gr2',27,1); repmat('gr3',38,1); repmat('gr4',40,1)];
  
  groupes3=[ones(35,1);2*ones(27,1);3*ones(38,1);4*ones(40,1)]; 
    
  [model]=cvclass(x,groupes,7,3,'fda');
  [model2]=cvclass(x,groupes2,7,3,'fda');
  [model3]=cvclass(x,groupes3,7,3,'fda');
  
  diff1=model2.conf_cal_nobs(3).d -model.conf_cal_nobs(3).d;
  diff2=model3.conf_cal_nobs(3).d -model.conf_cal_nobs(3).d;
  
  // test de l'existance des sorties 
  if ~isdef('model')| ~isdef('model2') | ~isdef('model3') then pause;
  end
  
  // test de l'égalité des sorties 
  if max(abs(diff1))>0 then
      pause 
  end
  
  if max(abs(diff2))>0 then
      pause 
  end
  
  
  clear model model2 model3 diff1 diff2 x groupes groupes2 groupes3 xfarines 
//=================================
