// This file is released into the public domain
//=================================
// load saisir
if ~isdef('glx_tab2div')  then
  root_tlbx_path = SCI+'\contrib\fact\';    
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  res=glx_tab2div('x_inst1.tabular');
  
  if ~isdef('res') then pause;
  end
  
  clear res
//=================================
