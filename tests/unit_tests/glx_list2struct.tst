// This file is released into the public domain
//=================================
// load fact
if ~isdef('glx_list2struct')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================


  // list2struct

  // chargement des données: 
  loadmatfile x_acom1.mat
  x2=list();
  x2(1)=x.col1;
  x2(2)=x.col2;
  x2(3)=x.col3;
  x2(4)=x.col4;
  x2(5)=x.col5;
  
  x3=glx_list2struct(x2);


  if ~isdef('x3') then pause;
  end
    
  clear model x y x1 x2 x3 groupes xfarines
//=========================================
