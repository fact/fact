// This file is released into the public domain
//=================================
// load fact
if ~isdef('nns_buildbayes')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
// Demo 03 IC Trelea AgroParisTech 
 
Inp=[linspace(0,3,11) linspace(6,10,21)]';
Tar=10*sin(Inp).*exp(-0.2*Inp)  +  0.5*rand(size(Inp,1),size(Inp,2));

[Nd,Ni]=size(Inp);
[Nd,No]=size(Tar);
Nh=10;
[Wh,Wo]=nns_init(Inp,Tar,Nh,'n');

Opt=[];
//Opt.RegClass=[1 3 2];  // Uncomment for custom classes
Opt.RegClass=3;
Opt.MaxTime=30000;
Opt.DisplayMode=3;
Opt.DisplayFreq=20;
Opt.MomentParam=0.8;
Opt.PreProc=2;
//Opt.StdResMin=0.5;     // Uncomment to set the expected output standard error
//Opt.StdResMax=1;       // Uncomment to set the expected output standard error

// Training
ypred=nns_simul(Wh,Wo,Inp);



[res]=nns_buildbayes(Wh,Wo,Inp,ypred,Opt);

  
  if ~isdef('res') then pause;
  end
  
  clear ypred ypred2 Opt Nh No Nd Ni Tar Wh Wo Inp res
//===========================================================================
