// This file is released into the public domain
//=================================
// load fact
if ~isdef('cvgroups')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
    
    // Données: 20 échantillons avec des répétitions -> 40 mesures
    x_sameobs=string([1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 15 16 16 17 17 18 18 19 19 20 20]'); 
    prefixed_blocks=[1 6 4 6 1 5 5 2 6 4 4  2 3 6 5 6 3 3 6 4 3 3 6 5 2 2 4 4  2 5 5 1 1 4 4  1 1 5 5 2 ; ...
      6 4 5 1 1 4 5 1 4 5  1 1 4 5 1 6 2 4 5 2 2 4 5 2 2 4 5 2 4 6 6 3 6 6 3 6 5 3 3 3; ...
      1 2 3 6 2 3 1 6 3 1 2 3 1 2 6 1 2 3 1 3 6 5 4 5 4 5 4 6 4 5 4 6 4 5 4 5 4 5 6 5   ]';
    prefixed_blocks_string=[ 'a' 'a' 'b' 'b' 'c' 'c' 'd' 'd' 'e' 'e' 'f' 'f' 'g' 'g' 'h' 'h' 'i' 'i' 'j' ...
              'j' 'k' 'k' 'l' 'l' 'm' 'm' 'n' 'n' 'o' 'o' 'p' 'p' 'q' 'q' 'r' 'r' 's' 's' 't' 't' ;
              'a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' 'p' 'q' 'r' 's' 't' ...
               'a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' 'p' 'q' 'r' 's' 't' ]'; 
   
    prefixed_blocks_disj=zeros(40,6,3);  
    for i=1:3;
        prefixed_blocks_disj(:,:,i)=conj2dis(prefixed_blocks(:,i));
    end

    // paramètres de la version d’avant Nov.18
    lot11=cvgroups(40,'jck6');                                          // 40x6
    lot12=cvgroups(40,'blk6');              // blk = jck = blocs contigus   // 40x6
    lot13=cvgroups(40, 6);                  // random                       // 40x6
    lot14=cvgroups(40,[6 4]);               // random répété                // 40x6x10
    lot15=cvgroups(40,'vnb6');                                          // 40x6

    // paramètres de la version d’après Nov.18
    
    split0=struct();
    split0.method='random'; 
    split0.nblocks=6;
    split0.nbrepeat=10;
    split0.sameobs=[1:40]';                 // version entiers 
    
    split1=struct();
    split1.method='random'; 
    split1.nblocks=6;
    split1.nbrepeat=10;
    split1.sameobs=x_sameobs;

    split2=struct();
    split2.method='jack';
    split2.nblocks=6;
    split2.nbrepeat=1; 		                // inutile de répéter avec ‘jack’ ou ‘venitian’
    split2.sameobs=string([1:40]');         // version chaines de caractères

    split2b=struct();
    split2b.method='jack';
    split2b.nblocks=6;
    split2b.nbrepeat=1; 		            // inutile de répéter avec ‘jack’ ou ‘venitian’
    split2b.sameobs=x_sameobs;

    split3=struct();
    split3.method='venitian';
    split3.nblocks=6;
    split3.nbrepeat=1;                      // inutile de répéter avec ‘jack’ ou ‘venitian’
    split3.sameobs=string([1:40]');

    split4=struct();
    split4.method='prefixed';
    split4.nblocks=prefixed_blocks;

    lot20=cvgroups(40,split0);              // random répété sans répétitions   // 40x6x10
    lot21=cvgroups(40,split1);              // random répété avec répétitions   // 40x6x10
    lot22=cvgroups(40,split2);              // jack sans répétitions            // 40x6
    lot22b=cvgroups(40,split2b);            // jack avec répétitions            // 40x6
    lot23=cvgroups(40,split3);              // venitian sans repets             // 40x6
    lot24=cvgroups(40,split4);              // prefixed                         // 40x6x3

    // re-utilisation des blocs --------------------  
    lot25=cvgroups(40,lot20);                                                   // 40x6x10
    lot26=cvgroups(40,lot21);                                                   // 40x6x10
    lot27=cvgroups(40,lot13); 
    lot28=cvgroups(40,lot14);  
    lot29=cvgroups(40,prefixed_blocks);
    lot30=cvgroups(40,prefixed_blocks_disj);
  
    // ---- codage disjonctif avec strings et répétitions -------------------------------
    split31=prefixed_blocks_string;

    lot31=cvgroups(40,split31);
    
    // -----random-pcent ----------------------------------------------------------------
    split32.method="random_pcent";
    split32.nblocks=30;             // 30%
    split32.nbrepeat=4
    split32.sameobs=x_sameobs;
    
    split33.method="random_pcent";
    split33.nblocks=70;             // 70%
    split33.nbrepeat=4
 
    
    lot32=cvgroups(40,split32);
    lot33=cvgroups(40,split33);
    
    
    // comparaisons
    if lot29 ~= lot30 then
        pause
    end
    if size(lot24)~=[40,6,3] then
        pause
    end
    if size(lot14)~=[40,6,10] | size(lot20)~=[40,6,10] | size(lot21)~=[40,6,10] | size(lot25)~=[40,6,10] | size(lot26)~=[40,6,10] then
        pause
    end
    if size(lot11)~=[40,6] | size(lot12)~=[40,6] | size(lot13)~=[40,6] | size(lot15)~=[40,6] | size(lot22)~=[40,6] | size(lot22b)~=[40,6] | size(lot23) ~=[40,6] then
        pause
    end
 
    if size(lot32)~=[40,2,4] | size(lot33)~=[40,2,4] then
        pause
    end
    
    if lot25(:,:,1)~=lot20(:,:,1) then 
        pause
    end
    if lot26(:,:,1)~=lot21(:,:,1) then 
        pause
    end
    if lot27~=lot13 then 
        pause
    end
    if lot28~=lot14 then 
        pause
    end
    if lot12~=lot22 then         // jack
        pause
    end
    if lot15~=lot23 then         // venitian
        pause
    end

    if  sum(lot31) ~= 80 then    // prefixed avec codage conjonctif avec chaines de caractères
       pause 
    end
    

  clear x_sameobs prefixed_blocks lot11 lot12 lot13 lot14 lot15 split1 split2 split3 split4 lot21 lot22 lot23 lot24 lot29 lot30 lot31
  clear lot20 lot22b lot25 lot26 lot27 lot28 split0 split2b split5 split6 split31 
  clear prefixed_blocks_disj prefixed_blocks_string
  
//=================================================================================================================
