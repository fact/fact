// This file is released into the public domain
//=================================
// load fact
if ~isdef('kcmeans')  then
  root_tlbx_path = SCI+'\contrib\fact\';    
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

 // x=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8];

  x=zeros(20,100);
  x(1,:)=sin([1:1:100]); 
  x(2,:)=sin(2*[1:1:100]);
  x(3,:)=sin(3*[1:1:100]);
  x(4,:)=sin(4*[1:1:100]);
  x(5,:)=sin(5*[1:1:100]);
  x(6,:)=sin(6*[1:1:100]);
  x(7,:)=sin(7*[1:1:100]);
  x(8,:)=sin(8*[1:1:100]);
  x(9,:)=sin(9*[1:1:100]);
  x(10,:)=cos(10*[1:1:100]);
  x(11,:)=cos([1:1:100]); 
  x(12,:)=cos(2*[1:1:100]);
  x(13,:)=cos(3*[1:1:100]);
  x(14,:)=cos(4*[1:1:100]);
  x(15,:)=cos(5*[1:1:100]);
  x(16,:)=cos(6*[1:1:100]);
  x(17,:)=cos(7*[1:1:100]);
  x(18,:)=cos(8*[1:1:100]);
  x(19,:)=cos(9*[1:1:100]);
  x(20,:)=cos(10*[1:1:100]);


  [res_kcmeans]=kcmeans(x,3);
  
  if ~isdef('res_kcmeans') then pause;
  end

  clear x res_kcmeans
//=================================
