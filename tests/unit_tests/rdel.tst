// This file is released into the public domain
//=================================
// load fact
if ~isdef('rdel')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=rand(20,30);

  index=[4;2;6];
   
  
  [xdelr]=rdel(x,index);
  
  if size(xdelr.d,1) ~=17 then pause;
  end
  
  clear xdelr x index
//=================================
