// This file is released into the public domain
//=================================
// load fact
if ~isdef('classplsfda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  load('x_plsda.dat');
    
  [model]=classplsfda(xc,classe,[5,2]);


  // verification de la reconstruction de la matrice xc:--------------
  // modifier + executer à la main
  flag=0; 
  if flag==1 then
        xc_rebuilt=model.scores*model.rloadings';
        curves(xc','b');
        curves(xc_rebuilt','r');
        pause
        h=gcf();
        close(h);
        clear xc_rebuilt
  end
  // -----------------------------------------------------------------
  
  if ~isdef('model') then pause;
  end
  
  clear model xc classe flag ;

//=================================
