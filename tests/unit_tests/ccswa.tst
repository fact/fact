 // This file is released into the public domain
 //=================================
 if ~isdef('ccswa')  then
   root_tlbx_path = SCI+'\contrib\fact\';  
   exec(root_tlbx_path + 'loader.sce',-1); 
 end
 //=================================

  col=list();
  
  load('x_pca_pls.dat')
   
  col(1)=x_pca_pls.x(:,1:50);
  col(2)=x_pca_pls.x(:,201:250);
  col(3)=x_pca_pls.x(:,300:340);
  col(4)=x_pca_pls.x(:,400:440);
  col(5)=x_pca_pls.x(:,600:680);
  
  [model]=ccswa(col,5);
  
  if ~isdef('model') then pause;
  end
  
  clear model col x_pca_pls
//=================================
