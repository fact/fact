// This file is released into the public domain
//=================================
// load fact

// <-- TEST WITH GRAPHIC -->

if ~isdef('barycentermap')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================


  //holdon

  x=[1 2 3 4 5 6 7 8 9 10; 5 6 4 8 7 6 4 3 2 5; 8 9 8 7 0 6 5 7 4 4; 3 2 5 4 1 8 4 6 9 7];
  class=[1;1;2;2];
    
  h=figure();
  barycentermap(x,1,2,class) ;  
  //errclear();
  h.tag="OK";
   
  //h=get_figure_handle(0);
  h2=findobj("tag", "OK");
  
  if ~isdef('h2') | isempty('h2')  then pause;
  end
  clear x class h h2 ans
  close()
  clearglobal newfig
//=================================
