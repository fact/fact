// This file is released into the public domain
//=================================
// load fact
if ~isdef('ca')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x_contingency=[3 15 10 7; 10 4 16 9; 2 5 13 5; 8 0 2 4; 3  5 14 5]; 
    
  [model]=ca(x_contingency);
  
  if ~isdef('model') then pause;
  end
  
  clear model x_contingency
//=================================
