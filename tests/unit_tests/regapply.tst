// This file is released into the public domain
//=================================
// load fact
if ~isdef('regapply')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  
  load('x_pca_pls.dat')
  x=x_pca_pls.x;
  y=x_pca_pls.y;
  
  [model_pls]=pls(x,y,5,10);
  [ypred]=regapply(model_pls,x,y);
  
  if ~isdef('ypred') then 
      pause;
  end

  close()  
  clear x y x_pca_pls model_pls ypred 
  clearglobal
//=================================
