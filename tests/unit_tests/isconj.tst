
// This file is released into the public domain
//=================================
// load fact
if ~isdef('isconj')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  x=[1;2;4;2;1;3;3;4;3;2;4];


  [testconj]=isconj(x);
  
  if ~isdef('testconj') then pause;
  end
  
  clear testconj x
//=================================
