// This file is released into the public domain
//=============================================

if ~isdef('pcana')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=============================================

  load('x_pca_pls.dat')  // Cargill dataset

  // PCA 
  res=pcana(x_pca_pls.x,0);
  r=correl(res.eigenvec.d(:,20),x_pca_pls.p20_pca);
  r2=r^2;
  if ~isdef('r2') | r2 < 0.99 then pause;
  end
  
  // PCA centred
  res=pcana(x_pca_pls.x,1);
  r=correl(res.eigenvec.d(:,20),x_pca_pls.p20_pca_mc);
  r2=r^2;
  if ~isdef('r2') | r2 < 0.99 then pause;
  end  
  
  // PCA scentred-standardized
  res=pcana(x_pca_pls.x,1,1);
  r=correl(res.eigenvec.d(:,20),x_pca_pls.p20_pca_autosc);
  r2=r^2;
  if ~isdef('r2') | r2 < 0.99 then pause;
  end   
  
  
  clear res r r2 x_pca_pls
  
//========================================================
