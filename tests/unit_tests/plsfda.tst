// This file is released into the public domain
//=================================
// load fact
if ~isdef('plsfda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  // chargement des données de JMR:
  // loadmatfile x_plsda_res_test_matlab_JMR.mat
  
  // chargement des données JC: 
  load x_140farines.dat
  
  //x=xfarines(:,1:10:$);
  x=xfarines;
    
  // groupes définis par des noms
  groupes2=[repmat('a',35,1); repmat('b',27,1);repmat('c',38,1);repmat('d',40,1)];  
 
 
  // deux codages des groupes  
  //[model]=plsda(x,groupes,2,4);
  
  
  model =plsda(x,groupes2,4,[2]);
  model2=plsfda(x,groupes2,4,[5,2]);


  
  // garantie que la fonction tourne
  if ~isdef('model') | ~isdef('model2') then 
      pause;
  end
  
  
  clear model model2 x groupes groupes2 xfarines difference
  clear diference s1 s2 s3 s4 m1 m2 m3 m4 conf_cv conf_cal err_cal err_cv
  clear vp_cal err_cal_2 err_cv_2 conf_cv_2 vp_cal_2 conf_cal_2
//=======================================================================
