// This file is released into the public domain
//=================================
// load fact
if ~isdef('glx_div2struct')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  // chargement des données: 
  load x_140farines.dat
  
  x=xfarines(:,1:10:$);
  x=div(x);
  
  x2=glx_div2struct(x);


  if ~isdef('x2') then pause;
  end
    
  clear model x y x1 x2 groupes xfarines
//======================================
