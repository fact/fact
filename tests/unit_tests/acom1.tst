// This file is released into the public domain
//=================================
if ~isdef('acom1')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  
  // charger les données et mise au format liste
  loadmatfile x_acom1.mat
  x2=list();
  x2(1)=x.col1;
  x2(2)=x.col2;
  x2(3)=x.col3;
  x2(4)=x.col4;
  x2(5)=x.col5;

  // charger les résultats obtenus avec la fonction Matlab de D.Bertrand 
  loadmatfile x_acom1_res.mat
  
  // calculer avec la fonction Scilab:
  res_sci=acom1(x2,8);
  res_sci_2=acom1(x2,8,0);
  
  difference=list();
  difference(1)=abs(res_sci.global_scores.d(:,8)) - abs(res.global.score.d(:,8));
  difference(2)=abs(res_sci.global_loadings.d(:,8)) - abs(res.global.loadings.d(:,8));
  difference(3)=abs(res_sci.individual_scores(3).d(:,8)) -  abs(res.individual.score(3).d(:,8));
  difference(4)=abs(res_sci.individual_loadings(3).d(:,8)) - abs(res.individual.loadings(3).d(:,8));
  difference(5)=abs(res_sci.individual_mean(3).d -  res.individual.mean(3).d');
  difference(6)=abs(res_sci.individual_tablenorm.d(3) -  res.individual.TableNorm(3).d);
  difference(7)=abs(res_sci.individual_tableweight(3).d -  res.individual.TableWeight(3).d);
  difference(8)=abs(res_sci.trajectory.d(:,8) -  res.trajectory.d(:,8));
  difference(9)=abs(res_sci.tables_scores.d(:,8) -  res.table.score.d(:,8));
  difference(10)=abs(res_sci.tables_corr.d(:,8) -  res.table.cor.d(:,8));
  difference(11)=abs(res_sci.explained_sum_squares.d(:,8) -  res.ExplainedSumSquare.d(:,8));
  
 
  for i=1:11;
     press=difference(i)'*difference(i);
     if press > 0.001 then 
         disp(i)
         pause
     end 
  end

  if ~isdef('res_sci_2') then
      pause
  end

  //deletefile('killme.csv')
  clear x x2 res res_sci res_sci_2 difference press i 
//===================================================
