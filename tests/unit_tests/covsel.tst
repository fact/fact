// This file is released into the public domain
//=================================
// load fact
if ~isdef('covsel')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    // valeurs numériques
    xc=csv2div('xc.csv',',','.');
    yc=csv2div('yc.csv',',','.');
    
    res11_ref=[697;313;604;404;6];      // valeurs verifies avec JMR 12mai20
    res12_ref=[604;405;695;258;19];
    res13_ref=[604;153;11;700;554];
    
    // classes 
    res11=covsel(xc,yc,5);
    res12=covsel(centering(xc),centering(yc),5);
    res13=covsel(standardize(centering(xc)),standardize(centering(yc)),5);
    
    diff11=abs(res11_ref-res11);
    diff12=abs(res12_ref-res12);
    diff13=abs(res13_ref-res13);
 
    sum1=sum(diff11)+sum(diff12)+sum(diff13);
 
    // classes 
    xclasses=csv2div('xclasses.csv',',','.');
    yclasses=csv2div('yclasses.csv',',','.');
    yclasses.d=str2conj(yclasses.d);
    yclasses=conj2dis(yclasses.d);
    
    res22_ref=[130;173;118;89;201];     // valeurs verifies avec JMR 12mai20
    
    res22=covsel(centering(xclasses),centering(yclasses),5);
    
    diff22=abs(res22_ref-res22);
 
    sum2=sum(diff22);
    
    sum_tot=sum1+sum2;
  
    if ~isdef('sum_tot') | sum_tot~=0 then 
        pause;
    end
  
  clear xc yc xclasses yclasses res11 res12 res13 res22 res11_ref res12_ref res13_ref res22_ref
  clear diff11 diff12 diff13 diff22 sum1 sum2 sum_tot
//=============================================================================================
