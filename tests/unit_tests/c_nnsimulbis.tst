// This file is released into the public domain
//=================================

  if ~isdef('c_nnsimulbis')  then
    root_tlbx_path = SCI+'\contrib\fact\';      
    exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================

 
  Nd=100; 
  Ni=5; 
  No=3; 
  Nh=10;
  Nw=(Ni+1)*Nh+(Nh+1)*No;
  
  Inp=rand(Nd,Ni); 
  Wh=rand(Ni+1,Nh);   
  Wo=rand(Nh+1,No);
  StdRes=rand(1,No);
  CovW=rand(Nw,Nw); 
  CovW=CovW+CovW'; 

  [OUT,STDOUT]=c_nnsimulbis(Wh,Wo,Inp,StdRes,CovW); 
  
  if ~isdef('OUT') then
      pause
  end
  
  // Nettoyage
  clear Nd Ni No Nh Nw Inp Wh Wo StdRes CovW OUT STDOUT;
 
//======================================================
