// This file is released into the public domain
//=================================
// load fact
if ~isdef('covsel_fda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================


    // charger les donnees
    load('x_mayo.dat')
    loadmatfile x_res_covsel_fda_jmr.mat

    // 6 classes de mayonnaises

    // application de covsel_fda
    [res,num]=covsel_fda(xmayo,xmayo_classes,4,20,0,'c');
    
    // application de covsel_fda
    res2=daapply(res,xmayo,xmayo_classes);
   
    // extraction des erreurs
    err1=matrix(res.err.d(1,:,:),[5 20])';
    err2=res2.err_test.d';
    
    // comparaison visuelle des erreurs
    [err1(1:5,:);zeros(1,5);err2(1:5,:);zeros(1,5);sec(1:5,:)]
    
    // autre essai avec d'autres options 
    [res_bis,num_bis]=covsel_fda(xmayo,xmayo_classes,4,20,1,'cs');
    
    
    
    // validation des resultats
    if ~isdef('res')| ~isdef('res_bis') then
        pause
    end
    
    if max(abs(err1(5,:)-err2(5,:)))>5  then   // toléré 5% d’erreur 19aout21
       pause
    end
    
    if max(abs(err1(5,:)-sec(5,:)))> 5  then   
       pause
    end
    
    
    clear cf crbvar ep num pr pred res sec secv sel vp x xmayo xmayo_classes y err1 err2 res2
    clear res_bis num_bis  

//===========================================================================================
