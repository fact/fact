// This file is released into the public domain
//=================================
// load fact
if ~isdef('groupcreate')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 

  x=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8;2 3 4 2 6 5 7 8; 6 5 4 7 8 6 9 7; 0 5 3 5 4 6 7 3; 5 4 7 3 2 4 5 4];

  classes=[1;1;2;2;3;3;4;4];

  [col_groups]=groupcreate(x,classes);
  
  if ~isdef('col_groups') then pause;
  end
  
  clear col_groups x classes
//=================================
