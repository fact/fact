// This file is released into the public domain
//=================================
if ~isdef('msc')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  load('x_corn.dat');

  [xc]=msc(x1);
  [xc]=msc(x1,mean(x1,'r'));
  
  if ~isdef('xc') then pause;
  end
  
  clear xc x b t p x1 x2 x3 y
//=================================
