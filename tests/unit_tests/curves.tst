// This file is released into the public domain
//=================================
// load fact

// <-- TEST WITH GRAPHIC -->

if ~isdef('curves')  then
  root_tlbx_path = SCI+'\contrib\fact\';                 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x.d=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 6 7 4 5 3 9 8 0; 2 3 4 3 4 6 5 8];
  x.v=string([1:1:8]');
  x=div(x);

  h=curves(x');  
  
  if ~isdef('h') | isempty(h)  then pause;
  end
  
  close()
  
  clear x h
  clearglobal
//=================================
