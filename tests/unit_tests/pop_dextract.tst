// This file is released into the public domain
//=================================
// load fact
if ~isdef('pop_dextract')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x1=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8;2 3 4 2 6 5 7 8; 6 5 4 7 8 6 9 7; 0 5 3 5 4 6 7 3; 5 4 7 3 2 4 5 4;3 4 6 5 3 7 8 9; 5 6 4 3 2 2 4 2; 3 2 1 1 4 7 6 9; 5 6 7 7 5 9 0 9];
  
  x=[1 2 3 4 5 6 7 8 ; 7 5 9 7 3 2 4 1; 2 8 0 8 1 2 0 5; 8 4 3 2 9 9 5 2;5 7 3 4 2 5 9 10; 3 2 1 5 6 4 8 9;5 6 4 3 2 8 7 6; 7 8 6 5 4 3 7 6];
  y=[3;4;5;2;4;3;3;2];
  
  
  classes_perturb=[1;1;1;1;2;2;2;2;3;3;3;3]; 
  classes_ech=[1;2;3;4;1;2;3;4;1;2;3;4];

  [popd]=pop_dextract('epo',x1,classes_ech,classes_perturb);
  
  if ~isdef('popd') then pause;
  end
  
  clear popd x1 x y classes_ech classes_perturb
//=========================================
