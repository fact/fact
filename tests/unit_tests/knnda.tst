// This file is released into the public domain
//=================================
// load fact
if ~isdef('knnda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    x1=[1 3 6 3 7 8 5 3 6 8; 3 4 6 2 3 7 9 5 1 6; 4 5 6 4 3 2 2 2 6 9; 3 4 4 7 8 9 4 7 4 1];  
    x2=[5 3 3 4 5 3 2 1 8 6; 4 8 6 2 4 3 5 2 9 5; 6 4 8 7 2 1 2 1 5 4]; 
    x=[x1;x2];
    
    y=[1 0 0; 1 0 0; 0 1 0; 0 1 0; 0 1 0; 0 0 1; 0 0 1];
    
    [model]=knnda(x,y,3,3);
  
  if ~isdef('model') then pause;
  end
  
  clear model x y x1 x2
//=================================
