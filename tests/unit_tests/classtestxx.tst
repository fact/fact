// This file is released into the public domain
//=================================
// load fact
if ~isdef('classtestxx')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    load('x_140farines.dat')
    x=xfarines.d(:,1:100:$);
    y=groupes.d;
    
    [model]=classtestxx(x,y,x,y,3,'fda');
  
  if ~isdef('model') then pause;
  end
  
  clear model x y 
  clear groupes xfarines classes_ech classes_perturb dmatrix 
//==================================
