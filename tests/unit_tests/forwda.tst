// This file is released into the public domain
//=================================
// load fact
if ~isdef('forwda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    
  load x_140farines.dat
  
  x=xfarines(:,1:50:$); 
    
  [model]=forwda(x,groupes,2,4);

  if ~isdef('model') then pause;
  end
  
  clear model x y x1 x2 groupes xfarines
//======================================
