// This file is released into the public domain
//=================================
// load fact
if ~isdef('calridge')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x=[1 2 3 4 5 6 7 8 9 10; 5 3 7 5 9 7 3 2 4 1; 5 4 2 8 0 8 1 2 0 5; 4 7 8 4 3 2 9 9 5 2;9 6 5 7 3 4 2 5 9 10; 2 3 3 2 1 5 6 4 8 9;3 4 5 6 4 3 2 8 7 6; 4 6 7 8 6 5 4 3 7 6];
  y=[3;4;5;2;4;3;3;2];
  [b,t,p]=calridge(x,y,[0.1;0.01;0.001]);
  
  if ~isdef('b') | ~isdef('t') | ~isdef('p')  then pause;
  end

  clear x y b t p
//=================================
