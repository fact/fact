// This file is released into the public domain
//=================================
// load fact
if ~isdef('cdfsnk')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  

  [q,t]=cdfsnk(8,4);
  
  if ~isdef('q') then pause;
  end
  
  clear q t
//=================================

