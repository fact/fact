// This file is released into the public domain
//=================================
// load fact
if ~isdef('snk')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    x1=[1 1.1 1.2 1.3 2 2.1 2.2 2.3 2.2 2.3 2.4 2.5 3 3.1 3.2 3.3 3.4 ]';
    x2=x1($:-1:1);
    x=[x1 x2];
      
    y=['a';'a';'a';'a';'a';'b';'b';'b';'c';'c';'c';'c';'c';'d';'d';'d'];
    
    res=snk(x,y);

  
    if ~isdef('res') then pause;
    end
  
    clear x y res x1 x2
//=================================
