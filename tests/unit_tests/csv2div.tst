// This file is released into the public domain
//=================================
if ~isdef('csv2div')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x=rand(20,30);
  x=div(x);
  x0=div2csv(x,'killme.csv');

  x2=csv2div('killme.csv',',','.');
  
  if ~isdef('x2') then pause;
  end

  deletefile('killme.csv');
  clear x0 x x2
//=================================
