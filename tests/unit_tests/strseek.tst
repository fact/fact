// This file is released into the public domain
//=================================
// load fact
if ~isdef('strseek')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=['abcd'; 'bcabd'; 'iretf'];

  
  [res]=strseek(x,'ab');
  
  if ~isdef('res') then pause;
  end
  
  clear res x
//=================================
