// This file is released into the public domain
//=================================
if ~isdef('div2mat_div2struct')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  sci_version=strtod(part(getversion(),[8;9;10]));

  if sci_version <6 then
 

    // ---------------------------------------
    // exemple 1: régressions
   
    // obtention des données:
    load('x_pca_pls.dat')  // Cargill dataset
    x1=x_pca_pls.x;
    y1=x_pca_pls.y; 
    res=pls(x1,y1,5,20);

    // application de div2mat_div2struct: 
    res3=div2mat_div2struct(res);
  

    // --------------------------------- 
    // exemple 2: PLSDA
  
    // obtention des données: 
    load x_140farines.dat
    [model_plsda]=plsda(xfarines,groupes,4,4);
  
    // application de div2mat_div2struct
    res4=div2mat_div2struct(model_plsda);

 
    // ---------------------------------------------------------------
    // exemple 3: ACOM (choisi car les sorties comprennent des listes)

    // obtention des données
    loadmatfile x_acom1.mat
    x2=list();
    x2(1)=x.col1;
    x2(2)=x.col2;
    x2(3)=x.col3;
    x2(4)=x.col4;
    x2(5)=x.col5;
  
    res_acom=acom1(x2,8);

    // application de div2mat_div2struct
    res5=div2mat_div2struct(res_acom);



    // --------------------------------------------------------------
    // exemple 4: données EPO (choisies car les plus compliquées)

    load('x_res_epo.dat')
    
    // application de div2mat_div2struct
    res6=div2mat_div2struct(res_epo);
    
 
    
    // -------------------------------------------------
    // vérification du bon fonctionnement: 
    if  ~isdef('res3') | ~isdef('res4') | ~isdef('res5') | ~isdef('res6') then
       pause
    end;
  

  
    // nettoyage des fichiers temporaires
    deletefile('respls.mat');
    deletefile('resplsda.mat');
    deletefile('resacom.mat');
    deletefile('temp_x_res_epo.mat');
  
    clear x x2 y res res2 res3 res4 res6 res_acom matname matname2 matname3 x_pca_pls
    clear res5 model_plsda groupes xfarines sci_version
    clear diff1 diff2 diff6 res_epo
    clear res_bis x1 y1
    
    

  end 

  clear sci_version

  // =========================================================================
  
  
  
  
  
