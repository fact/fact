// This file is released into the public domain
//=================================
// load fact
if ~isdef('isdisj')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  xd=[0 1 0 0;1 0 0 0; 0 0 0 1;0 0 1 0; 0 1 0 0;1 0 0 0; 0 0 0 1;0 0 1 0;0 1 0 0;1 0 0 0; 0 0 0 1;0 0 1 0];

  [test]=isdisj(xd);
  if ~isdef('test') then pause;
  end
  
  clear xd test
//=================================
