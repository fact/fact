// This file is released into the public domain
//=============================================

if ~isdef('outlier')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=============================================

  load('x_pca_pls.dat')  // Cargill dataset

  res_pcana=pcana(x_pca_pls.x,0,0);

  [t2hot,res_variances, leverage]=outlier(x_pca_pls.x,res_pcana.scores, res_pcana.eigenvec,15);

 
  if ~isdef('t2hot') then 
      pause;
  end   
  
  
  clear x_pca_pls res_pcana t2hot res_variances leverage
  
//========================================================
