// This file is released into the public domain
//=================================

  if ~isdef('ikpls')  then
     root_tlbx_path = SCI+'\contrib\fact\';   
     exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================
 
  load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y;         // third variable
  b20_raw=x_pca_pls.b20_pls_brut;
  b20_centred=x_pca_pls.b20_pls_centre;
  
  res=ikpls(x,y,4,20,0);  // raw
  r=correl(res.b.d(:,20),b20_raw);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then 
      pause;
  end
 
  res=ikpls(x,y,4,20,1);   // centred
  r=correl(res.b.d(:,20),b20_centred);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then 
      pause;
  end
 
  // test avec split=une structure
  x_sameobs=[[1:20]' ; [1:20]';[1:20]';[1:20]'];
  split.method='random';
  split.nblocks=2
  split.nbrepeat=10;  
  split.sameobs=x_sameobs;
  res=ikpls(x,y,split,20,1);
  if ~isdef('res') then
      pause
  end
  
  // extraction de la structure de CV
  lot10=res.cv_structure;       // 80 x 2 x 10
  lot1=lot10(:,:,1);            // 80 x 2 
  
  // CV en donnant la structure de la CV - 1 matrice 2D
  split2.method='prefixed';
  split2.nblocks=lot1;
  res=ikpls(x,y,split2,20,1);
  if ~isdef('res') then
      pause
  end
  
  // CV en donnant la structure de la CV - 1 matrice 3D
  split3.method='prefixed';
  split3.nblocks=lot10;
  res=ikpls(x,y,split3,20,1);
  if ~isdef('res') then
      pause
  end
  

  clear x y b20_raw b20_centred res r r2 x_pca_pls x_sameobs split split2 split3 lot1 lot10
//=========================================================================================
