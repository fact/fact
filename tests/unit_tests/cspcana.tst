// This file is released into the public domain
//=================================

  if ~isdef('cspcana')  then
    root_tlbx_path = SCI+'\contrib\fact\';   
    exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //======================================

  load('x_pca_pls.dat')  // Cargill dataset

  // PCA centered and standardized
  res=cspcana(x_pca_pls.x);
  r=correl(res.eigenvec.d(:,20),x_pca_pls.p20_pca_autosc);
  r2=r^2;
  if ~isdef('r2') | r2 < 0.99 then pause;
  end
  
  clear x_pca_pls res r r2 
//=======================================
