// This file is released into the public domain
//=================================

  if ~isdef('dimension_tune')  then
     root_tlbx_path = SCI+'\contrib\fact\';   
     exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================
 
  load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y;         // third variable
  
  model=pls(x,y,10,4);
  
  res=dimension_tune(model);
  
  if ~isdef('res') then 
      pause;
  end
  

  clear x_pca_pls x y model res
//=======================================
