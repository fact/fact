// This file is released into the public domain
//=================================
// load saisir
if ~isdef('dop')  then
  root_tlbx_path = SCI+'\contrib\fact\';    
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x1=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8];
  y1=[3;4;2;5];
  x2=[6 4 5 7 8 9 4 3];
  y2=[2.5];  
  
  [pdop]=dop(x1,y1,x2,y2);
  
  if ~isdef('pdop') then pause;
  end
  
  clear pdop x1 x2 y1 y2
//=================================
