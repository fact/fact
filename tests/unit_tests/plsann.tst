// This file is released into the public domain
//=================================

  if ~isdef('pls')  then
     root_tlbx_path = SCI+'\contrib\fact\';   
     exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================
 
  load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y;     
  
  opt.RegClass=0;
  opt.PreProc=0;
  
  
  model1=plsann(x,y,4,10,1,7,opt);
  // 4= nbre blocs
  // 10 = LV (le calcul est fait sur LV, pas sur 1 à LV )
  // 1 = centré 
  // 7 = wh = nbre de neurones dans la couche cachée 
  // opt=options 

  model2=plsann(x,y,4,[3,5],1,7,opt);



  if ~isdef('model1') |  ~isdef('model2')   then
      pause
  end

  clear x_pca_pls x y model1 model2
//=================================================================
