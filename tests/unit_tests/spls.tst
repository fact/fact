// This file is released into the public domain
//=================================
// load saisir
if ~isdef('spls')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y1;         // third variable
  sec_ref_4bl_5_10_20VL=[0.3695 0.2295 0.0554]';  // obtained with Matlab, S.D.Brown's toolbox
  sec_ref_8bl_5_10_20VL=[0.3677 0.2002 0.0653]';  // idem
  
  res4=spls(x,y,20,'blk10','blk4',0); 
  res8=spls(x,y,20,'blk10','blk8',0);  
  
  sec_res4_5_10_20VL=res4.err.d([5 10 20],1);
  sec_res8_5_10_20VL=res8.err.d([5 10 20],1);
  
  error4_pcent= (sec_ref_4bl_5_10_20VL - sec_res4_5_10_20VL)./sec_ref_4bl_5_10_20VL;
  error4_pcent=mean(abs(error4_pcent));
  
  error8_pcent= (sec_ref_8bl_5_10_20VL - sec_res8_5_10_20VL)./sec_ref_8bl_5_10_20VL;
  error8_pcent=mean(abs(error8_pcent));
  
  if ~isdef('error4_pcent') | ~isdef('error8_pcent') then 
      pause;
  else 
      if error4_pcent >0.10 | error8_pcent > 0.10 then 
          pause;
      end  
  end
  
  clear res x y x_pca_pls sec_ref_4bl_5_10_20VL sec_ref_8bl_5_10_20VL
  clear res4 res8 sec_res4_5_10_20VL sec_res8_5_10_20VL error4_pcent error8_pcent
//================================================================================
