// This file is released into the public domain
//=================================
// load fact
if ~isdef('vodka')  then
  root_tlbx_path = SCI+'\contrib\fact\';    
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y;         // y=third variable out of four
  b20_raw=x_pca_pls.b20_pls_brut;
  b20_centred=x_pca_pls.b20_pls_centre;
  
  res=vodka(x,y,4,20,1,0);   // 1=x'y  0=raw
  r=correl(res.b.d(:,20),b20_raw);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then pause;
  end
 
  res=vodka(x,y,4,20,1,1);   // 1=x'y  1=centred
  r=correl(res.b.d(:,20),b20_centred);
  r2=r^2;
  if ~isdef('r2') | r2<0.99 then pause;
  end
 
  clear x y b20_raw b20_centred res r r2 x_pca_pls
//================================================
