// This file is released into the public domain
//=================================
// load fact
if ~isdef('obiwarp')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  x1=lmata2div('x_tmp1B.lmata');
  x2=lmata2div('x_tmp2.lmata');
  
  x2_new_rt=obiwarp(x1,x2,'cov',[],[],[2.1,1],[0,11.7],0,30);
  
  x2_new_rt_ref=[1200.34 1208.1215 1216.8047 1226.2557 1236.34 1247.6094 1259.9746 1272.34 1284.6614 1297.1971 1309.3042 1320.34 1329.7654 1338.1887 1346.6876 1356.34 1368.34 1379.2556 1389.5706 1399.5193 1409.3362 1419.2556 1429.5121 1440.34 1451.9807 1464.1604 1476.34 1488.34 1500.34 1512.34 1524.34 1535.243 1545.5288 1555.4441 1565.2358 1575.1511 1585.4369 1596.34 1608.34 1619.2429 1621.0328 1631.5647 1641.6654 1651.231 1660.1572 1668.34]';
  
  if ~isdef('x2_new_rt') then       // 1° test: existance de la sortie
      pause;
  else
      diff_rt=x2_new_rt - x2_new_rt_ref;
      diff_max=max(abs(diff_rt));
      
      if diff_max > 1 then          // 2° test: comparaison avec une sortie precedente
          pause
      end
  end

  
  clear x1 x2 x2_new_rt x2_new_rt_ref diff_rt diff_max 
//====================================================


