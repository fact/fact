// This file is released into the public domain
//=================================
// load saisir
if ~isdef('statislda')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  
  x=list(); 

  x(1)=rand(20,10);
  x(2)=rand(20,5);
  x(3)=rand(20,15);

  y=[1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4 5 5 5 5]';
 
  res=statislda(x,y);
 
  if ~isdef('res') then pause;
  end
  
  clear x y res
//=================================
