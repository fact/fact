// This file is released into the public domain
//=================================
if ~isdef('pds_calc')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  

   load('x_corn.dat')
   
   x1cal=x1([1;5;7;12;13;28;33;36],:); 
   x2cal=x2([1;5;7;12;13;28;33;36],:);
   x1test=x1;
   x2test=x2;
   
   // calcul du modèle 
   [model_pds]=pds_calc(x1cal,x2cal,15);
  
   if ~isdef('model_pds') then 
       pause
   end
 

   clear x11 x2 x3 y x1cal x2cal x1test x2test xhat0 xhat model_pds b p x1 t
   
   clearglobal
//========================================================================
