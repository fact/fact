// This file is released into the public domain
//=================================
// load fact
if ~isdef('statis')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  col=list();
  col(1)=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8];
  col(2)=[2 3 4 2 6 5 7 8; 6 5 4 7 8 6 9 7; 0 5 3 5 4 6 7 3; 5 4 7 3 2 4 5 4];
  col(3)=[3 4 6 5 3 7 8 9; 5 6 4 3 2 2 4 2; 3 2 1 1 4 7 6 9; 5 6 7 7 5 9 0 9];
  
  [res_statis]=statis(col);  
  
  if ~isdef('res_statis') then pause;
  end
  
  clear res_statis col
//=================================
