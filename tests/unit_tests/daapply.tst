// This file is released into the public domain
//=================================
// load fact
if ~isdef('daapply')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  
  // chargement des données: 
  load x_140farines.dat
  
  //x=xfarines(:,1:10:$);
  x=xfarines;
    
  // groupes définis par des noms
  groupes2=[repmat('a',35,1); repmat('b',27,1);repmat('c',38,1);repmat('d',40,1)];  
 
 
  // modèle PLS-DA
  [model_plsda]=plsda(x,groupes,4,5);                      // 5 est trop grand: 4 classes
  [model2_plsda]=daapply(model_plsda,x);
  [model2_plsda_bis]=daapply(model_plsda,x,groupes);
  
  // modèle FDA
  [model_fda]=fda(x(:,1:50:$),groupes,4,5);                // 5 est trop grand: 4 classes
  [model2_fda]=daapply(model_fda,x(:,1:50:$),groupes);
  
  // modèle PLS-FDA  
  [model_pls_fda]=plsfda(x,groupes,4,[10,5],1,'cs');       // 5 est trop grand: 4 classes
  [model2_pls_fda]=daapply(model_pls_fda,x,groupes); 

  // vérification que la fonction tourne
  if  ~isdef('model2_plsda') |  ~isdef('model2_plsda_bis') | ~isdef('model_fda') |~isdef('model2_fda') | ~isdef('model_pls_fda') | ~isdef('model2_pls_fda')  then 
      pause;
  end
  
  // vérification que les résultats de daapply sont les mêmes que ceux des méthodes discriminantes: 
  diff_scores= model_plsda.scores.d - model2_plsda.xval_scores.d;
  if max(diff_scores)>10000* %eps then
      pause
  end
  
  diff_scores= model_pls_fda.scores.d - model2_pls_fda.xval_scores.d;
  if max(diff_scores)>10000* %eps then
      pause
  end
  
  // vérification que pour PLS-FDA la classification est parfaite avec 2 dimensions et 10 VL
  matrice_confusion=model2_pls_fda.conf_test_nobs(2).d;
  diff_mc=sum(matrice_confusion(:,:,10))-sum(diag(matrice_confusion(:,:,10)));
  if diff_mc>0 then
      pause
  end
  
  clear x xfarines groupes2 model_plsda model2_plsda model2_plsda_bis model_fda model2_fda
  clear groupes conf_cv conf_cal err_cal err_cv vp_cal err_cal_2 err_cv_2 conf_cv_2
  clear conf_cal_2 vp_cal_2 
  clear diff_mc diff_scores matrice_confusion model_pls_fda model2_pls_fda 
//===============================================================================
