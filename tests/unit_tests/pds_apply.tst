// This file is released into the public domain
//=================================
if ~isdef('pds_apply')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  
   load('x_corn.dat')
   
   x1cal=x1([1;5;7;12;13;28;33;36],:); 
   x2cal=x2([1;5;7;12;13;28;33;36],:);
   x1test=x1;
   x2test=x2;
   
   // calcul du modèle 
   [model_pds]=pds_calc(x1cal,x2cal,15);

   // application sur les données d'étalonnage:
   xhat0=pds_apply(model_pds,x1cal);

   figure;
   plot(xhat0.d(1:5,:)','r')
   plot(x1cal(1:5,:)','k')
   plot(x2cal(1:5,:)','b')

   // application sur les données de test:
   xhat=pds_apply(model_pds, x1test);

   for i=1:5; 
     figure;
     plot(xhat.d(i*8,:)','r')
     plot(x1test(i*8,:)','k')
     plot(x2test(i*8,:)','b')
   end
  
   if ~isdef('model_pds') then 
       pause
   end
 
   for i=1:6;				// maj 19aout21
   	h=gcf();
	close(h); 
   end;

   clear x11 x2 x3 y x1cal x2cal x1test x2test xhat0 xhat model_pds b p x1 i t
   
   clearglobal
//=============================================================================
