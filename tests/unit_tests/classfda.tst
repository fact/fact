// This file is released into the public domain
//=================================
// 
if ~isdef('classfda')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
   
    // charger les donnees
    load('x_mayo.dat')
    load('x_res_fda_jmr.dat')
    
    // extraction des vecteurs-propres de reference
    vp=res_mayo_jmr.vp;
    
    // selection des variables -> construction de x et y 
    x=xmayo(:,res_mayo_jmr.num(1:5));
    x=centering(x);
    x=x.d;
    y=conj2dis(xmayo_classes.d);
    
    // application de la fda 
    [model,V,valp]=classfda(x,y,5);

    // validation 
    x_corr_v=corrmat(V,vp);
    corr_moy=mean(abs(diag(x_corr_v.d)));
  
    if ~isdef('corr_moy') | corr_moy<0.97 then 
        pause;
    end
  
  clear vp res_mayo_jmr xmayo xmayo_classes x y model V valp x_corr_v corr_moy  
//=================================
