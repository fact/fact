// This file is released into the public domain
//=================================
// load fact
if ~isdef('dendro')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    x=[1.01   2.1   3.31   4.21   5.12   6.04]';
    code=['a';'b';'c';'d';'e';'f']
    
    [x_dist,x_l]=dendro(x);
    
    //x_dist  =  1.09   2.3   3.2   4.11   5.03   1.21   2.11   3.02   3.94   0.9    1.81   2.73   0.91   1.83   0.92
    //            1-2    1-3   1-4   1-5    1-6    2-3    2-4    2-5    2-6    3-4    3-5    3-6    4-5   4-6    5-6 
    // résultat attendu: les distances entre 1-2, 1-3, ...5-6
    
    x_l_ref  = [ 3.  4.   0.9 ;5.  7.  0.91; 6.  8.  0.92; 1. 2.  1.09; 9.   10.   1.21]; 
  
    diff_i=abs(x_l_ref(:,1:2)-x_l.d(:,1:2));
  
    if max(diff_i) > 100* %eps then
        pause 
    end

    h=gcf();
    clear h

    clear x x_dist x_l x_l_ref diff_i code 
//=======================================================================
