// This file is released into the public domain
//=================================

  if ~isdef('c_nnsimul')  then
    root_tlbx_path = SCI+'\contrib\fact\';      
    exec(root_tlbx_path + 'loader.sce',-1); 
  end
  //=================================

 
  Nd=100; 
  Ni=5; 
  No=3; 
  Nh=10;

  Inp=rand(Nd,Ni); 
  Wh=rand(Ni+1,Nh);   
  Wo=rand(Nh+1,No);
  
  res=c_nnsimul(Wh,Wo,Inp);
  
  if ~isdef('res') then
      pause
  end 
  
  
  // Nettoyage
  clear Nd Ni No Nh Inp Wh Wo res 
 
//=================================
