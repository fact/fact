// This file is released into the public domain
//=================================
// load saisir
if ~isdef('vanderm')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 

  [vdm]=vanderm(700,3);
  
  if ~isdef('vdm') then pause;
  end
  
  clear vdm
//=================================
