// This file is released into the public domain
//=================================
// load fact
if ~isdef('ridge')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

    load('x_pca_pls.dat')  // Cargill dataset
  
  x=x_pca_pls.x;
  y=x_pca_pls.y;         // third variable
  
  [model_ridge]=ridge(x,y,4,[0.1;2],0);
  
  if ~isdef('model_ridge') then pause;
  end
  
  clear model_ridge x y
//=================================
