// This file is released into the public domain
//=================================
// load fact
if ~isdef('cvxx')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
    // chargement des données 
    load('x_pca_pls.dat');
    x=x_pca_pls.x;
    y=x_pca_pls.y;
    
    // adaptation à x_sameobs et prefixed_blocks
    //x=x(1:40,:);
    //y=y(1:40,:);
    
    
     // Données: 20 échantillons avec des répétitions -> 40 mesures
    x_sameobs=string([1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 15 16 16 17 17 18 18 19 19 20 20 ...
         21 21 22 22 23 23 24 24 25 25 26 26 27 27 28 28 29 29 30 30 31 31 32 32 33 33 34 34 35 35 36 36 37 37 38 38 39 39 40 40]'); 
    prefixed_blocks=[1 6 4 6 1 5 5 2 6 4 4  2 3 6 5 6 3 3 6 4 3 3 6 5 2 2 4 4  2 5 5 1 1 4 4  1 1 5 5 2 ...
      6 4 5 1 1 4 5 1 4 5  1 1 4 5 1 6 2 4 5 2 2 4 5 2 2 4 5 2 4 6 6 3 6 6 3 6 5 3 3 3; ...
      6 4 5 1 1 4 5 1 4 5  1 1 4 5 1 6 2 4 5 2 2 4 5 2 2 4 5 2 4 6 6 3 6 6 3 6 5 3 3 3 ...
      1 6 4 6 1 5 5 2 6 4 4  2 3 6 5 6 3 3 6 4 3 3 6 5 2 2 4 4  2 5 5 1 1 4 4  1 1 5 5 2; ...
      1 2 3 6 2 3 1 6 3 1 2 3 1 2 6 1 2 3 1 3 6 5 4 5 4 5 4 6 4 5 4 6 4 5 4 5 4 5 6 5 ...
      1 6 4 6 1 5 5 2 6 4 4  2 3 6 5 6 3 3 6 4 3 3 6 5 2 2 4 4  2 5 5 1 1 4 4  1 1 5 5 2 ; ...
      1 6 4 6 1 5 5 2 6 4 4  2 3 6 5 6 3 3 6 4 3 3 6 5 2 2 4 4  2 5 5 1 1 4 4  1 1 5 5 2  ...
      1 2 3 6 2 3 1 6 3 1 2 3 1 2 6 1 2 3 1 3 6 5 4 5 4 5 4 6 4 5 4 6 4 5 4 5 4 5 5 4 ;... 
      6 4 5 1 1 4 5 1 4 5  1 1 4 5 1 6 2 4 5 2 2 4 5 2 2 4 5 2 4 6 6 3 6 6 3 6 5 3 3 3 ...
      1 2 3 6 2 3 1 6 3 1 2 3 1 2 6 1 2 3 1 3 6 5 4 5 4 5 4 6 4 5 4 6 4 5 4 5 4 5 6 5];
    prefixed_blocks=prefixed_blocks';

    // paramètres de la version d’avant Nov.18
    [secv11,predcv11,dof11,lot11,stats11] = cvxx(x,y,5,'pls',1,30);           // 5 blocs random - 1=centré - 10VL
    [secv12,predcv12,dof12,lot12,stats12] = cvxx(x,y,[5 4],'pls',1,30);       // 5 blocs, 4 repets, random - 1=centré - 10VL
    [secv13,predcv13,dof13,lot13,stats13] = cvxx(x,y,'blk5','pls',1,30);      // 5 blocs random - 1=centré - 10VL
    [secv14,predcv14,dof14,lot14,stats14] = cvxx(x,y,'jck5','pls',1,30);      // 5 blocs jack - 1=centré - 10VL
    [secv15,predcv15,dof15,lot15,stats15] = cvxx(x,y,'vnb5','pls',1,30);      // 5 blocs venitian - 1=centré - 10VL
    
    // paramètres de la version d’après Nov.18
    split1=struct();
    split1.method='random'; 
    split1.nblocks=5;
    split1.nbrepeat=1;
    split1.sameobs=string([1:80]');                             // pas de répétitions 

    split1b=struct();
    split1b.method='random'; 
    split1b.nblocks=5;
    split1b.nbrepeat=1;
    split1b.sameobs=x_sameobs;                                   // répétitions
  
    split2=struct();
    split2.method='random'; 
    split2.nblocks=5;
    split2.nbrepeat=4;                                           // répétitions des CV
    split2.sameobs=string([1:80]');                              // pas de répétition des observations 

    split2b=struct();
    split2b.method='random';                // FAUX 
    split2b.nblocks=5;
    split2b.nbrepeat=4;
    split2b.sameobs=x_sameobs;                                   // répétitions
  
    split4=struct();
    split4.method='jack';
    split4.nblocks=5;
    split4.nbrepeat=1;                                           // inutile de répéter avec ‘jack’ ou ‘venitian’
    split4.sameobs=string([1:80]');                              // pas de répétitions 

    split4b=struct();
    split4b.method='jack';
    split4b.nblocks=5;
    split4b.nbrepeat=1;                                          // inutile de répéter avec ‘jack’ ou ‘venitian’
    split4b.sameobs=x_sameobs;

    split5=struct();
    split5.method='venitian';
    split5.nblocks=3;
    split5.nbrepeat=1;                                           // inutile de répéter avec ‘jack’ ou ‘venitian’
    split5.sameobs=([1:80]'); 

    split5b=struct();
    split5b.method='venitian';
    split5b.nblocks=3;
    split5b.nbrepeat=1;                                         // inutile de répéter avec ‘jack’ ou ‘venitian’
    split5b.sameobs=x_sameobs;

    split6=struct();
    split6.method='prefixed';
    split6.nblocks=prefixed_blocks;                             // sameobs=inutile si 'prefixed'

    [secv21,predcv21,dof21,lot21,stats21] =     cvxx(x,y,split1,'pls',1,30);
    [secv21b,predcv21b,dof21b,lot21b,stats21b] = cvxx(x,y,split1b,'pls',1,30);
    [secv22,predcv22,dof22,lot22,stats22] =     cvxx(x,y,split2,'pls',1,30);
    [secv22b,predcv22b,dof22b,lot22b,stats22b] = cvxx(x,y,split2b,'pls',1,30);   // faux voir plus bas 
    [secv24,predcv24,dof24,lot24,stats24] =     cvxx(x,y,split4,'pls',1,30);
    [secv24b,predcv24b,dof24b,lot24b,stats24b] = cvxx(x,y,split4b,'pls',1,30);
    [secv25,predcv25,dof25,lot25,stats25] =     cvxx(x,y,split5,'pls',1,30);    
    [secv25b,predcv25b,dof25b,lot25b,stats25b] = cvxx(x,y,split5b,'pls',1,30);
    [secv26,predcv26,dof26,lot26,stats26] =     cvxx(x,y,split6,'pls',1,30);
  
    
    // essais avec en entrée les matrices disjonctives
    split1c=struct()
    split1c.method='prefixed';
    split1c.nblocks=lot21;
    
    split2c=struct()
    split2c.method='prefixed';
    split2c.nblocks=lot22;
   
    split4c=struct()
    split4c.method='prefixed';
    split4c.nblocks=lot24;
 
    split5c=struct()
    split5c.method='prefixed';
    split5c.nblocks=lot25;
   
    split6c=struct()
    split6c.method='prefixed';
    split6c.nblocks=lot26;
   
    [secv21c,predcv21c,dof21c,lot21c,stats21c] = cvxx(x,y,split1c,'pls',1,30);
    [secv22c,predcv22c,dof22c,lot22c,stats22c] = cvxx(x,y,split2c,'pls',1,30);
    [secv24c,predcv24c,dof24c,lot24c,stats24c] = cvxx(x,y,split4c,'pls',1,30);
    [secv25c,predcv25c,dof25c,lot25c,stats25c] = cvxx(x,y,split5c,'pls',1,30);
    [secv26c,predcv26c,dof26c,lot26c,stats26c] = cvxx(x,y,split6c,'pls',1,30);
   
    
    // pour visualiser
    secv_all=[secv11 secv12 secv13 secv14 secv21 secv21b secv22 secv22b secv24 secv24b secv25 secv25b secv26 secv21c secv22c secv24c secv25c]; 
    // disp(secv_all,'secv_all=')
    
    // comparaison sur lot 
    thresh=100*%eps; 
   
    if  lot21 - lot21c >thresh  | lot22 - lot22c >thresh   =="T" then
        pause
    end
    if  lot24 - lot24c > thresh | lot25 - lot25c > thresh =="T" then 
        pause
    end
    if lot26 - lot26c >thresh  =="T" then
        pause
    end
   
    // comparaison sur rmsecv
    if sum(abs(secv13-secv14))>thresh | sum(abs(secv21c-secv21))>thresh  =="T"   then
        pause;
    end
    if sum(abs(secv22c-secv22))>thresh | sum(abs(secv24b-secv14))>thresh =="T"   then
        pause;
    end
    if secv11 - stats11.mean > thresh |  secv12 - stats12.mean > thresh =="T" then
        pause
    end
    if secv13 - stats13.mean > thresh | secv14 - stats14.mean > thresh =="T" then
        pause
    end
    if secv15 - stats15.mean > thresh | secv21 - stats21.mean > thresh =="T" then
        pause
    end
    if secv21b - stats21b.mean > thresh |  secv22 - stats22.mean > thresh =="T" then
        pause
    end
    
    // cas particulier de: 1) répétitions d'observations; 2) répétitions de CV => problèmes d'arrondis entre les 2 calculs
    if (secv22b - stats22b.mean)./secv22b > 0.01 then                                
        pause  
    end    
    
    if  secv24 - stats24.mean > thresh =="T" then  
        pause
    end
    if secv24b - stats24.mean > thresh | secv25 - stats25.mean > thresh =="T" then
        pause
    end
    if secv25b - stats25b.mean > thresh | secv26 - stats26.mean > thresh =="T" then
        pause
    end
    if  secv21c - stats21c.mean > thresh | secv22c - stats22c.mean > thresh  =="T" then
        pause
    end
    if secv24c - stats24c.mean > thresh |  secv25c - stats25c.mean > thresh =="T" then
        pause
    end
    if secv26c - stats26c.mean > thresh =="T" then
        pause
    end

  
  clear x_sameobs prefixed_blocks  
  clear split1 split2 split3 split4 split1b split2b split3b split4b  
  clear split1c split2c split4c split5 split5b split5c split6 split6c
  clear secv11 secv12 secv13 secv14 secv21 secv22 secv23 secv24 secv21b secv22b secv23b secv24b 
  clear secv15 secv21c secv22c secv24c secv25 secv25b secv25c secv26 secv26c
  clear secv_all
  clear predcv11 predcv12 predcv13 predcv14 predcv21 predcv22 predcv23 predcv24 predcv21b predcv22b predcv23b predcv24b 
  clear predcv15 predcv21 predcv22 predcv24 predcv25 predcv25b predcv25c predcv26 
  clear predcv21c predcv22c predcv24c predcv26 predcv26c
  clear dof11 dof12 dof13 dof14 dof21 dof22 dof23 dof24 dof24c dof21b dof22b dof23b dof24b  
  clear dof15 dof21c dof22 dof22c dof24 dof25 dof25b dof25c dof26 dof26c 
  clear lot11 lot12 lot13 lot14 lot15 
  clear lot21 lot22 lot23 lot24 lot21b lot22b lot23b lot24b
  clear lot21c lot22c lot24c lot25 lot25b lot25c lot26 lot26c
  clear stats11 stats12 stats13 stats14 stats15
  clear stats21c stats22c stats24c stats25c stats26c 
  clear stats21 stats21b stats22 stats22b stats24 stats24b stats25 stats25b stats26 
  clear x_pca_pls x y ans thresh
//=======================================================================================
