// This file is released into the public domain
//=================================
// load saisir
if ~isdef('conf_obs2pcent')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x1.d=[5 3 2 0 0;0 4 1 0 3;1 0 6 5 0;1 2 0 0 0;1 1 1 5 7];
  x2.d=[5 0 0 0 0;0 0 1 0 3;1 0 8 3 0;1 0 0 7 0;1 0 1 0 7];
  
  x1.i='pred' + string([1:1:5]');
  x1.v='ref'  + string([1:1:5]');
  
  x1=div(x1);
  x2=div(x2);

  nobs1=[10;10;10;10;10]';
  nobs2=[10;0;10;10;10]';

  [conf_pcent1,err_classif1,err_classif_byclass1,not_class1,not_class_byclass1]=conf_obs2pcent(x1.d);
  [conf_pcent2,err_classif2,err_classif_byclass2,not_class2,not_class_byclass2]=conf_obs2pcent(x1.d,nobs1);
  [conf_pcent3,err_classif3,err_classif_byclass3,not_class3,not_class_byclass3]=conf_obs2pcent(x2.d);
  [conf_pcent4,err_classif4,err_classif_byclass4,not_class4,not_class_byclass4]=conf_obs2pcent(x2.d,nobs2);
  
  
  if ~isdef('err_classif1') | abs(err_classif1 -54.16667)>0.0001                         then pause; end  
  if ~isdef('err_classif_byclass1') | err_classif_byclass1 ~= [37.5;60;40;100;30]        then pause; end
 
  if ~isdef('err_classif2') | abs(err_classif2 -56)>0.0001                               then pause; end  
  if ~isdef('err_classif_byclass2') | err_classif_byclass2 ~= [50  ;60;40;100;30]        then pause; end
  if ~isdef('not_class2') | not_class2 ~= 4                                              then pause; end
  if ~isdef('not_class_byclass2') | not_class_byclass2 ~= [20;0;0;0;0]                   then pause; end
  
  if ~isdef('err_classif3') | abs(err_classif3 -28.947368)>0.0001                        then pause; end  
  if ~isdef('err_classif_byclass3') | err_classif_byclass3 ~= [37.5;0;20;30;30]          then pause; end

  if ~isdef('err_classif4') | abs(err_classif4 -32.5)>0.0001                             then pause; end  
  if ~isdef('err_classif_byclass4') | err_classif_byclass4 ~= [50;0;20;30;30]            then pause; end
  if ~isdef('not_class4') | not_class4 ~= 5                                              then pause; end
  if ~isdef('not_class_byclass4') | not_class_byclass4 ~= [20;0;0;0;0]                   then pause; end
 
  
  clear x1 x2 nobs1 nobs2 conf_pcent1 conf_pcent2 conf_pcent3 conf_pcent4 err_classif1 err_classif2 err_classif3 err_classif4 
  clear err_classif_byclass1 err_classif_byclass2 err_classif_byclass3 err_classif_byclass4 not_class1 not_class2 not_class3 not_class4
  clear not_class_byclass1 not_class_byclass2 not_class_byclass3 not_class_byclass4
//======================================================================================================================================
