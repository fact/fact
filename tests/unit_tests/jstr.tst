// This file is released into the public domain
//=================================
// load saisir
if ~isdef('jstr')  then
  root_tlbx_path = SCI+'\contrib\fact\';  
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 
  x=['ABCD'; 'BB'; 'EAART';'T';'YBCQ';'NAARE'];
 
  [xj]=jstr(x,5);
  
  if ~isdef('xj') then pause;
  end
  
  clear  x xj
//=================================
