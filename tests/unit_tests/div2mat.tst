// This file is released into the public domain
//=================================
if ~isdef('div2mat')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
 

  //sci_version=strtod(part(getversion(),[8;9;10]));

  
  //if sci_version <6 then 
  
  for i=1:10;      // 10 repetitions car cette fonction plante aleatoirement 
 
    // ---------------------------------------
    // exemple 1: régressions
   
    // obtention des données:
    load('x_pca_pls.dat')  // Cargill dataset
    x1=x_pca_pls.x;
    y1=x_pca_pls.y; 
    res=pls(x1,y1,5,20);

    // application de div2mat: 
    matname='temp_respls.mat';
    res3=div2mat(res,matname);
  

    // --------------------------------- 
    // exemple 2: PLSDA
  
    // obtention des données: 
    load x_140farines.dat
    [model_plsda]=plsda(xfarines,groupes,4,4);
  
    // application de div2mat
    matname2='temp_resplsda.mat';
    res4=div2mat(model_plsda,matname2);

 
    // ---------------------------------------------------------------
    // exemple 3: ACOM (choisi car les sorties comprennent des listes)

    // obtention des données
    loadmatfile x_acom1.mat
    clear x2
    x2=list();
    x2(1)=x.col1;
    x2(2)=x.col2;
    x2(3)=x.col3;
    x2(4)=x.col4;
    x2(5)=x.col5;
  
    res_acom=acom1(x2,8);

    matname3='temp_resacom.mat';
    res5=div2mat(res_acom,matname3);



    // --------------------------------------------------------------
    // exemple 4: données EPO (choisies car les plus compliquées)

    load('x_res_epo.dat')
    
    res6=div2mat(res_epo, 'temp_x_res_epo.mat');
    
    diff1=abs(max(res_epo.d_matrix.d - res6.d_matrix.d));
    diff2=abs(max(res_epo.pls_models(3).err.d - res6.pls_models.list3.err.d));
     
    if max(diff1) > 1000*%eps | max(diff2) > 1000*%eps then
        error('error in div2mat')
    end 
 
 
    
    // -------------------------------------------------
    // vérification du bon fonctionnement: 
    if  ~isdef('res3') | ~isdef('res4') | ~isdef('res5') | ~isdef('res6') then
       pause
    end;
  
 end
  

    // nettoyage des fichiers temporaires
    deletefile('temp_respls.mat');
    deletefile('temp_resplsda.mat');
    deletefile('temp_resacom.mat');
    deletefile('temp_x_res_epo.mat');
  
    clear x x1 x2 y y1 res res2 res3 res4 res6 res_acom matname matname2 matname3 x_pca_pls
    clear res5 model_plsda groupes xfarines 
    clear diff1 diff2 diff6 res_epo sci_version
  
  //end

  // =========================================================================
  
  
  
  
  
