// This file is released into the public domain
//=================================
// load fact
if ~isdef('pop_dtune')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  
  x1=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8;2 3 4 2 6 5 7 8; 6 5 4 7 8 6 9 7; 0 5 3 5 4 6 7 3; 5 4 7 3 2 4 5 4;3 4 6 5 3 7 8 9; 5 6 4 3 2 2 4 2; 3 2 1 1 4 7 6 9; 5 6 7 7 5 9 0 9];
  y1=[1;2;3;4;5;6;7;8;9;10;11;12];
  
  classes_perturb=[1;1;1;1;2;2;2;2;3;3;3;3]; 
  classes_ech=[1;2;1;2;1;2;1;2;1;2;1;2];

  dmatrix=pop_dextract('epo',x1,classes_ech,classes_perturb);

  result=pop_dtune(dmatrix,3,x1,classes_ech,x1,y1,[1;1;2;2;3;3;1;1;2;2;3;3],'pls',1,2);
  
  if ~isdef('result') then 
      pause;
  end
  
  clear result classes_ech classes_perturb x1 y1 dmatrix
  
//======================================================
