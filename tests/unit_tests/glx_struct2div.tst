// This file is released into the public domain
//=================================
// load fact
if ~isdef('glx_struct2div')  then
  root_tlbx_path = SCI+'\contrib\fact\';      
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  // chargement des données: 
  load x_140farines.dat
  
  x=xfarines(:,1:10:$);
  x=div(x);
  
  x2=glx_div2struct(x);
  
  x3=glx_struct2div(x2);
  

  if ~isdef('x3') then pause;
  end
    
  clear model x y x1 x2 x3 groupes xfarines
//======================================
