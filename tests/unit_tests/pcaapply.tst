// This file is released into the public domain
//=================================
if ~isdef('pcaapply')  then
  root_tlbx_path = SCI+'\contrib\fact\';   
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
  
  x=rand(20,30);
  
  res=cspcana(x);
  
  res2=pcaapply(res,x,5);

  differ=res2.d-res.scores.d(:,1:5);
  differ=abs(differ);
  
  if max(differ)>10E-12 then
       pause
  end  

  //deletefile('killme.csv')
  clear x res res2 differ
//=========================================
