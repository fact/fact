// This file is released into the public domain
//=================================
if ~isdef('gapremove')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  load('x_corn.dat');

  x1=div(x1);
  
  x1bis=gapremove(x1,[30,45,75],3);
  
  if ~isdef('x1bis') then pause;
  end
  
  clear x1bis b t p x1 x2 x3 y
//=================================
