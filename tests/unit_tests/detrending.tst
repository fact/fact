// This file is released into the public domain
//=================================
// load saisir
if ~isdef('detrending')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x=[1 2 3 4 5 6 7 8 9 10; 5 3 7 5 9 7 3 2 4 1; 5 4 2 8 0 8 1 2 0 5; 4 7 8 4 3 2 9 9 5 2];

  [xc]=detrending(x);
  
  if ~isdef('xc') then pause;
  end
  
  clear xc x
//=================================
