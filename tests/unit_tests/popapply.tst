// This file is released into the public domain
//=================================
// load fact
if ~isdef('popapply')  then
  root_tlbx_path = SCI+'\contrib\fact\'; 
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================

  x1=[1 4 5 6 4 8 7 9; 5 6 5 4 3 8 7 9; 5 4 3 6 2 9 8 0; 5 4 5 3 7 6 6 8;2 3 4 2 6 5 7 8; 6 5 4 7 8 6 9 7; 0 5 3 5 4 6 7 3; 5 4 7 3 2 4 5 4;3 4 6 5 3 7 8 9; 5 6 4 3 2 2 4 2];

  x2=[4 3 2 8 6 7 4 5; 3 6 7 5 6 9 0 8; 2 4 5 6 8 9 5 2;3 4 5 3 3 2 6 0; 6 4 2 5 7 8 9 0; 8 7 9 3 7 1 9 9; 4 5 5 6 7 9 2 6; 5 4 3 2 2 2 7 5; 3 2 1 1 4 7 6 9; 5 6 7 7 5 9 0 9];
  
  x3=(x1+x2)/2;
  x4=(x1-x2)/2;
  
  x=[x1;x2;x3;x4];
  
  y=[3;4;5;2;4;3;3;2;4;5;3;8;3;4;5;2;4;3;3;2;4;5;3;8;3;4;5;2;4;3;3;2;4;5;3;8;3;4;5;2];
 
  x0=[1 2 3 4 5 6 7 8 ; 7 5 9 7 3 2 4 1; 2 8 0 8 1 2 0 5; 8 4 3 2 9 9 5 2;5 7 3 4 2 5 9 10; 3 2 1 5 6 4 8 9;5 6 4 3 2 8 7 6; 7 8 6 5 4 3 7 6]; 
  
  classes_perturb=[1;1;1;1;2;2;2;2]; 
  classes_ech=[1;2;1;2;1;2;1;2];

  [pepo]=epo(x0,classes_ech,classes_perturb,x,y,2,2);
 
  [res_applyepo]=popapply(pepo,1,x,y);
  if ~isdef('res_applyepo') then pause;
  end
  
  clear pepo x0 x1 x2 x3 x4 x y classes_ech classes_perturb res_applyepo
//==========================================================
